package com.storatwebview;

import com.facebook.react.ReactActivity;
//import com.RNFetchBlob.RNFetchBlobPackage;
//import com.farmisen.react_native_file_uploader.RCTFileUploaderPackage;
// import com.burlap.filetransfer.FileTransferPackage;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "StoratApp";
    }
}

import { AppRegistry } from 'react-native';
import AppRoot from './src';

AppRegistry.registerComponent('StoratApp', () => AppRoot);
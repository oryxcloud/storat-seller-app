import base64 from 'base-64';
import encodePostData from './encodePostData';

export default (url, formBody, headers) => {
    let defaultHeaders = {
        'Content-Type': 'application/x-www-form-urlencoded',
        // 'Authorization': 'Basic ' + btoa('hamooz:hamooz@123')
    };

    //let cheaders = new Headers();
    //console.log(base64.encode('hamooz:hamooz@123'))
    //console.log(btoa('hamooz:hamooz@123'))
    //cheaders.append('Authorization', 'Basic ' + base64.encode('hamooz:hamooz@123'));
    //cheaders.append('Content-Type', 'application/x-www-form-urlencoded');


    return fetch(url, {
        method: 'post',
        headers: Object.assign(defaultHeaders, headers),
        body: formBody || encodePostData({})
    });
}
export default email => {
    return /^.+@.+\..+$/i.test(email);
}
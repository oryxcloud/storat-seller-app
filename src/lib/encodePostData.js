export default params => {
    let formBody = [];

    Object.keys(params).forEach(param => {
        let key = encodeURIComponent(param);
        let value = encodeURIComponent(params[param]);

        formBody.push(`${key}=${value}`);
    });

    return formBody.join('&');
};
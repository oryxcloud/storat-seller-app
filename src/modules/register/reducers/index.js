const initialState = {
    isLoggedIn: false,
    toastType: 'danger',
    showToast: false,
    toastMessage: '',
    showSpinner: false,
    loginButtonText: 'Login',
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_LOGIN':
            return {
                ...state,
                email: action.payload.email,
                password: action.payload.password,
                isLoggedIn: action.payload.password === '123'
            }
        case 'USER_LOGIN_REQUEST':
            return {
                ...state,
                showToast: false,
                showSpinner: true,
                isLoggedIn: false,
                loginButtonText: 'Please wait...'
            }
        case 'USER_LOGIN_FAILED':
            return {
                ...state,
                toastType: 'danger',
                showToast: true,
                toastMessage: 'These credentials do not match our records.',
                showSpinner: false,
                isLoggedIn: false,
                loginButtonText: 'Login'
            }
        case 'USER_LOGIN_SUCCESS':
            return {
                ...state,
                toastType: 'success',
                showToast: true,
                toastMessage: 'You are successfully logged in.',
                showSpinner: true,
                isLoggedIn: true,
                loginButtonText: '...'
            }
        case 'USER_LOGIN_REDIRECT':
            return {
                ...state,
                toastType: 'success',
                showToast: true,
                toastMessage: 'Redirecting...',
                showSpinner: true,
                isLoggedIn: true
            }
        case 'UNEXPECTED_ERROR':
            return {
                ...state,
                toastType: 'danger',
                showToast: true,
                toastMessage: 'Whoops, we are facing some technical issues.',
                showSpinner: false,
                isLoggedIn: false,
                loginButtonText: 'Login'
            }
        default:
            return state;
    }
};

export default reducer;
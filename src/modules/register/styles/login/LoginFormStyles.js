export default {
    container: {
        padding: 20,
    },
    input: {
        height: 40,
        borderWidth: 1,
        borderColor: '#d3d3d3',
        borderRadius: 3,
        marginBottom: 5,
        fontSize: 14,
        color: '#000',
        padding: 10,
        paddingLeft: 15
    },
    inputContainer: {
        marginBottom: 10,        
        marginTop: 10,        
    },
    buttonContainer: {
        backgroundColor: '#f94e51',
        paddingVertical: 11,
        borderRadius: 3,
        marginTop: 25
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        // fontWeight: '700'
    },
    forgotPasswordContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: -5,
    },
    link: {
        color: '#2c82c3',
        fontSize: 12,
    }
}
export default {
    storatCover: {
        resizeMode: 'cover',
        width: null,
        height: 150
    }
};
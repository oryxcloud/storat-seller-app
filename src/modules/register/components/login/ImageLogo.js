import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native';
import logoImage from '@images/logo.png';

export default class ImageLogo extends Component {
    render() {
        return (
            <View style={styles.logoContainer}>
                <Image 
                    source={logoImage}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    logoContainer: {
        padding: 30,
        alignItems: 'center'
    },
    storatLogo: {
     
    }
});
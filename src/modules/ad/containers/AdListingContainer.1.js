import React, { Component } from 'react';
import { Linking, View, Text, Image, StyleSheet, ScrollView, TextInput, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';
import { Container, Header, Content, Card, CardItem, List, ListItem, Thumbnail, Button, Icon, Left, Body, Right, Form, Item, Input, Label } from 'native-base';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { Actions } from 'react-native-router-flux';
import ImageSlider from 'react-native-image-slider';
import StatusBarTemplate from '@templates/statusbar';
import ErrorLabel from '@templates/error-label';
import ModalTemplate from '@templates/ui/modal';
import store from 'react-native-simple-store';
import {email, empty} from '@lib/validation';
import post from '@lib/post';
import encodePostData from '@lib/encodePostData';
import ToastTemplate from '@templates/toast';
import CountDownTimer from '@templates/countdown';
import ImagePreview from 'react-native-image-preview';
import call from 'react-native-phone-call';
import FooterTabComponent from '@modules/common/components/footer';



export default class AdContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showContactSellerFormModal: false,
            adDetails: {},
            name: {error: false, value: ''},
            email: {error: false, value: ''},
            phone: {error: false, value: ''},
            message: {error: false, value: ''},
            toast: {ype: 'success', show: false, message: ''},
            formType: 'contact',
            revealContact: false,
            adImages: [],
            adAttributes: [],
            imageModal: {
                visible: false,
                uri: '',
                setVisibleToFalse: true
            }
        };

        store.get('storat.cache')
            .then(res => {
                this.setState({adDetails: res.listingDetails});
                this.fetchImages(res.listingDetails.id);
                Actions.refresh({title: res.listingDetails.translations.en.title});
            })
    }

    fetchImages(listingId) {
        let url = 'https://uae.storat.com/api/v2/ads/listing/images/' + listingId; 

        fetch(url)
            .then(res => res.json())
            .then(res => {
                this.setState({adImages: res});
            });
    }

    showContactForm(type) {

        this.setState({
            formType: (type === 'promotion') ? 'promotion' : 'contact',
            showContactSellerFormModal: true
        });

    }

    revealContact() {
        this.setState({revealContact: true});
    }

    validate(field, value) {
        switch(field) {
            case 'name':
                this.setState({
                    name: {
                        error: empty(value),
                        value: value
                    }
                }) 
                break;
            case 'phone': 
                this.setState({
                    phone: {
                        error: empty(value),
                        value: value
                    }
                })
            case 'email':
                this.setState({
                    email: {
                        error: ! email(value),
                        value: value
                    }
                }) 
                break;
            case 'message': 
                this.setState({
                    message: {
                        error: empty(value),
                        value: value
                    }
                })
        }
    }

    nameInvalid() {
        return empty(this.state.phone.value) || this.state.phone.error;
    }          
    phoneInvalid() {
        return empty(this.state.phone.value) || this.state.phone.error;
    } 

    emailInvalid() {
        return ! this.state.email.value.trim() || this.state.email.error;
    }          
    messageInvalid() {
        return empty(this.state.message.value) || this.state.message.error;
    }  

    formInvalid() {
        if(
            this.nameInvalid() || 
            this.phoneInvalid() ||
            this.emailInvalid() ||
            this.messageInvalid()
        ) {
            return true;
        }

        return false;
    }

    submit() {
        if(this.formInvalid()) {
            this.validate('name', this.state.name.value);
            this.validate('phone', this.state.phone.value);
            this.validate('email', this.state.email.value);
            this.validate('message', this.state.message.value);

            return;
        }

        this.postMessage();
        
    }

    postMessage() {
        let url = 'https://uae.storat.com/api/v2/ads/' + this.state.adDetails.id + '/contact';
        let formBody = encodePostData({
            user_name: this.state.name.value,
            user_phone: this.state.phone.value,
            user_email: this.state.email.value,
            user_message: this.state.message.value
        });

        post(url, formBody)
            .then(response => response.json())
            .then(response => {
                if(response.meta.code === 200) {
                    this.setState({
                        toast: {
                            type: 'success',
                            show: true,
                            message: 'hi'
                        }
                    });
                } else {
                    this.setState({
                        toast: {
                            type: 'danger',
                            show: true,
                            message: 'hi'
                        }
                    });
                }
            })
            .catch(err => {
               
            });
    }

    getFormattedPrice(num) {
        return num.toLocaleString();
    }

    getContactFormBody(type) {

        return (
            <View style={{flex: 1}}>

                {/* Title */}
                <Text style={[styles.bold, {alignSelf: 'center'}]}>
                    {this.state.formType !== 'promotion' ? 'Contact Seller' : 'Get Discount Voucher'}
                </Text>

                {/* Toast */}
                <ToastTemplate 
                    type={this.state.toast.type}
                    show={this.state.toast.show} 
                    message={this.state.toast.message} 
                />

                {/* Input Text: Name */}
                <View style={[styles.inputContainer]}>
                    <TextInput
                        style={styles.input}
                        autocapitalize="none"
                        autoCorrect={false}
                        keyboardType="email-address"
                        returnKeyType="next"
                        placeholder="Name: Pradeep Tiwari"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"
                        onChangeText={ text => this.validate('name', text) }
                    />
                    <ErrorLabel 
                        showError={this.state.name.error} 
                        message={'Please enter your name'} 
                    />
                </View>

                {/* Input Text: Phone */}                
                <View style={[styles.inputContainer]}>
                    <TextInput 
                        style={styles.input}
                        autocapitalize="none"
                        returnKeyType="go"
                        placeholder="Phone: +502-123 4322"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"                      
                        onChangeText={ text => this.validate('phone', text) }             
                    />
                    <ErrorLabel 
                        showError={this.state.phone.error} 
                        message={'Please enter your phone number'} 
                    />
                </View>

                {/* Input Text: Email */}                
                <View style={[styles.inputContainer]}>
                    <TextInput 
                        style={styles.input}
                        autocapitalize="none"
                        returnKeyType="go"
                        placeholder="Email: pradeep@storat.com"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"                      
                        onChangeText={ text => this.validate('email', text) }              
                    />
                    <ErrorLabel 
                        showError={this.state.email.error} 
                        message={'Please provide a valid email address'} 
                    />
                </View>

                {/* Input Text: Message */}   
                {this.state.formType !== 'promotion' ?             
                <View style={[styles.inputContainer]}>
                    <TextInput 
                        style={styles.input}
                        autocapitalize="none"
                        returnKeyType="go"
                        placeholder="Message: Write your message here."
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"                      
                        onChangeText={ text => this.validate('message', text) }            
                    />
                    <ErrorLabel 
                        showError={this.state.message.error} 
                        message={'Please write your message for the seller'} 
                    />
                </View>
                : null}

                {/* Submit Button*/}      
                <TouchableOpacity onPress={() => {this.submit()}} style={[styles.buttonContainer, {marginTop: 10}]}>
                    {this.props.showSpinner ?
                        <ActivityIndicator 
                            animating={true}
                            color={'#fff'}
                        />
                    :
                        
                        
                        <Text style={styles.buttonText}>
                            Send Message
                        </Text>
                        
                    }
                </TouchableOpacity>  

                {/* Cancel */}
                <Text 
                    onPress={() => this.setState({showContactSellerFormModal: false, toast: {show: false}})}
                    style={[styles.link, {alignSelf: 'center', marginTop: 10}]}
                >
                    Cancel
                </Text>
            </View>
        );

    }

    showImageModal(uri) {
        this.setState({
            imageModal: {
                visible: true,
                uri: uri,
                setVisibleToFalse: false
            }
        });
    }

    populateAttributes() {
        let adAttributes = this.state.adDetails.custom_attributes;
        let allAttributes = {};
        let attributesMap = {};

        store.get('storat.cache')
            .then(data => {
                allAttributes = data.attributes;
            })
            .then(() => {
                Object.keys(adAttributes).forEach(k => {
                    [categoryId, optionId] = k.split('-');
                    attributesMap[categoryId] = optionId;
                })

                console.log(attributesMap);
            });
        
    }

    render() {

        return (
            <Container style={[styles.container]}>
                {/* Status bar */}
                <StatusBarTemplate />
                {/* Main Page */}
                <Content>
                        {Object.values(this.state.adDetails).length !== 0 ?
                        <Card>
                            <CardItem cardBody style={[styles.imageThumbnailsContainer]}>
                                <ImageSlider 
                                    images={this.state.adImages} 
                                    onPress={(imageObj, index) => {this.showImageModal(imageObj.image)}}
                                />
                                <ImagePreview visible={this.state.imageModal.visible} source={{uri: this.state.imageModal.uri}} close={() => this.setState({imageModal: {visible: false}}) } />
                            </CardItem>
                            <CardItem>
                                <Text style={[styles.adListingHead]}>
                                    {this.state.adDetails.translations.en.title}
                                </Text>
                            </CardItem>
                            
                            <CardItem>
                                {/* <Text style={[styles.adListingLocationHead]}>
                                    Khalifa City, Abu Dhabi City, Abu Dhabi
                                    {this.state.adDetails.location.region.translations.en.name}
                                </Text> */}
                            </CardItem>
                            
                            <CardItem style={[styles.adSnippetContainer]}>
                                <Text style={[styles.adListingPrice]}>
                                    {/* {this.state.adDetails.discount_price} AED */}
                                    {this.getFormattedPrice(1500)} AED
                                </Text>

                                <View>
                                    <Text style={[styles.adDiscountPrice]}>
                                        <Text style={[styles.lineStroke]}>3000 AED </Text>
                                        You Save: <Text style={[styles.bold, styles.black]}>1500 AED</Text>
                                    </Text>
                                </View>


                                {/* Promotion Text */}
                                <View style={[styles.promotionContainer]}>
                                    <Text style={[styles.red, styles.lightBold]}>Promotion will end in </Text>
                                    <CountDownTimer
                                        //date={new Date(parseInt(endTime))}
                                        date="2018-10-28T00:00:00+00:00"
                                        days={{plural: 'd ',singular: 'd '}}
                                        hours='h'
                                        mins='m'
                                        segs='s'

                                        daysStyle={styles.time}
                                        hoursStyle={styles.time}
                                        minsStyle={styles.time}
                                        secsStyle={styles.time}
                                        firstColonStyle={styles.colon}
                                        secondColonStyle={styles.colon}
                                        color={[styles.red, styles.bold, styles.italic]}
                                    />
                                </View>

                                {/* Discount Voucher Button */}
                                <View style={[styles.getDiscountVoucherButtonContainer]}>
                                    <Button 
                                        small success full style={[styles.dicountVoucherButton]} 
                                        onPress={() => this.showContactForm('promotion')}
                                    >
                                        <Text style={[styles.contactSellerButtonText]}>
                                            Get Discount Voucher Now!
                                        </Text>
                                    </Button>

                                    {/* Claims text */}
                                    <Text style={[styles.orange, styles.small]}>Storat discount vouchers claimed 52 times in the last 4 hours</Text>
                                </View>

                                <View style={[styles.buttonsContainer]}>
                                    {/* Reveal Number */}
                                    <View style={[styles.revealNumberButtonContainer]}>
                                        <Button 
                                            small full style={[styles.revealNumberButton]} 
                                            onPress={() => { this.revealContact(); call({number: '9712233445566', prompt: true})}}
                                        >
                                            <Text style={[styles.contactSellerButtonText]}>
                                                {this.state.revealContact ?
                                                    '+971-2233445566'
                                                : 
                                                    '+971-266XXXX (Reveal number)'
                                                }
                                            </Text>
                                        </Button>
                                    </View>

                                    {/* Contact Seller */}
                                    <View style={[styles.contactSellerButtonContainer]}>
                                        <Button 
                                            small danger full style={[styles.contactSellerButton]} 
                                            onPress={() => this.showContactForm()}
                                        >
                                            <Icon name='mail' />
                                            <Text style={[styles.contactSellerButtonText]}>
                                                Contact Seller
                                            </Text>
                                        </Button>
                                    </View>
                                </View>
                            </CardItem>

                            
                            {/* Ad Details Heading */}
                            <CardItem>
                                <Text style={[styles.adDetailsHead]}>
                                    Details
                                </Text>
                            </CardItem>

                            <CardItem>
                                <Text style={[styles.thin]}>
                                    {this.state.adDetails.translations.en.description}
                                </Text>
                            </CardItem>


                            {/* Additional Details */}
                            <CardItem>
                                <Text style={[styles.adDetailsHead]}>
                                    Aditional Details
                                </Text>
                            </CardItem>
                            
                            <Content style={[]}>
                                <List>
                                    <ListItem>
                                        <Left>
                                            <Text style={[styles.thin]}>
                                                Wheels
                                            </Text>
                                        </Left>
                                        <Body>
                                            <Text></Text>
                                        </Body>
                                        <Right>
                                            <Text style={[styles.bold, styles.thin]}t>4</Text>
                                        </Right>
                                    </ListItem>

                                    <ListItem>
                                        <Left>
                                            <Text style={[styles.thin]}>
                                                Year
                                            </Text>
                                        </Left>
                                        <Body>
                                            <Text></Text>
                                        </Body>
                                        <Right>
                                            <Text style={[styles.bold, styles.thin]}>2017</Text>
                                        </Right>
                                    </ListItem>

                                    <ListItem>
                                        <Left>
                                            <Text style={[styles.thin]}>
                                                Number of Doors
                                            </Text>
                                        </Left>
                                        <Body>
                                            <Text></Text>
                                        </Body>
                                        <Right>
                                            <Text style={[styles.bold, styles.thin]}>4</Text>
                                        </Right>
                                    </ListItem>

                                    <ListItem>
                                        <Left>
                                            <Text style={[styles.thin]}>
                                                Transmission Type
                                            </Text>
                                        </Left>
                                        <Body>
                                            <Text></Text>
                                        </Body>
                                        <Right>
                                            <Text style={[styles.bold, styles.thin]}>Auto</Text>
                                        </Right>
                                    </ListItem>

                                    <ListItem>
                                        <Left>
                                            <Text style={[styles.thin]}>
                                                Color
                                            </Text>
                                        </Left>
                                        <Body>
                                            <Text></Text>
                                        </Body>
                                        <Right>
                                            <Text style={[styles.bold, styles.thin]}>White</Text>
                                        </Right>
                                    </ListItem>
                                </List>
                                {this.populateAttributes()}
                            </Content>

                        
                            {/* Map */}
                            {/* <CardItem>
                                <Text style={[styles.adDetailsHead]}>
                                    Map
                                </Text>
                            </CardItem>
                            
                            
                            <Content style={[]}>
                            <MapView
                                initialRegion={{
                                latitude: 37.78825,
                                longitude: -122.4324,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                                }}
                            />
                            </Content> */}

                            <View style={[styles.verticalGap]}></View>

                        </Card>
                        : null }

                    
                </Content>

                <ModalTemplate 
                    body={this.getContactFormBody()}
                    visible={this.state.showContactSellerFormModal} 
                />                                    
            

                {/* FooterTabs */}
                {/* <FooterTabComponent /> */}

            </Container>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        // paddingLeft: 10,
        // paddingRight: 10,
    },
    imageThumbnailsContainer: {
        padding: 15
    },
    adCountTitle: {
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
    },
    adListingHead: {
        fontSize: 15,
        fontWeight: '500',
    },
    adListingLocationHead: {
        marginTop: -15,
        color: '#646464',
        fontSize: 12
    },
    adListingPrice: {
        fontSize: 18,
        fontWeight: '500',
        marginBottom: 5,
    },
    adDiscountPrice: {
        fontSize: 13,
        color: '#646464'
    },
    bold: {
        fontWeight: 'bold'
    },
    adSnippetContainer: {
        alignItems: 'flex-start',
        flexDirection: 'column',
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    promotionContainer: {
        flexDirection: 'row',
        marginTop: 10
    },
    buttonsContainer: {
        backgroundColor: '#f7f8fb', 
        alignSelf: 'stretch', 
        padding: 20, 
        borderColor: '#f5f6fa', 
        borderRadius: 3
    },
    getDiscountVoucherButtonContainer: {
        alignSelf: 'stretch',
        marginBottom: 10,
        marginTop: 10
    },
    revealNumberButtonContainer: {
        alignSelf: 'stretch',
        marginBottom: 10,
        // marginTop: 10
    },
    dicountVoucherButton: {
        marginBottom: 10
    },  
    contactSellerButtonContainer: {
        alignSelf: 'stretch',
    },
    contactSellerButton: {
        // alignSelf: 'flex-end',
        backgroundColor: '#f94e51',
        borderRadius: 2
    },
    contactSellerButtonText: {
        color: '#fff',
        alignSelf: 'flex-end'
    },
    adDetailsHead: {
        fontWeight: '700',
        marginTop: 30
    },
    verticalGap: {
        paddingTop: 30
    },
    bold: {
        fontWeight: 'bold',
    },
    small: {
        fontSize: 11
    },
    medium: {
        fontSize: 14
    },
    lightBold: {
        fontWeight: '500'
    },
    black: {
        color: '#000'
    },
    red: {
        color: '#fa4954'
    },
    orange: {
        color: '#f17a03'
    },
    italic: {
        fontStyle: 'italic'
    },
    lineStroke: {
        textDecorationLine: 'line-through',
    },
    thin: {
        fontSize: 14
    },
    input: {
        height: 40,
        borderWidth: 1,
        borderColor: '#d3d3d3',
        borderRadius: 3,
        marginBottom: 5,
        fontSize: 14,
        color: '#000',
        padding: 10,
        paddingLeft: 15
    },
    inputContainer: {
        marginBottom: 10,        
        marginTop: 10,        
    },
    buttonContainer: {
        backgroundColor: '#f94e51',
        paddingVertical: 11,
        borderRadius: 3,
        marginTop: 25
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        // fontWeight: '700'
    },
    forgotPasswordContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: -5,
    },
    link: {
        color: '#2c82c3',
        fontSize: 12,
    },
    time: {

    },
    colon: {

    }
});
import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ScrollView, ActivityIndicator, Modal, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right, List, ListItem } from 'native-base';
import { Actions } from 'react-native-router-flux';
import store from 'react-native-simple-store';
import SelectCategoryContainer from '@store/containers/select-category';
import SelectLocationContainer from '@store/containers/select-location';
import CustomAttributesContainer from '@store/containers/custom-attributes';
import PriceComponent from '../components/PriceComponent';
import RangeComponent from '../components/RangeComponent';
import DropDownComponent from '../components/DropDownComponent';
import OptionBoxComponent from '../components/OptionBoxComponent';
import { SelectionBoxTemplate, ButtonTemplate } from '@templates/form';
import { CardTemplate, PickerTemplate } from '@templates/ui';
import StatusBarTemplate from '@templates/statusbar';
import FooterTabComponent from '@modules/common/components/footer';


/**
 
-------------------------------------------------
Filter Options Format:
-------------------------------------------------
country = "UAE"
c = "properties/for-rent/apartments-for-rent"
min_price = "111"
max_price = "1111111"
attr-drop-2 = "z"
attr-min-10 = "1212"
attr-max-10 = "121212"
attr-65 = "sdasas"
attr-drop-47 = "z"
attr-min-48 = "21"
attr-max-48 = "222"
attr-min-11 = "1"
attr-max-11 = "1"
attr-ch-13-3 = "on"
attr-ch-13-6 = "on"
attr-ch-13-8 = "on"

 */

export default class AdFilterContainer extends Component {

    constructor(props) {
        super(props);

        this.setCategory = this.setCategory.bind(this);
        this.hidePickerItems = this.hidePickerItems.bind(this);
        this.populateModalItem = this.populateModalItem.bind(this);
        this.storeSelectedItem = this.storeSelectedItem.bind(this);

        this.state = {
            isLoading: true,
            showPicker: false,
            pickerItems: [],
            locations: [],
            categories: [],
            attributes: [],
            locationFilters: [],
            categoriesFilters: [],
            selectedLocationFilters: [],
            modalItems: [],
            attributesData: {},
            selectedCategoryId: -1,
            filterOptions: {
                country: '',
                state: '',
                city: '',
                region: '',
                categoryId: -1
            }
        };

        store.get('storat.cache')
            .then(res => {
                this.state.locations = res.locations;
                this.state.categories = res.categories;
                this.state.filterOptions['c'] = res.categoryUrl;
                this.state.selectedCategoryId = res.categoryId;
                this.state.filterOptions.categoryId = res.categoryId;
                this.generateLocationFilters('countries', res.country);
                this.generateCategoriesFilters(res.categories, res.categoryId);                    

                fetch('https://uae.storat.com/api/v2/attributes/' + res.categoryId)
                    .then(res => res.json())
                    .then(res => {
                        this.generateAttributes(res);
                    });
            });
    }

    generateCategoriesFilters(categories, categoryId) {
        categories.forEach(e => {
            if(e.id == categoryId) {
                this.state.categoriesFilters.push(
                    <SelectionBoxTemplate 
                        key={e.nameEn}
                        text={e.nameEn}
                        onPress={() => this.showPickerItemsForCategories(e.parentId)}                
                    />
                );

                // this.state.categoriesFilters.reverse();
                
                if(e.parentId !== null) {
                    this.generateCategoriesFilters(categories, e.parentId);
                } else {
                    this.state.categoriesFilters.reverse();
                }
            }
        });

        // this.setState({isLoading: false});
    }

    getTopMostParentCategoryId(categoryId) {
        let topmostCategoryId = -1;

        this.state.categories.forEach(e => {
            if(e.id == categoryId) {
                if(e.parentId !== null) {
                    this.getTopMostParentCategoryId(e.parentId);
                } else {
                    topmostCategoryId = e.id;
                }
            }
        });

        return topmostCategoryId;
    }

    generateAttributes(data) {
        data.category_attributes.forEach(e => {
            this.mapAttributeToComponent(e.attribute);
        });

        // this.state.categoriesFilters.reverse();
        this.setState({isLoading: false, attributesData: data});
    }

    mapAttributeToComponent(attributes) {
        switch(attributes.type) {
            case 'Range':
                this.pushRangeComponent(attributes);
                return;
            case 'Drop Down':
                this.pushDropDownComponent(attributes);
                return;
            case 'Option Box':
                this.pushOptionBoxComponent(attributes);
                return;
        }
    }

    pushRangeComponent(attributes) {
        const data = attributes.current_translation[0];

        this.state.attributes.push(
            <RangeComponent 
                styles={styles}
                data={data}
                key={data.id}
                storeSelectedItem={this.storeSelectedItem}                
            />
        );
    }

    pushDropDownComponent(attributes) {
        const data = attributes.current_translation[0];
     
        this.state.attributes.push(
            <DropDownComponent 
                styles={styles}
                data={data}
                key={data.id}
                populateModalItem={this.populateModalItem}
                storeSelectedItem={this.storeSelectedItem}
            />
        );
    }

    pushOptionBoxComponent(attributes) {
        const data = attributes.current_translation[0];

        this.state.attributes.push(
            <OptionBoxComponent 
                data={data}
                styles={styles}
                key={data.id}
                storeSelectedItem={this.storeSelectedItem}
            />
        ); 
    }

    setCategory(categoryId) {
        this.state.categoriesFilters = [];
        this.generateCategoriesFilters(this.state.categories, categoryId);
        this.updateAttributes(categoryId);
        this.state.categories.forEach(e => {
            if(e.id == categoryId) {
                this.state.filterOptions['c'] = e.url;
            }
        });
        this.setState({
            pickerItems: [],
            showPicker: false,
        });
    }

    updateAttributes(categoryId) {
        fetch('https://uae.storat.com/api/v2/attributes/' + categoryId)
            .then(res => res.json())
            .then(res => {
                this.state.attributes = [];
                this.state.attributesData = [];
                this.generateAttributes(res);
            });
    }

    hasSubCategories(categoryId) {
        let result = false;

        this.state.categories.forEach(e => {
            if(e.parentId === categoryId) {
                result = true;
                return;
            }
        });

        return result;
    }

    showPickerItemsForCategories(parentId) {
        let items = [];
        
        this.state.categories.forEach(e => {
            if(e.parentId === parentId) {
                this.state.pickerItems.push(
                    <ListItem key={e.id} onPress={() => {this.state.selectedCategoryId = e.id; this.state.filterOptions.categoryId = e.id; this.setCategory(e.id)}}>
                        <Text>{e.nameEn}</Text>
                    </ListItem>
                );
            }
        })

        this.setState({ showPicker: true });
    }

    generateLocationFilters(locationType, locationId, locationName) {
        switch(locationType) {
            case 'countries':
                this.state.filterOptions.country = locationName;
                this.state.filterOptions.state = '';
                this.state.filterOptions.city = '';
                this.state.filterOptions.region = '';
                this.pushCountriesFilters(locationId);
                break;
            case 'states':
                this.state.filterOptions.state = locationName;
                this.state.filterOptions.city = '';
                this.state.filterOptions.region = '';
                this.pushStatesFilters(locationId);               
                break;
            case 'cities':
                this.state.filterOptions.city = locationName;
                this.state.filterOptions.region = '';
                this.pushCitiesFilters(locationId);            
                break;
            case 'regions':
                this.state.filterOptions.region = locationName;                
                this.pushRegionsFilters(locationId);               
                break;
        }

        console.log(this.state.filterOptions);
    }

    pushCountriesFilters(countryId) {
        this.state.locationFilters = [];

        this.state.locations.countries.forEach(e => {
            if(e.id == countryId) {
                this.state.locationFilters.push(
                    <SelectionBoxTemplate 
                        key={'country1-' + e.nameEn}
                        text={e.nameEn}
                        onPress={() => this.showPickerItemsForLocations('countries', null)}                
                    />
                );
            }
        });

        this.state.locationFilters.push(
            <SelectionBoxTemplate 
                key={'state1-' + 'All States'}
                text={'All States'}
                onPress={() => this.showPickerItemsForLocations('states', countryId)}                
            />
        );
    }

    pushStatesFilters(stateId) {
        this.state.locationFilters.splice(1);

        this.state.locations.states.forEach(e => {
            if(e.id == stateId) {
                this.state.locationFilters.push(
                    <SelectionBoxTemplate 
                        key={'state2-' + e.nameEn}
                        text={e.nameEn}
                        onPress={() => this.showPickerItemsForLocations('states', e.countryId)}                
                    />
                );
            }
        });

        this.state.locations.cities.forEach(e => {
            if(e.stateId == stateId) {
                this.state.locationFilters.push(
                    <SelectionBoxTemplate 
                        key={'city1-' + 'All Cities'}
                        text={'All Cities'}
                        onPress={() => this.showPickerItemsForLocations('cities', stateId)}                
                    />
                );

                return;
            }
        });
    }

    pushCitiesFilters(cityId) {
        this.state.locationFilters.splice(2);

        this.state.locations.cities.forEach(e => {
            if(e.id == cityId) {
                this.state.locationFilters.push(
                    <SelectionBoxTemplate 
                        key={'city2-' + e.nameEn}
                        text={e.nameEn}
                        onPress={() => this.showPickerItemsForLocations('cities', e.stateId)}                
                    />
                );
            }
        });

        this.state.locations.regions.forEach(e => {
            this.state.locationFilters.push(
                <SelectionBoxTemplate 
                    key={'region1-' + 'All Regions'}
                    text={'All Regions'}
                    onPress={() => this.showPickerItemsForLocations('regions', cityId)}                
                />
            );
        });
        
    }

    pushRegionsFilters(regionId) {
        this.state.locationFilters.splice(3);

        this.state.locations.regions.forEach(e => {
            if(e.id == regionId) {
                this.state.locationFilters.push(
                    <SelectionBoxTemplate 
                        key={'region2-' + e.nameEn}
                        text={e.nameEn}
                        onPress={() => this.showPickerItemsForLocations('regions', e.cityId)}                
                    />
                );
            }
        });
    }

    showPickerItemsForLocations(locationType, locationId) {
        switch(locationType) {
            case 'countries':
                this.populateCountries();
                break;
            case 'states':
                this.populateStates(locationId);
                break;
            case 'cities':
                this.populateCities(locationId);
                break;
            case 'regions':
                this.populateRegions(locationId);
        }
    }

    populateCountries() {
        this.state.locations.countries.forEach(e => {  
            this.state.pickerItems.push(
                <ListItem key={e.id} onPress={ () => { this.generateLocationFilters('countries', e.id, e.nameEn); this.hidePickerItems()} }>
                    <Text>{e.nameEn}</Text>
                </ListItem>
            );  
        })

        this.setState({ showPicker: true });    
    }

    populateStates(countryId) {
        this.state.locations.states.forEach(e => {
            if(e.countryId === countryId) {
                this.state.pickerItems.push(
                    <ListItem key={e.id} onPress={ () => { this.generateLocationFilters('states', e.id, e.nameEn); this.hidePickerItems()} }>
                        <Text>{e.nameEn}</Text>
                    </ListItem>
                );
            }
        });

        this.setState({ showPicker: true }); 
    }

    populateCities(stateId) {
        this.state.locations.cities.forEach(e => {
            if(e.stateId === stateId) {
                this.state.pickerItems.push(
                    <ListItem key={e.id} onPress={ () => { this.generateLocationFilters('cities', e.id, e.nameEn); this.hidePickerItems()} }>
                        <Text>{e.nameEn}</Text>
                    </ListItem>
                );
            }
        });

        this.setState({ showPicker: true }); 
    }

    populateRegions(cityId) {
        this.state.locations.regions.forEach(e => {
            if(e.cityId === cityId) {
                this.state.pickerItems.push(
                    <ListItem key={e.id} onPress={ () => { this.generateLocationFilters('regions', e.id, e.nameEn); this.hidePickerItems()} }> 
                        <Text>{e.nameEn}</Text>
                    </ListItem>
                );
            }
        });

        this.setState({ showPicker: true }); 
    }

    hidePickerItems() {
        this.setState({
            pickerItems: [],
            showPicker: false
        });
    }

    populateModalItem(itemName, callback) {
        let options = [];

        this.state.attributesData.category_attributes.forEach(e => {
            if(e.attribute.current_translation[0].name === itemName) {
                options = e.attribute.current_translation[0].options;
            }
        });

        this.setState({modalItems: options, callback: callback});
    }

    showModal() {
        if(this.state.modalItems.length === 0) {
            return null;
        }

        let items = [];

        this.state.modalItems.forEach(e => {
            items.push(
                <ListItem key={e} onPress={() => this.setSelectedOption(e)}>
                    <Text>{e}</Text>
                </ListItem>
            )
        });

        return this.renderModal(items);
    }

    renderModal(items) {
        return (
            <Modal
                animationType={"slide"}
                transparent={false}
                visible={true}   
            >
                <Container>
                    <Header>
                        <Left>
                            <TouchableOpacity 
                                style={[styles.buttonContainer, styles.buttonSmall, styles.buttonBlue]}
                                onPress={() => this.setState({modalItems: []})}
                            >
                                <Text style={[styles.buttonText]}>
                                    Back
                                </Text>
                            </TouchableOpacity>
                        </Left>    
                    </Header>
                    <Content>
                        <List>
                            {items}
                        </List>                           
                    </Content>
                </Container>
            </Modal>
        );
    }

    setSelectedOption(option) {
        this.setState({selectedOption: option, modalItems: []});
        this.state.callback(option);
    }

    storeSelectedItem(itemName, itemValue) {
        this.state.attributesData.category_attributes.forEach(e => {
            if(e.attribute.name === itemName) {
                if(e.attribute.type === 'Drop Down') {
                    this.state.filterOptions['attr-drop-' + e.attribute_id] = e.attribute.options.indexOf(itemValue) || 'z';
                }

                if(e.attribute.type === 'Option Box') {
                    let filterKey = 'attr-ch-' + e.attribute_id + '-' + e.attribute.options.indexOf(itemValue);
                    this.state.filterOptions[filterKey] = (this.state.filterOptions[filterKey] === 'on') ? 'off' : 'on'
                }

                if(e.attribute.type === 'Range') {
                    this.state.filterOptions['attr-' + itemValue.label + '-' + e.attribute_id] = itemValue.value;
                }
            }
        });

        if(itemName === 'Price') {
            this.state.filterOptions[itemValue.label + '_price'] = itemValue.value;            
        }

        console.log(this.state.filterOptions)
    }

    showIndicator() {
        return (
            <View>
                {/* Spacer */}
                <View style={styles.spacer}></View>

                {/* Indicator */}
                <ActivityIndicator 
                    animating={true}
                    color={'#646464'}
                />
            </View>
        );
    }

    getCategoriesFilters() {
        let parentId = null;

        //this.state.categoriesFilters.reverse();

        if(this.hasSubCategories(this.state.selectedCategoryId)) {

            this.state.categories.forEach(e => {
                if(e.id == this.state.selectedCategoryId) {
                    parentId = e.parentId;
                }
            })

            this.state.categoriesFilters.push(
                <SelectionBoxTemplate 
                        key={0}
                        text={'All Categories'}
                        onPress={() => this.showPickerItemsForCategories(this.state.selectedCategoryId)}                
                    />
            );
            // this.state.categoriesFilters.reverse();
            
        }

        return this.state.categoriesFilters;
    }

    getPriceFields() {
        return (
            <PriceComponent 
                styles={styles}
                key={'price'}
                storeSelectedItem={this.storeSelectedItem}                
            />
        );
    }

    showFilteredAds() {
        store.update('storat.cache', {filterOptions: this.state.filterOptions})
            .then(() => Actions.adListings());
    }

    showContent() {
        return (
            <Content style={[styles.content]}>

                {/* Locations Filter */}
                <View style={[styles.filterBox]}>
                    <Text style={[styles.filterTypeHeading]}>
                        Location
                    </Text>
                    
                    <View style={[styles.filterOptionsBox]}>
                        {this.state.locationFilters}
                    </View>
                </View>

                {/* Categories Filter */}
                <View style={[styles.filterBox]}>
                    <Text style={[styles.filterTypeHeading]}>
                        Categories
                    </Text>
                    
                    <View style={[styles.filterOptionsBox]}>
                        {this.getCategoriesFilters()}
                    </View>
                </View>

                {/* Search Button */}
                <Button full danger style={[styles.searchButton]} onPress={ () => this.showFilteredAds() }>
                    <Text style={[styles.searchButtonText]}>
                        Search
                    </Text>
                </Button>

                {/* Advanced Filter */}
                <View style={[styles.filterBox]}>
                    <Text style={[styles.filterTypeHeading]}>
                        Advanced Options
                    </Text>
                    
                    <View style={[styles.filterOptionsBox, {paddingTop: -30}]}>
                        {this.getPriceFields()}
                        {this.state.attributes}
                    </View>
                </View>

                {/* Search Button */}
                <Button full danger style={[styles.searchButton]} onPress={ () => this.showFilteredAds() }>
                    <Text style={[styles.searchButtonText]}>
                        Search
                    </Text>
                </Button>

            </Content>
        );
    }

    render() {

        return (
            <Container style={[styles.container]}>
                
                {/* Status bar */}
                <StatusBarTemplate />
                
                {/* Content */}
                {this.state.isLoading ? this.showIndicator() : this.showContent()}
        
                {/* FooterTabs */}
                {/* <FooterTabComponent /> */}

                {/* Modal */}
                {this.showModal()}

                {/* Picker */}
                <PickerTemplate 
                    show={this.state.showPicker}
                    items={this.state.pickerItems}
                    closePicker={this.hidePickerItems} 
                    buttonTitle='Back'
                    buttonSize='small'
                    buttonColor='blue' 
                />
                
          </Container>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    content: {
        padding: 10,
    },
    contentHeading: {
        backgroundColor: '#F8F8F8',
        padding: 15,
    },
    labelHeading: {
        marginBottom: 5,
    },
    heading: {
        fontSize: 13,
        fontWeight: '700',
        color: '#000'
    },
    blurb: {
        color: '#a2a2a2',
        fontSize: 16,
        marginBottom: 15
    },
    inputText: {
        borderBottomColor: '#646464',
        borderBottomWidth: 1
    },
    input: {
        // height: 60,
        // borderBottomWidth: 1,
        // borderBottomColor: '#d3d3d3',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#d3d3d3',
        marginBottom: 5,
        fontSize: 13,
        color: '#000',
        padding: 10,
        paddingLeft: 10
    },
    selectionInput: {
        color: '#94a6b1',
    },
    textArea: {
        height: 120,
        paddingTop: 10,
    },
    inputContainer: {
        marginTop: 30,      
        // padding: 15  
    },
    submitButtonContainer: {
        alignItems: 'center',
    },
    label: {
        color: '#000',
        fontSize: 18
    },
    buttonContainer: {
        backgroundColor: '#e24248',
        paddingVertical: 10,
        width: 120,
        borderRadius: 3
    },
    buttonSmall: {
        paddingVertical: 3,
        width: 60
    },
    buttonBlue: {
        backgroundColor: '#1470b9'
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    spacer: {
        height: 80
    },
    cancelIcon: {
        fontSize: 16,
    },
    filterBox: {
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 3,
        marginBottom: 30
    },
    filterTypeHeading: {
        fontWeight: 'bold',
        backgroundColor: '#f9f9f9',
        padding: 10
    },
    filterOptionsBox: {
        padding: 20,
    },
    searchButton: {
        borderRadius: 3,
        marginTop: -10,
        marginBottom: 30,
    },
    searchButtonText: {
        color: '#fff',
        fontWeight: 'bold'
    }
});
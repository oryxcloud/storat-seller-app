import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ActivityIndicator } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Actions } from 'react-native-router-flux';
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import noImageAvailable from '@images/no_image_available.jpeg';
import FavoriteButtonComponent from '../components/FavoriteButtonComponent';
import SearchBarComponent from '@modules/common/components/search';
import FooterTabComponent from '@modules/common/components/footer';


export default class AdContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categoryId: null,
            ads: { results: [] },
            showAdsLoadingSpinner: true,
            favoriteAds: {}
        };

        this.populateAds();

    }

    populateAds(url) {
        // scrolls to top
        //this.refs.scrollView.scrollTo({x: 0, y: 0, animated: true}); 

        store.get('storat.cache')
            .then(res => {
                store.update('storat.cache', { filterOptions: null });
                return res;
            })
            .then(res => {

                // show filtered ads
                if (res.filterOptions) {
                    let queryString = '';

                    Object.keys(res.filterOptions).forEach(filter => {
                        queryString += filter + '=' + encodeURIComponent(res.filterOptions[filter]) + '&';
                    });

                    url = 'https://uae.storat.com/api/v2/ads/search?' + queryString;
                    this.setState({ categoryId: res.filterOptions.categoryId });
                } else { // show ads by category
                    url = url || 'https://uae.storat.com/api/v2/ads/' + res.categoryId;
                    this.setState({ categoryId: res.categoryId });
                }
                console.log(url);
                fetch(url)
                    .then(res => res.json())
                    .then(res => {
                        console.log(res)
                        this.setState({ ads: res });
                    }).then(() => {

                    });

            });
    }

    setListingDetails(listing) {
        store.update('storat.cache', { listingDetails: listing })
            .then(() => { Actions.adListing() });
    }

    renderAds() {
        let result = [];

        this.state.ads.results.forEach(e => {

            if (e.id) { // we need this check because some ads do have id = undefined 

                result.push(
                    <Card key={e.id || i++} style={[styles.adCardContainer]}>
                        <CardItem button onPress={() => this.setListingDetails(e)}>
                            <Image source={e.image.path ? { uri: 'https://cdn04.storat.com/' + e.image.path } : noImageAvailable} style={{ backgroundColor: '#efefef', height: 120, width: null, flex: 1 }} />
                        </CardItem>
                        <CardItem>
                            <Text style={[styles.adListingHead]}>
                                {e.translations.en ? e.translations.en.title : null}
                            </Text>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Button transparent>
                                    <Icon name="pin" />
                                    <Text> {e.location.country.translations.en.name}</Text>
                                </Button>
                            </Left>
                            <Body>
                                <Button transparent>
                                    <Text>{e.discount_price || e.price} AED</Text>
                                </Button>
                            </Body>
                            <Right>
                                {/* <Button transparent small>
                                    <Icon name="heart" active style={{ color: "#ccc", fontSize: 24 }} />
                                </Button> */}
                                <FavoriteButtonComponent 
                                    adId={e.id}
                                    adTitle={e.translations.en ? e.translations.en.title : ''}
                                />
                            </Right>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Button block small primary onPress={() => { this.setListingDetails(e) }}>
                                    <Text style={{ color: '#fff' }}>Contact</Text>
                                </Button>
                            </Body>
                        </CardItem>
                    </Card>
                );
            }
        });

        // this.setState({showAdsLoadingSpinner: false});

        return result;
    }

    render() {
        return (
            <Container style={[styles.container]}>
                {/* Status bar */}
                <StatusBarTemplate />

                {/* SearchBar */}
                {/* <SearchBarComponent /> */}

                {/* Main Page */}
                <Content style={[styles.content]}>
                    <Grid>
                        {/* Ads Count Title */}
                        <Row style={[styles.adCountTitle]}>

                            {/* show title else loading */}
                            {this.state.ads.title ?
                                <Text style={[styles.bold]}>{this.state.ads.title.slice(0, 30) + '...'}</Text>
                                :
                                <View style={[styles.loadingAdsContainer]}>
                                    <ActivityIndicator size="small" color="#000" />
                                    <Text> Loading Ads...</Text>
                                </View>
                            }

                            {this.state.ads.title ?
                                <Button small dark iconLeft style={[styles.searchButton]} onPress={() => { Actions.adFilter() }}>
                                    <Icon name='list' />
                                    <Text style={{ color: '#fff' }}>Filter</Text>
                                </Button>
                                : null}
                        </Row>
                        {/* Ads Listing */}
                        {this.state.ads.title ? this.renderAds() : null}

                        {/* <- Prev | Next -> */}
                        <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'center', margin: 15 }}>
                            {this.state.ads.prevUrl ?
                                <Button small iconLeft style={[styles.searchButton]} onPress={() => { this.populateAds(this.state.ads.prevUrl) }}>
                                    <Icon name='arrow-round-back' />
                                    <Text style={{ color: '#fff' }}>Prev</Text>
                                </Button>
                                : null}

                            {this.state.ads.nextUrl ?
                                <Button small iconRight style={[styles.searchButton]} onPress={() => { this.populateAds(this.state.ads.nextUrl) }}>
                                    <Text style={{ color: '#fff' }}>Next</Text>
                                    <Icon name='arrow-round-forward' />
                                </Button>
                                : null}
                        </View>

                    </Grid>

                </Content>

                {/* FooterTabs */}
                <FooterTabComponent />

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {

    },
    content: {
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
    },
    adCountTitle: {
        justifyContent: 'space-between',
        paddingTop: 20,
        paddingBottom: 10,
    },
    loadingAdsContainer: {
        flexDirection: 'row',
    },
    adListingHead: {
        fontWeight: 'bold'
    },
    adCardContainer: {
        marginBottom: 10,
        borderColor: '#c5c5c5'
    },
    searchButton: {
        // marginTop: -5, 
        marginLeft: 10,
        height: 28
    },
    bold: {
        fontWeight: '700'
    },
    lineStroke: {
        textDecorationLine: 'line-through',
    },
});
import React, { Component } from 'react';
import { Modal, Linking, View, Text, Image, StyleSheet, ScrollView, TextInput, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';
import { Container, Header, Content, Card, CardItem, List, ListItem, Thumbnail, Button, Icon, Left, Body, Right, Form, Item, Input, Label } from 'native-base';
// import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { Actions } from 'react-native-router-flux';
import ImageSlider from 'react-native-image-slider';
import Gallery from 'react-native-image-gallery';
import StatusBarTemplate from '@templates/statusbar';
import ErrorLabel from '@templates/error-label';
import ModalTemplate from '@templates/ui/modal';
import store from 'react-native-simple-store';
import { email, empty } from '@lib/validation';
import post from '@lib/post';
import encodePostData from '@lib/encodePostData';
import ToastTemplate from '@templates/toast';
import CountDownTimer from '@templates/countdown';
import ImagePreview from 'react-native-image-preview';
import call from 'react-native-phone-call';
import FavoriteButtonComponent from '../components/FavoriteButtonComponent';
import FooterTabComponent from '@modules/common/components/footer';
import HTML from 'react-native-render-html';
import ImageViewer from 'react-native-image-zoom-viewer';

// Workaround for new Date for IPhoneX
function jsCoreDateCreator(dateString) { 
    // dateString *HAS* to be in this format "YYYY-MM-DD HH:MM:SS"  
    let dateParam = dateString.split(/[\s-:]/)  
    dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString()  
    return new Date(...dateParam)  
}

// Simple utility for generating random integer
function randInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

let randTimes = randInt(10, 70);
let randHours = randInt(1, 4);
let randPeople = randInt(3, 10);

export default class AdContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showContactSellerFormModal: false,
            adDetails: {},
            adCompleteDetails: {
                promotion_ends_at: null,
                user: {mobile: null},
                description: 'loading...'
            },
            name: { error: false, value: '' },
            email: { error: false, value: '' },
            phone: { error: false, value: '' },
            message: { error: false, value: '' },
            toast: { ype: 'success', show: false, message: '' },
            formType: 'contact',
            revealContact: false,
            adImages: [],
            adAttributes: [],
            imageModal: {
                visible: false,
                uri: '',
                setVisibleToFalse: true
            }
        };

        store.get('storat.cache')
            .then(res => {
                console.log('ADDD: ', res.listingDetails)
                this.setState({ adDetails: res.listingDetails });
                this.fetchImages(res.listingDetails.id);
                this.fetchCompleteAdDetails(res.listingDetails.id);
                console.log('DETAILS', this.state.adDetails);
                this.populateAttributes();
                Actions.refresh({ title: res.listingDetails.translations.en.title });
            })
    }

    fetchImages(listingId) {
        let url = 'https://uae.storat.com/api/v2/ads/listing/images/' + listingId;

        fetch(url)
            .then(res => res.json())
            .then(res => {
                this.setState({ adImages: res });
            });
    }

    fetchCompleteAdDetails(listingId) {
        let url = 'https://uae.storat.com/api/v2/ads/listing/' + listingId;

        fetch(url)
            .then(res => res.json())
            .then(res => {
                console.log('uhigyutfyrct', res)
                this.setState({ adCompleteDetails: res });
            });
    }

    showContactForm(type) {

        this.setState({
            formType: (type === 'promotion') ? 'promotion' : 'contact',
            showContactSellerFormModal: true
        });

    }

    revealContact() {
        this.setState({ revealContact: true });

        // log the request
        let url = 'https://uae.storat.com/api/sendleadnotification/' + this.state.adDetails.id;

        fetch(url)
            .then(() => {
                console.log('Contact Revealed');
            });
    }

    validate(field, value) {
        switch (field) {
            case 'name':
                this.setState({
                    name: {
                        error: empty(value),
                        value: value
                    }
                })
                break;
            case 'phone':
                this.setState({
                    phone: {
                        error: empty(value),
                        value: value
                    }
                })
            case 'email':
                this.setState({
                    email: {
                        error: !email(value),
                        value: value
                    }
                })
                break;
            case 'message':
                this.setState({
                    message: {
                        error: empty(value),
                        value: value
                    }
                })
        }
    }

    nameInvalid() {
        return empty(this.state.phone.value) || this.state.phone.error;
    }
    phoneInvalid() {
        return empty(this.state.phone.value) || this.state.phone.error;
    }

    emailInvalid() {
        return !this.state.email.value.trim() || this.state.email.error;
    }
    messageInvalid() {
        return empty(this.state.message.value) || this.state.message.error;
    }

    formInvalid() {
        if (
            this.nameInvalid() ||
            this.phoneInvalid() ||
            this.emailInvalid() ||
            this.messageInvalid()
        ) {
            return true;
        }

        return false;
    }

    submit() {
        if (this.formInvalid()) {
            this.validate('name', this.state.name.value);
            this.validate('phone', this.state.phone.value);
            this.validate('email', this.state.email.value);
            this.validate('message', this.state.message.value);

            return;
        }

        this.postMessage();

    }

    postMessage() {
        let url = 'https://uae.storat.com/api/v2/ads/' + this.state.adDetails.id + '/contact'
        let formBody = encodePostData({
            user_name: this.state.name.value,
            user_phone: this.state.phone.value,
            user_email: this.state.email.value,
            user_message: this.state.message.value
        });

        post(url, formBody)
            .then(response => response.json())
            .then(response => {
                console.log('RES: ', response);
                if (response.meta.code === 200) {
                    this.setState({
                        toast: {
                            type: 'success',
                            show: true,
                            message: 'Success, the seller will contact you soon.'
                        }
                    });
                } else {
                    this.setState({
                        toast: {
                            type: 'danger',
                            show: true,
                            message: 'Whoops, we couldn\'t send your message to seller. Please try again.'
                        }
                    });
                }
            })
            .catch(err => {
                this.setState({
                    toast: {
                        type: 'danger',
                        show: true,
                        message: 'Whoops, we are facing some technical issues. Please try again after some time.'
                    }
                });
            });
    }

    getFormattedPrice(num) {
        return num.toLocaleString();
    }

    getContactFormBody(type) {

        return (
            <View style={{ flex: 1 }}>

                {/* Title */}
                <Text style={[styles.bold, { alignSelf: 'center' }]}>
                    {this.state.formType !== 'promotion' ? 'Contact Seller' : 'Get Discount Voucher'}
                </Text>

                {/* Toast */}
                <ToastTemplate
                    type={this.state.toast.type}
                    show={this.state.toast.show}
                    message={this.state.toast.message}
                />

                {/* Input Text: Name */}
                <View style={[styles.inputContainer]}>
                    <TextInput
                        style={styles.input}
                        autocapitalize="none"
                        autoCorrect={false}
                        keyboardType="email-address"
                        returnKeyType="next"
                        placeholder="Name: Your Name"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"
                        onChangeText={text => this.validate('name', text)}
                    />
                    <ErrorLabel
                        showError={this.state.name.error}
                        message={'* required'}
                    />
                </View>

                {/* Input Text: Phone */}
                <View style={[styles.inputContainer]}>
                    <TextInput
                        style={styles.input}
                        autocapitalize="none"
                        returnKeyType="go"
                        placeholder="Phone: 050 000 0000"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"
                        onChangeText={text => this.validate('phone', text)}
                    />
                    <ErrorLabel
                        showError={this.state.phone.error}
                        message={'* required'}
                    />
                </View>

                {/* Input Text: Email */}
                <View style={[styles.inputContainer]}>
                    <TextInput
                        style={styles.input}
                        autocapitalize="none"
                        returnKeyType="go"
                        placeholder="Email: you@email.com"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"
                        onChangeText={text => this.validate('email', text)}
                    />
                    <ErrorLabel
                        showError={this.state.email.error}
                        message={'* required'}
                    />
                </View>

                {/* Input Text: Message */}
                {this.state.formType !== 'promotion' ?
                    <View style={[styles.inputContainer]}>
                        <TextInput
                            style={styles.input}
                            autocapitalize="none"
                            returnKeyType="go"
                            placeholder="Message: Write your message here."
                            placeholderTextColor="#8095a2"
                            underlineColorAndroid="transparent"
                            onChangeText={text => this.validate('message', text)}
                        />
                        <ErrorLabel
                            showError={this.state.message.error}
                            message={'* required'}
                        />
                    </View>
                    : null}

                {/* Submit Button*/}
                <TouchableOpacity onPress={() => { this.submit() }} style={[styles.buttonContainer, { marginTop: 10 }]}>
                    {this.props.showSpinner ?
                        <ActivityIndicator
                            animating={true}
                            color={'#fff'}
                        />
                        :


                        <Text style={styles.buttonText}>
                            Send Message
                        </Text>

                    }
                </TouchableOpacity>

                {/* Cancel */}
                <Text
                    onPress={() => this.setState({ showContactSellerFormModal: false, toast: { show: false } })}
                    style={[styles.link, { alignSelf: 'center', marginTop: 10 }]}
                >
                    Cancel
                </Text>
            </View>
        );

    }

    showImageModal(uri) {
        this.setState({
            imageModal: {
                visible: true,
                uri: uri,
                setVisibleToFalse: false
            }
        });
    }

    populateAttributes() {
        console.log('ATTRIBUTES', this.state.adDetails);
        let adAttributes = this.state.adDetails.custom_attributes;
        let allAttributes = {};
        let attributesMap = {};

        store.get('storat.cache')
            .then(data => {
                data.attributes.forEach(e => {
                    allAttributes[e.id] = e;
                });
            })
            .then(() => {
                Object.keys(adAttributes).forEach(k => {
                    [attributeId, optionId] = k.split('-');

                    let attrName = allAttributes[attributeId].translations.en.name;

                    if (!attributesMap[attrName]) {
                        attributesMap[attrName] = '';
                    }

                    if (!optionId) {
                        attributesMap[attrName] = adAttributes[k].value;
                    } else {
                        if (attributesMap[attrName]) {
                            attributesMap[attrName] += ', ';
                        }

                        attributesMap[attrName] += allAttributes[attributeId].translations.en.options[optionId];
                    }

                })
            })
            .then(() => {
                let items = [];

                Object.keys(attributesMap).forEach(k => {

                    // attributesMap[k] = attributesMap[k].slice(0, -2);

                    items.push(
                        <ListItem key={k} style={[styles.listContent]}>
                            <Body style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={[styles.thin]}>
                                    {k}
                                </Text>
                                <Text style={[styles.bold, styles.thin, styles.marginLeft15]}>
                                    {attributesMap[k]}
                                </Text>
                            </Body>
                        </ListItem>
                    );
                });

                this.setState({ adAttributes: items });
            });

    }

    getAdImagesForGallery() {
        if(! this.state.imageModal.visible) return;

        let arr = [];

        this.state.adImages.forEach(e => {
            arr.push({url: e});
            // arr.push({source: {uri: e}});
        })

        return arr;
    }

    render() {
        return (
            <Container style={[styles.container]}>
                {/* Status bar */}
                <StatusBarTemplate />

                {/* Main Page */}
                <Content style={[styles.content]}>
                    {Object.values(this.state.adDetails).length !== 0 ?


                        <View>

                            {/* Ad Images */}
                            <View cardBody style={[styles.imageThumbnailsContainer]}>
                                <ImageSlider
                                    images={this.state.adImages}
                                    onPress={(imageObj, index) => { this.showImageModal(imageObj.image) }}
                                />
                                
                                {/* <Gallery
                                    style={{ width: null, height: 150, backgroundColor: 'black' }}
                                    images={this.getAdImagesForGallery()}
                                /> */}
                                {/* <ImagePreview visible={this.state.imageModal.visible} source={{ uri: this.state.imageModal.uri }} close={() => this.setState({ imageModal: { visible: false } })} /> */}
                                <Modal visible={this.state.imageModal.visible} transparent={true}>
                                    {/* <Icon name='close' color='#fff' onPress={() => this.setState({ imageModal: { visible: false } }) } /> */}
                                    <View style={{ flex: 1, backgroundColor: 'black' }}>
                                        <ImageViewer 
                                            imageUrls={this.getAdImagesForGallery()} 
                                            enableImageZoom={true} 
                                            onCancel={() => { this.setState({ imageModal: { visible: false } }) }}
                                        />
                                        <Button iconLeft transparent style={{marginBottom: 40, marginLeft: 10, height: 18, alignSelf: 'center'}} onPress={() => this.setState({ imageModal: { visible: false } }) }>
                                            <Icon name='close' style={{color:'#fff'}} />
                                            <Text style={{color: '#fff'}}>Close</Text>
                                        </Button>
                                    </View>
                                </Modal>
                            </View>
                            

                            {/* Ad Title */}
                            <View style={[styles.adTitleContainer]}>
                                <Text style={[styles.adListingHead]}>
                                    {this.state.adDetails.translations.en.title}
                                </Text>
                            </View>

                            {/* Ad Price & Favorite */}
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                {/* Ad Price */}
                                <View style={[styles.adPriceContainer]}>
                                    {this.getFormattedPrice(this.state.adDetails.discount_price || this.state.adDetails.price) != -1 ?
                                    <Text style={[styles.adListingPrice]}>
                                        {this.getFormattedPrice(this.state.adDetails.discount_price || this.state.adDetails.price)} AED
                                    </Text>
                                    : null}

                                    {this.state.adDetails.discount_price == 0 && this.getFormattedPrice(this.state.adDetails.discount_price || this.state.adDetails.price) != -1 ?
                                        <View>
                                            <Text style={[styles.adDiscountPrice]}>
                                                <Text style={[styles.lineStroke]}>{this.state.adDetails.price} AED </Text>
                                                You Save: <Text style={[styles.bold, styles.black]}>{this.getFormattedPrice(this.state.adDetails.price - this.state.adDetails.discount_price)} AED</Text>
                                            </Text>
                                        </View>
                                        : null}
                                    
                                </View>

                                {/* Ad Favorite */}
                                <View>
                                    {/* <FavoriteButtonComponent
                                        adId={this.state.adDetails.id}
                                        adTitle={this.state.adDetails.translations.en.title}
                                    /> */}
                                </View>
                            </View>

                            {/* Promotional Details */}
                            {this.state.adCompleteDetails.promotion_ends_at ?
                                <View style={[styles.promotionContainer]}>
                                    <Text style={[styles.red, styles.lightBold]}>Promotion will end in </Text>
                                    <CountDownTimer
                                        // date="2018-04-11T00:00:00+00:00"
                                        // date={new Date(this.state.adCompleteDetails.promotion_ends_at).getTime() }
                                        // date={this.state.adCompleteDetails.promotion_ends_at}
                                        date={jsCoreDateCreator(this.state.adCompleteDetails.promotion_ends_at).getTime()}
                                        days={{ plural: 'd ', singular: 'd ' }}
                                        hours='h'
                                        mins='m'
                                        segs='s'

                                        daysStyle={styles.time}
                                        hoursStyle={styles.time}
                                        minsStyle={styles.time}
                                        secsStyle={styles.time}
                                        firstColonStyle={styles.colon}
                                        secondColonStyle={styles.colon}
                                        color={[styles.red, styles.bold, styles.italic]}
                                    />
                                </View>
                                : null}

                            {/* Claim Voucher Button */}
                            {this.state.adDetails.discount_price != 0 ?
                                <View style={[styles.getDiscountVoucherButtonContainer]}>
                                    <Button
                                        small success full style={[styles.dicountVoucherButton]}
                                        onPress={() => this.showContactForm('promotion')}
                                    >
                                        <Text style={[styles.contactSellerButtonText]}>
                                            Get Discount Voucher Now!
                                    </Text>
                                    </Button>


                                    <Text style={[styles.orange, styles.small]}>* Storat discount vouchers claimed {randTimes} times in the last {randHours} hours</Text>
                                    <Text style={[styles.red, styles.small]}>* {randPeople} people viewing this offer now</Text>
                                </View>
                                : null}


                            {/* Seller Contact Buttons Container */}
                            <View style={[styles.buttonsContainer]}>
                                {/* Reveal Number Button */}
                                <View style={[styles.revealNumberButtonContainer]}>
                                    <Button
                                        small full style={[styles.revealNumberButton]}
                                        onPress={() => { this.revealContact(); call({ number: this.state.adCompleteDetails.user.mobile, prompt: true }) }}
                                    >
                                        <Text style={[styles.contactSellerButtonText]}>
                                            {this.state.revealContact ?
                                                this.state.adCompleteDetails.user.mobile
                                                :
                                                '+971-266XXXX (Show number)'
                                            }
                                        </Text>
                                    </Button>
                                </View>

                                {/* Contact Seller */}
                                <View style={[styles.contactSellerButtonContainer]}>
                                    <Button
                                        small full style={[styles.contactSellerButton]}
                                        onPress={() => this.showContactForm()}
                                    >
                                        <Icon name='mail' />
                                        <Text style={[styles.contactSellerButtonText]}>
                                            Contact
                                        </Text>
                                    </Button>
                                </View>
                            </View>

                            {/* Ad Details Heading */}
                            <View>
                                <Text style={[styles.adDetailsHead]}>
                                    Details
                                </Text>
                            </View>

                            {/* Ad Details */}
                            <View>
                                {/* <Text style={[styles.thin]}>
                                    {this.state.adDetails.translations.en.description}
                                </Text> */}
                                <HTML html={this.state.adCompleteDetails.description} style={[styles.thin]} />
                            </View>

                            {/* Additional Details Heading */}
                            {this.state.adAttributes.length ?
                                <View>
                                    <Text style={[styles.adDetailsHead]}>
                                        Aditional Details
                                </Text>
                                </View>
                                : null}

                            {/* Additional Details Content */}
                            <View>
                                <List>
                                    {this.state.adAttributes}
                                </List>

                            </View>

                            {/* Add some vertical space */}
                            <View style={[styles.verticalGap]}></View>


                        </View>
                        : null}


                </Content>

                <ModalTemplate
                    body={this.getContactFormBody()}
                    visible={this.state.showContactSellerFormModal}
                />


                {/* FooterTabs */}
                <FooterTabComponent />

            </Container>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    content: {
        padding: 10
    },
    imageThumbnailsContainer: {
        // padding: 15
        marginTop: -10,
        marginLeft: -10,
        marginRight: -10,
    },
    adCountTitle: {
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
    },
    adTitleContainer: {
        marginTop: 15,
        marginBottom: 15
    },
    adPriceContainer: {
        marginBottom: 15
    },
    adListingHead: {
        fontSize: 15,
        fontWeight: '700',
    },
    adListingLocationHead: {
        marginTop: -15,
        color: '#646464',
        fontSize: 12
    },
    adListingPrice: {
        color: '#cc0000',
        fontWeight: '700',
        marginBottom: 5,
    },
    adDiscountPrice: {
        fontSize: 13,
        color: '#646464'
    },
    bold: {
        fontWeight: 'bold'
    },
    adSnippetContainer: {
        alignItems: 'flex-start',
        flexDirection: 'column',
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    promotionContainer: {
        flexDirection: 'row',
        marginTop: 10
    },
    buttonsContainer: {
        backgroundColor: '#f7f8fb',
        alignSelf: 'stretch',
        padding: 20,
        borderColor: '#f5f6fa',
        borderRadius: 3
    },
    getDiscountVoucherButtonContainer: {
        alignSelf: 'stretch',
        marginBottom: 20,
        marginTop: 10
    },
    revealNumberButtonContainer: {
        alignSelf: 'stretch',
        marginBottom: 10,
        marginTop: 10
    },
    revealNumberButton: {
        backgroundColor: '#1471B9',
        borderRadius: 3
    },
    dicountVoucherButton: {
        backgroundColor: '#009900',
        marginBottom: 10,
        borderRadius: 3
    },
    contactSellerButtonContainer: {
        alignSelf: 'stretch',
    },
    contactSellerButton: {
        // alignSelf: 'flex-end',
        backgroundColor: '#009900',
        borderRadius: 3
    },
    contactSellerButtonText: {
        color: '#fff',
        fontWeight: '700',
        alignSelf: 'flex-end'
    },
    adDetailsHead: {
        fontWeight: '700',
        marginTop: 30,
        marginBottom: 15,
    },
    listContent: {
        marginLeft: 0
    },
    verticalGap: {
        paddingTop: 30
    },
    bold: {
        fontWeight: 'bold',
    },
    small: {
        fontSize: 11
    },
    medium: {
        fontSize: 14
    },
    lightBold: {
        fontWeight: '500'
    },
    black: {
        color: '#000'
    },
    red: {
        color: '#cc0000'
    },
    orange: {
        color: '#d66309'
    },
    italic: {
        fontStyle: 'italic'
    },
    lineStroke: {
        textDecorationLine: 'line-through',
    },
    thin: {
        fontSize: 14
    },
    input: {
        height: 40,
        borderWidth: 1,
        borderColor: '#d3d3d3',
        borderRadius: 3,
        marginBottom: 5,
        fontSize: 14,
        color: '#000',
        padding: 10,
        paddingLeft: 15
    },
    inputContainer: {
        marginBottom: 10,
        marginTop: 10,
    },
    buttonContainer: {
        backgroundColor: '#f94e51',
        paddingVertical: 11,
        borderRadius: 3,
        marginTop: 25
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        // fontWeight: '700'
    },
    forgotPasswordContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: -5,
    },
    link: {
        color: '#2c82c3',
        fontSize: 12,
    },
    time: {

    },
    colon: {

    },
    marginLeft15: {
        marginLeft: 15
    }
});
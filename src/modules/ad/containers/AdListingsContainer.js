import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Actions } from 'react-native-router-flux';
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import noImageAvailable from '@images/no_image_available.jpeg';
import FavoriteButtonComponent from '../components/FavoriteButtonComponent';
import SearchBarComponent from '@modules/common/components/search';
import FooterTabComponent from '@modules/common/components/footer';


export default class AdContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categoryId: null,
            ads: { results: [] },
            showAdsLoadingSpinner: true,
            showMoreAdsLoadingSpinner: false,
            favoriteAds: {}
        };

        this.populateAds();

    }

    getFormattedPrice(num) {
        return num.toLocaleString();
    }

    populateAds(url) {
        // scrolls to top
        //this.refs.scrollView.scrollTo({x: 0, y: 0, animated: true}); 

        store.get('storat.cache')
            .then(res => {
                store.update('storat.cache', { filterOptions: null });
                return res;
            })
            .then(res => {

                // show filtered ads
                if (res.filterOptions) {
                    let queryString = '';

                    Object.keys(res.filterOptions).forEach(filter => {
                        queryString += filter + '=' + encodeURIComponent(res.filterOptions[filter]) + '&';
                    });

                    url = 'https://uae.storat.com/api/v2/ads/search?' + queryString;
                    console.log(url);
                    this.setState({ categoryId: res.filterOptions.categoryId });
                } else { // show ads by category
                    // url = url || 'https://uae.storat.com/api/v2/ads/' + res.categoryId;
                    let c = '';
                    res.categories.forEach(e => {
                        if(e.id == res.categoryId) {
                            c = encodeURIComponent(e.url);
                        }
                    })
                    
                    url = url || 'https://uae.storat.com/api/v2/ads/search?' + 'country=' + res.countryLabel + '&state=' + (res.stateLabel == 'All States' ? '' : res.stateLabel) + '&c=' + c;
                    // console.log(res.categories);
                    // console.log(url)
                    this.setState({ categoryId: res.categoryId });
                }
              
                fetch(url)
                    .then(res => res.json())
                    .then(res => {
                    
                        let currentResult = res;
                        let previousResult = this.state.ads;

                        // previousResult.results.push(currentResult.results);
                        currentResult.results = previousResult.results.concat(currentResult.results);
                        this.setState({ ads: currentResult, showMoreAdsLoadingSpinner: false });
                    }).then(() => {

                    });

            });
    }

    setListingDetails(listing) {
console.log('CURRENT', listing)
        
        store.get('storat.cache')
            .then(res => console.log('ERRRRR', res));

        store.update('storat.cache', {listingDetails: {custom_attributes: null}})
            .then(() => {
                store.update('storat.cache', {listingDetails: listing})
                    .then(() => { Actions.adListing() });
            })
    }

    renderNoAds() {
        let result = [];

               result.push(
                    <Card key={'key-no-ads-card'} style={[styles.adCardContainer]}>
                        
                        <CardItem>
                            <Text style={[styles.adListingHead]}>
                                Sorry, we couldn’t find any result for this query.
                            </Text>
                        </CardItem>
                    </Card>
                );
            

        // this.setState({showAdsLoadingSpinner: false});

        return result;
    }

    renderAds() {
        let result = [];        

        this.state.ads.results.forEach(e => {
               
            if (e.id) { // we need this check because some ads do have id = undefined 

                result.push(
                    <Card key={e.id || i++} style={[styles.adCardContainer]}>
                        <TouchableOpacity button style={[styles.cardImage]} onPress={() => this.setListingDetails(e)}>
                            <Image source={e.image.path ? { uri: 'https://cdn04.storat.com/' + e.image.path } : noImageAvailable} style={{ backgroundColor: '#f5f5f5', height: 230, width: null, flex: 1 }} />
                        </TouchableOpacity>
                        <CardItem>
                            <Text style={[styles.adListingHead]}>
                                {e.translations.en ? e.translations.en.title : null}
                            </Text>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Button transparent>
                                    <Icon name="pin" />
                                    <Text> {e.location.state.translations.en.name}</Text>
                                </Button>
                            </Left>
                            <Body>
                                <Button transparent>
                                    {this.getFormattedPrice(e.discount_price || e.price) != -1 ? 
                                    <Text>{this.getFormattedPrice(e.discount_price || e.price)} AED</Text>
                                    : null}
                                </Button>
                            </Body>
                            <Right>
                                {/* <Button transparent small>
                                    <Icon name="heart" active style={{ color: "#ccc", fontSize: 24 }} />
                                </Button> */}
                                <FavoriteButtonComponent 
                                    adId={e.id}
                                    adTitle={e.translations.en ? e.translations.en.title : ''}
                                />
                            </Right>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Button block small primary onPress={() => { this.setListingDetails(e) }}>
                                    <Text style={{ color: '#fff' }}>Contact</Text>
                                </Button>
                            </Body>
                        </CardItem>
                    </Card>
                );
            }
        });

        // this.setState({showAdsLoadingSpinner: false});

        if(result.length === 0) {
            result = this.renderNoAds();
        }

        return result;
    }

    loadMoreAds(event) {
        if(this.distanceFromEnd(event) < 5) {
            this.setState({showMoreAdsLoadingSpinner: true});
            this.populateAds(this.state.ads.nextUrl);
        }
    }

    distanceFromEnd(event) {
        let {
            contentSize,
            contentInset,
            contentOffset,
            layoutMeasurement,
        } = event.nativeEvent;
  
        
        let contentLength = contentSize.height;
        let trailingInset = contentInset.bottom;
        let scrollOffset = contentOffset.y;
        let viewportLength = layoutMeasurement.height;
        
        return contentLength + trailingInset - scrollOffset - viewportLength;
    }

    render() {
        return (
            <Container style={[styles.container]}>
                {/* Status bar */}
                <StatusBarTemplate />

                {/* SearchBar */}
                {/* <SearchBarComponent /> */}

                {/* Main Page */}
                <Content style={[styles.content]} onScroll={(evt) => this.loadMoreAds(evt)} scrollEventThrottle={200} automaticallyAdjustContentInsets={false}>
                    <Grid>
                        {/* Ads Count Title */}
                        <Row style={[styles.adCountTitle, styles.pad]}>

                            {/* show title else loading */}
                            {this.state.ads.title ?
                                <Text style={[styles.bold]}>{this.state.ads.title.slice(0, 30) + '...'}</Text>
                                :
                                <View style={[styles.loadingAdsContainer]}>
                                    <ActivityIndicator size="small" color="#000" />
                                    <Text> Loading Ads...</Text>
                                </View>
                            }

                            {this.state.ads.title ?
                                <Button small iconLeft style={[styles.searchButton]} onPress={() => { Actions.adFilter() }}>
                                    <Icon name='list' />
                                    <Text style={{ color: '#fff' }}>Filter</Text>
                                </Button>
                                : null}
                        </Row>
                        {/* Ads Listing */}
                        {this.state.ads.title ? this.renderAds() : null}

                        {/* More Ads Loading Spinner */}
                        {this.state.showMoreAdsLoadingSpinner ? 
                        <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'center', margin: 15, marginBottom: 30 }}>
                            <ActivityIndicator size="small" color="#000" />
                        </View>
                        : null}

                        {/* <- Prev | Next -> */}
                        {/* <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'center', margin: 15 }}>
                            {this.state.ads.prevUrl ?
                                <Button small iconLeft style={[styles.searchButton]} onPress={() => { this.populateAds(this.state.ads.prevUrl) }}>
                                    <Icon name='arrow-round-back' />
                                    <Text style={{ color: '#fff' }}>Prev</Text>
                                </Button>
                                : null}

                            {this.state.ads.nextUrl ?
                                <Button small iconRight style={[styles.searchButton]} onPress={() => { this.populateAds(this.state.ads.nextUrl) }}>
                                    <Text style={{ color: '#fff' }}>Next</Text>
                                    <Icon name='arrow-round-forward' />
                                </Button>
                                : null}
                        </View> */}

                    </Grid>

                </Content>

                {/* FooterTabs */}
                <FooterTabComponent />

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {

    },
    content: {
        backgroundColor: '#e9ebee',
        // paddingLeft: 10,
        // paddingRight: 10,
    },
    adCountTitle: {
        justifyContent: 'space-between',
        paddingTop: 20,
        paddingBottom: 10,
    },
    loadingAdsContainer: {
        flexDirection: 'row',
    },
    adListingHead: {
        fontWeight: 'bold'
    },
    adCardContainer: {
        marginBottom: 10,
        borderColor: '#c5c5c5',
        borderWidth: 0
    },
    cardImage: {
        padding: 0,
        margin: 0,
    },
    searchButton: {
        // marginTop: -5, 
        backgroundColor: '#41a44c',
        marginLeft: 10,
        height: 28
    },
    pad: {
        paddingLeft: 10,
        paddingRight: 10
    },
    bold: {
        fontWeight: '700'
    },
    lineStroke: {
        textDecorationLine: 'line-through',
    },
});
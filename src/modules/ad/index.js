import AdFilterContainer from './containers/AdFilterContainer';
import AdListingContainer from './containers/AdListingContainer';
import AdListingsContainer from './containers/AdListingsContainer';

export default class AdModule {
    static adListings() {
        return AdListingsContainer;
    }
    static adListing() {
        return AdListingContainer;
    }
    static adFilter() {
        return AdFilterContainer;
    }
}
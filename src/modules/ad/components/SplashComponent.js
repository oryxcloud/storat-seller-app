import React, { Component } from 'react';
import { View, Text, Image, StatusBar, StyleSheet, ActivityIndicator } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
import SlideShow from './SlideShow';
import StatusBarTemplate from '@templates/statusbar';
import headerImage from '@images/splash-header.png';
import footerImage from '@images/splash-footer.png';
import * as Animatable from 'react-native-animatable';

export default class SplashComponent extends Component {
    skipIntro() {
        store.get('storat.data')
            .then(data => {
                if(data) {
                    Actions.store();
                } else {
                    Actions.login();
                }
            });
    }
    render() {
        return (
            <View style={[styles.container]}>
                
                {/* StatusBar */}
                <StatusBarTemplate hidden={true} />

                {/* Top */}
                <View style={[styles.header]}>
                    <Image 
                        style={[styles.headerImage]}
                        source={headerImage}
                    />
                </View>

                {/* Mid */}
                <View style={[styles.mid]}>
                    <Text style={[styles.brand]}>
                        STORAT SELLER
                    </Text>
                    <Text style={[styles.teaser]}>
                        The Fastest Way To Post An Ad
                    </Text>
                    <Text style={[styles.website]}>
                        www.storat.com
                    </Text>
                </View>

                {/* SlideShow */}
                <SlideShow skip={this.skipIntro} />

                {/* Skip Intro */}
                {/* <Text style={[styles.skipIntro]} onPress={this.skipIntro}>
                    Skip Intro
                </Text> */}
                
                {/* Bottom */}
                <View style={[styles.footer]}>
                    <Image 
                        style={[styles.footerImage]}
                        source={footerImage}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },
    mid: {
        alignItems: 'center'
    },
    headerImage: {
        alignSelf: 'flex-end',
        width: 370,
        height: 75,
        position: 'absolute',
        top: -20
    },
    footerImage: {
        width: null,
        height: 100
    },
    brand: {
        fontWeight: 'bold',
        fontSize: 27,
        color: '#1470b9',
        letterSpacing: 1.5
    },
    teaser: {
        fontWeight: 'bold',
        fontSize: 11,
        letterSpacing: 2,
        marginBottom: 10,
    },
    website: {
        fontSize: 10,
        color: '#646464',
        fontStyle: 'italic',
        marginBottom: 25
    },
    skipIntro: {
        alignSelf: 'center',
        color: '#1470b9',
        fontWeight: 'bold',
        fontSize: 13,
        textDecorationLine: 'underline',
        textDecorationStyle: "solid",
        textDecorationColor: "#09f"
    }
});
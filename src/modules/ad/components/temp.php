<?php

class Customer {

    private $orders = [];

    public function __construct(array $orders = []) {
        $this->orders = $orders;
    }

    public function printBill() {
        echo '<table>';
        echo '<tr>';
        echo '<td>fruits</td>';
        echo '<td>200</td>';
        echo '</tr>';
        echo '</table>';
    }

}

// create an object of customer
$customer = new Customer([
    'fruits'  => 200, 
    'clothes' => 1000, 
    'drinks'  => 500
]);

// print bill
$customer->printBill();
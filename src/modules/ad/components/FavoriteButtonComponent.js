import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body, Segment, Button, Form, Item, Label, Icon } from 'native-base';
import store from 'react-native-simple-store';

export default class FavoriteButtonComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.adId,
            title: this.props.adTitle,
            toggle: 0,
            color: '#ccc',
            favoriteAds: {}
        };

        store.get('storat.cache')
            .then(res => {
                this.state.favoriteAds = res.favoriteAds || {};
                
                if(res.favoriteAds && res.favoriteAds[this.state.id]) {
                    this.setState({color: 'red'});
                }
            })
    }
    toggleColor() {
        this.setState({
            color: (this.state.color === '#ccc') ? 'red' : '#ccc'
        })
    }
    favoriteAd() {
        let obj = {};

        if (this.state.favoriteAds[this.state.id]) {
            obj[this.state.id] = null;
        } else {
            obj[this.state.id] = { id: this.state.id, title: this.state.title };
        }


        store.update('storat.cache', {
            favoriteAds: obj
        });
    }
    render() {
        return (
            <Button key={this.state.id} transparent small onPress={() => { this.toggleColor(); this.favoriteAd(this.state.id, this.state.title) }}>
                <Icon name="heart" active style={{ color: this.state.color, fontSize: 24 }} />
            </Button>
        );
    }
}
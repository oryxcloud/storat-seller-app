import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import ErrorLabel from '@templates/error-label';

export default class RangeComponent extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            itemName: this.props.data.name,
            itemValue: {min: '', max: ''}
        };
    }
    render() {
        return (
            <View style={this.props.styles.field}>
                <View style={[this.props.styles.inputContainer]}>
                    {/* Label */}
                    <View style={this.props.styles.labelHeading}>
                        <Text style={this.props.styles.heading}>
                            {this.props.data.name}    
                        </Text>                        
                    </View>
                    {/* Text field */}
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <TextInput
                            style={[this.props.styles.input, {flex: 0.5}]}
                            autocapitalize="none"
                            autoCorrect={false}
                            keyboardType="numeric"
                            returnKeyType="next"
                            placeholder={'Min'}
                            placeholderTextColor="#94a6b1"
                            underlineColorAndroid="transparent"
                            onChangeText={ text => this.props.storeSelectedItem(this.props.data.name, {label: 'min', value: text}) }
                        />
                        <TextInput
                            style={[this.props.styles.input, {flex: 0.5}]}
                            autocapitalize="none"
                            autoCorrect={false}
                            keyboardType="numeric"
                            returnKeyType="next"
                            placeholder={'Max'}
                            placeholderTextColor="#94a6b1"
                            underlineColorAndroid="transparent"
                            onChangeText={ text => this.props.storeSelectedItem(this.props.data.name, {label: 'max', value: text}) }
                        />
                    </View>
                </View>
            </View>
        );
    }
}
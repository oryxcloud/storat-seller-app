import { connect } from 'react-redux';
import FavoritesComponent from '@modules/browse-ads/components/favorites';
import * as FavoritesActions from '@modules/browse-ads/actions/favorites';

const mapStateToProps = state => ({
    
});    

const mapDispatchToProps = dispatch => ({
    
});

export default connect(mapStateToProps, FavoritesActions)(FavoritesComponent);

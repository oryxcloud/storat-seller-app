export const loginFailed = () => {
    return {
        type: 'USER_LOGIN_FAILED'
    }
}

export const loginSuccess = () => {
    return {
        type: 'USER_LOGIN_SUCCESS'
    }
}

export const loginRequest = () => {
    return {
        type: 'USER_LOGIN_REQUEST'
    }
}

export const loginRedirect = () => {
    return {
        type: 'USER_LOGIN_REDIRECT'
    }
}

export const unexpectedError = () => {
    return {
        type: 'UNEXPECTED_ERROR'
    }
}
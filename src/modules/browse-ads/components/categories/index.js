import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Container, Content, List, ListItem, Header, Thumbnail, Left, Body, Right, Button, Icon, Title, Segment, Item, Input } from 'native-base';
import { Actions } from "react-native-router-flux";
import StatusBarTemplate from '@templates/statusbar';
import store from 'react-native-simple-store';
import { PickerTemplate } from '@templates/ui';
import motorsIcon from '@images/categories/motors.png';
import propertiesIcon from '@images/categories/properties.png';
import jobsIcon from '@images/categories/jobs.png';
import communityIcon from '@images/categories/community.png';
import classifiedsIcon from '@images/categories/classifieds.png';
import servicesIcon from '@images/categories/services.png';
import travelIcon from '@images/categories/travel.png';
import educationIcon from '@images/categories/education.png';
import financialsIcon from '@images/categories/financials.png';
import healthIcon from '@images/categories/health.png';
import restaurantsIcon from '@images/categories/restaurants.png';
import SearchBarComponent from '@modules/common/components/search';
import FooterTabComponent from '@modules/common/components/footer';


export default class CategoriesComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPicker: false,
            pickerItems: [],
            categories: []
        };

        store.get('storat.cache')
            .then(res => {
                this.setState({categories: res.categories});
                this.listCategories();
            });

    }

    // static navigationOptions = {
    //     headerLeft:null
    // }

    setCategory(categoryId, showSubCategories) {
        if(this.hasSubCategories(categoryId) && showSubCategories !== 'no') {
            this.showPickerItems(categoryId);
        } else {
            store.update('storat.cache', {'categoryId': categoryId})
                .then(() => {
                    this.hidePickerItems();
                    Actions.adListings({title: 'Browse Ads'});
                });    
            
        }
    }

    hasSubCategories(categoryId) {
        let result = false;

        this.state.categories.forEach(e => {
            if(e.parentId === categoryId) {
                result = true;
                return;
            }
        });

        return result;
    }
    
    showPickerItems(parentId) {
        let viewAllLabel = '';
        this.state.pickerItems = [];

        this.state.categories.forEach(e => {
            if(e.id === parentId) {
                viewAllLabel = e.nameEn;
            }
        });
        
        this.state.pickerItems = [
            <ListItem key={'cat-root-' + parentId} icon onPress={() => this.setCategory(parentId, 'no')}>
                <Body>
                    <Text style={[styles.flagText]}>View all {viewAllLabel}</Text>
                </Body>
                <Right>
                    <Icon name="arrow-forward" />
                </Right>
            </ListItem>
        ];
        

        this.state.categories.forEach(e => {
            if(e.parentId === parentId) {
                this.state.pickerItems.push(
                    <ListItem key={'cat-sub-' + e.id} icon onPress={() => this.setCategory(e.id, e.url)}>
                        <Body>
                            <Text style={[styles.flagText]}>{e.nameEn}</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>
                );
            }
        })

        this.setState({ showPicker: true });
    }
    hidePickerItems() {
        this.setState({
            pickerItems: [],
            showPicker: false
        });

        // Actions.adListings();
    }

    listCategories() {
        let arr = [];

        this.state.categories.forEach(e => {
            if(e.parentId === null) {
                arr.push(
                    <ListItem key={e.id} icon onPress={() => this.setCategory(e.id)}>
                        <Left>
                            <Icon name="bus" />
                        </Left>
                        <Body>
                            <Text style={[styles.flagText]}>{e.nameEn}</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>
                );
            }
        })

        return arr;
    }

    categoriesListView() {
        return (
            <List>
                <ListItem key={'id-21'} icon onPress={() => this.setCategory(21)}>
                    <Left>
                        <Image source={motorsIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Motors</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-1'} icon onPress={() => this.setCategory(1)}>
                    <Left>
                        <Image source={propertiesIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Properties</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-1929'} icon onPress={() => this.setCategory(1929)}>
                    <Left>
                        <Image source={financialsIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Financials</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-2101'} icon onPress={() => this.setCategory(2101)}>
                    <Left>
                        <Image source={healthIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Health & Wellness</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-2186'} icon onPress={() => this.setCategory(2186)}>
                    <Left>
                        <Image source={travelIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Travel & Tourism</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-995'} icon onPress={() => this.setCategory(995)}>
                    <Left>
                        <Image source={classifiedsIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Classifieds</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-1745'} icon onPress={() => this.setCategory(1745)}>
                    <Left>
                        <Image source={servicesIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Services</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-1956'} icon onPress={() => this.setCategory(1956)}>
                    <Left>
                        <Image source={restaurantsIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Restaurants</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-2094'} icon onPress={() => this.setCategory(2094)}>
                    <Left>
                        <Image source={educationIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Education</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-1758'} icon onPress={() => this.setCategory(1758)}>
                    <Left>
                        <Image source={jobsIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Jobs</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                <ListItem key={'id-981'} icon onPress={() => this.setCategory(981)}>
                    <Left>
                        <Image source={communityIcon} style={[styles.categoryIcon]}/>                        
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Community</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>
            </List>

        );
    }

    render() {
        return (
            <Container style={[styles.container]}>
                {/* Statusbar */}
                <StatusBarTemplate />
                
                {/* SearchBar */}
                <SearchBarComponent />

                {/* Categories */}
                <Content style={[styles.categoriesContainer]}>

                    {/* Categories Heading */}
                    {/* <View style={[styles.heading]}>
                        <Text style={[styles.categoryTitle]}>
                            Browse Ads by Categories
                        </Text>
                    </View> */}

                    {/* Categories List */}
                    {this.categoriesListView()}
                
                </Content>
                
                {/* <Content>
                    <List>
                        {this.listCategories()}
                    </List>
                </Content> */}

                {/* FooterTabs */}
                <FooterTabComponent />

                {/* Picker */}
                <PickerTemplate 
                    show={this.state.showPicker}
                    items={this.state.pickerItems}
                    closePicker={() => { this.hidePickerItems() }} 
                    buttonTitle='Back'
                    buttonSize='small'
                    buttonColor='blue' 
                />
                
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },

    categoryTitle: {
        fontWeight: 'bold',
    },
    categoryIcon: {
        height: 18,
        width: 18
    },
    heading: {
        margin: 15,
        paddingBottom: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#000'
    }
});
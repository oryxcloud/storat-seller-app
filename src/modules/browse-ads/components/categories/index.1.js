import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Container, Content, List, ListItem, Header, Thumbnail, Left, Body, Right, Button, Icon, Title, Segment } from 'native-base';
import { Actions } from "react-native-router-flux";
import StatusBarTemplate from '@templates/statusbar';
import store from 'react-native-simple-store';
import { PickerTemplate } from '@templates/ui';
import motorsIcon from '@images/categories/motors.png';
import propertiesIcon from '@images/categories/properties.png';
import jobsIcon from '@images/categories/jobs.png';
import communityIcon from '@images/categories/community.png';
import classifiedsIcon from '@images/categories/classifieds.png';
import servicesIcon from '@images/categories/services.png';
import travelIcon from '@images/categories/travel.png';
import educationIcon from '@images/categories/education.png';
import financialsIcon from '@images/categories/financials.png';
import healthIcon from '@images/categories/health.png';
import restaurantsIcon from '@images/categories/restaurants.png';
import SearchBarComponent from '@modules/common/components/search';
import FooterTabComponent from '@modules/common/components/footer';


export default class CategoriesComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPicker: false,
            pickerItems: [],
            categories: []
        };

        store.get('storat.cache')
            .then(res => {
                this.setState({categories: res.categories});
                this.listCategories();
            });

    }

    static navigationOptions = {
        headerLeft:null
    }

    setCategory(categoryId, showSubCategories) {
        if(this.hasSubCategories(categoryId) && showSubCategories !== 'no') {
            this.showPickerItems(categoryId);
        } else {
            store.update('storat.cache', {'categoryId': categoryId})
                .then(() => {
                    this.hidePickerItems();
                    Actions.adListings({title: 'Browse Ads'});
                });    
            
        }
    }

    hasSubCategories(categoryId) {
        let result = false;

        this.state.categories.forEach(e => {
            if(e.parentId === categoryId) {
                result = true;
                return;
            }
        });

        return result;
    }
    
    showPickerItems(parentId) {
        let viewAllLabel = '';
        this.state.pickerItems = [];

        this.state.categories.forEach(e => {
            if(e.id === parentId) {
                viewAllLabel = e.nameEn;
            }
        });
        
        this.state.pickerItems = [
            <ListItem key={'cat-root-' + parentId} icon onPress={() => this.setCategory(parentId, 'no')}>
                <Body>
                    <Text style={[styles.flagText]}>View all in {viewAllLabel}</Text>
                </Body>
                <Right>
                    <Icon name="arrow-forward" />
                </Right>
            </ListItem>
        ];
        

        this.state.categories.forEach(e => {
            if(e.parentId === parentId) {
                this.state.pickerItems.push(
                    <ListItem key={'cat-sub-' + e.id} icon onPress={() => this.setCategory(e.id, e.url)}>
                        <Body>
                            <Text style={[styles.flagText]}>{e.nameEn}</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>
                );
            }
        })

        this.setState({ showPicker: true });
    }
    hidePickerItems() {
        this.setState({
            pickerItems: [],
            showPicker: false
        });

        // Actions.adListings();
    }

    listCategories() {
        let arr = [];

        this.state.categories.forEach(e => {
            if(e.parentId === null) {
                arr.push(
                    <ListItem key={e.id} icon onPress={() => this.setCategory(e.id)}>
                        {/* <Left>
                            <Icon name="bus" />
                        </Left> */}
                        <Body>
                            <Text style={[styles.flagText]}>{e.nameEn}</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>
                );
            }
        })

        return arr;
    }

    render() {
        return (
            <Container style={[styles.container]}>
                {/* Statusbar */}
                <StatusBarTemplate />

                {/* SearchBar */}
                <SearchBarComponent />

                {/* Categories */}
                <Content style={[styles.categoriesContainer]}>

                    {/* Categories Heading */}
                    <View style={[styles.heading]}>
                        <Text style={[styles.categoryTitle]}>
                            Browse Ads by Categories
                        </Text>
                    </View>

                    {/* Categories List */}
                    <View style={{flex: 1, flexDirection: 'row', paddingVertical: 10}}>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(21)}>
                            <Image source={motorsIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Motors</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(1)}>
                            <Image source={propertiesIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Properties</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flex: 1, flexDirection: 'row', paddingVertical: 10}}>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(1929)}>
                            <Image source={financialsIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Financials</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(2101)}>
                            <Image source={healthIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Health & Wellness</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flex: 1, flexDirection: 'row', paddingVertical: 10}}>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(2186)}>
                            <Image source={travelIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Travel & Tourism</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(995)}>
                            <Image source={classifiedsIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Classifieds</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flex: 1, flexDirection: 'row', paddingVertical: 10}}>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(1745)}>
                            <Image source={servicesIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Services</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(1956)}>
                            <Image source={restaurantsIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Restaurants</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flex: 1, flexDirection: 'row', paddingVertical: 10}}>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(2094)}>
                            <Image source={educationIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Education</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(1758)}>
                            <Image source={jobsIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Jobs</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flex: 1, flexDirection: 'row', paddingVertical: 10}}>
                        <TouchableOpacity style={{flex: 0.5, alignItems: 'center'}} onPress={() => this.setCategory(981)}>
                            <Image source={communityIcon} style={[styles.categoryIcon]}/>
                            <Text style={[styles.categoryTitle]}>Community</Text>
                        </TouchableOpacity>
                    </View>
                
                </Content>
                
                {/* <Content>
                    <List>
                        {this.listCategories()}
                    </List>
                </Content> */}

                {/* FooterTabs */}
                <FooterTabComponent />

                {/* Picker */}
                <PickerTemplate 
                    show={this.state.showPicker}
                    items={this.state.pickerItems}
                    closePicker={() => { this.hidePickerItems() }} 
                    buttonTitle='Back'
                    buttonSize='small'
                    buttonColor='blue' 
                />
                
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },

    categoryTitle: {
        fontWeight: 'bold',
    },
    categoryIcon: {
        height: 30,
        width: 30
    },
    heading: {
        margin: 15,
        paddingBottom: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#000'
    }
});
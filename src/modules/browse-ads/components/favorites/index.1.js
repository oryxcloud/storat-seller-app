import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Container, Content, List, ListItem, Header, Thumbnail, Left, Body, Right, Button, Icon, Title, Segment } from 'native-base';
import { Actions } from "react-native-router-flux";
import StatusBarTemplate from '@templates/statusbar';
import store from 'react-native-simple-store';
import { PickerTemplate } from '@templates/ui';
import motorsIcon from '@images/categories/motors.png';
import propertiesIcon from '@images/categories/properties.png';
import jobsIcon from '@images/categories/jobs.png';
import communityIcon from '@images/categories/community.png';
import classifiedsIcon from '@images/categories/classifieds.png';
import servicesIcon from '@images/categories/services.png';
import travelIcon from '@images/categories/travel.png';
import educationIcon from '@images/categories/education.png';
import financialsIcon from '@images/categories/financials.png';
import healthIcon from '@images/categories/health.png';
import restaurantsIcon from '@images/categories/restaurants.png';
import SearchBarComponent from '@modules/common/components/search';
import FooterTabComponent from '@modules/common/components/footer';


export default class CategoriesComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            favoriteAds: {},
            favoriteAdsList: []
        };

        this.renderFavoriteAds();

    }

    renderFavoriteAds() {
        let arr = [];

        store.get('storat.cache')
            .then(res => {
                this.state.favoriteAds = res.favoriteAds || {};
                // this.renderFavoriteAds();
            })
            .then(() => {
                Object.keys(this.state.favoriteAds).forEach((k) => {
                    let elem = this.state.favoriteAds[k];

                    if (elem) {
                        arr.push(

                            <ListItem icon key={elem.id}>
                                <Body>
                                    <TouchableOpacity onPress={() => { this.showAdDetails(elem.id) }}>
                                        <Text style={{ color: "#1471b9" }}>
                                            {elem.title}
                                        </Text>
                                    </TouchableOpacity>
                                </Body>
                                <Right>
                                    <TouchableOpacity onPress={() => { this.deleteAd(elem.id) }}>
                                        <Icon name="trash" active style={{ color: "#cd5c5c", marginLeft: 10 }} />
                                    </TouchableOpacity>
                                </Right>
                            </ListItem>

                        );
                    }
                });
            })
            .then(() => {
                if(arr.length === 0) {
                    arr.push(

                        <ListItem icon key={'no-fav-ad'}>
                            <Body>
                               
                                    <Text style={{ color: "#646464" }}>
                                        0 Favorite Ads.
                                    </Text>
                               
                            </Body>
                        </ListItem>

                    );
                }
            })
            .then(() => {
                this.setState({ favoriteAdsList: arr });
            });

    }

    deleteAd(id) {
        store.get('storat.cache')
            .then(res => {
                if (res.favoriteAds[id]) {
                    store.update('storat.cache', {
                        favoriteAds: { [id]: null }
                    })
                        .then(() => {
                            this.renderFavoriteAds();
                        });
                }
            })
    }

    showAdDetails(id) {
        fetch('https://uae.storat.com/api/v2/ads/listing/' + id)
            .then(res => res.json())
            .then(res => {
                let listing = {
                    id: res.id,
                    price: res.price,
                    discount_price: res.discount_price,
                    translations: {en: {title: res.title, description: res.description}},
                    custom_attributes: res.custom_attributes
                };

                store.update('storat.cache', { listingDetails: listing })
                    .then(() => { Actions.adListing() });
            })
    }

    render() {
        return (
            <Container style={[styles.container]}>
                {/* Statusbar */}
                <StatusBarTemplate />

                {/* SearchBar */}
                <SearchBarComponent />

                {/* Categories */}
                <Content style={[styles.categoriesContainer]}>

                    {/* Categories Heading */}
                    <View style={[styles.heading]}>
                        <Text style={[styles.categoryTitle]}>
                            {/* <Icon name="heart" active style={{color: "#ccc", fontSize: 18}} /> */}
                            <Text>Favorite Ads</Text>
                        </Text>
                    </View>

                    {/* Favorite Ads */}
                    <List style={[styles.itemsContainer]}>
                        {this.state.favoriteAdsList}
                    </List>



                </Content>

                {/* FooterTabs */}
                <FooterTabComponent />

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },

    categoryTitle: {
        fontWeight: 'bold',
    },
    categoryIcon: {
        height: 30,
        width: 30
    },
    heading: {
        margin: 15,
        paddingBottom: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#000'
    },
    itemsContainer: {
        marginRight: 15
    }
});
import React, { Component } from 'react';
import {
    Text,
    View,
    Modal,
    TextInput,
    StatusBar,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import store from 'react-native-simple-store';
import { Spinner, Toast } from 'native-base';
import LoginFormStyles from '@modules/auth/styles/login/LoginFormStyles';
import ErrorLabel from './ErrorLabel';
import ForgotPassword from './ForgotPassword'; 
import ToastTemplate from '@templates/toast';
import post from '@lib/post';
import {email, empty} from '@lib/validation';
import encodePostData from '@lib/encodePostData';

export default class LoginForm extends Component {
    constructor(props) {
        super(props);
        console.log(this.props)
        this.state = {
            email: {
                error: false,
                value: ''
            },
            password: {
                error: false,
                value: ''
            },
            showForgotPasswordModal: false
        };

        this.submit = this.submit.bind(this);
        this.hideForgotPasswordModal = this.hideForgotPasswordModal.bind(this);
    }
    validate(field, value) {
        switch(field) {
            case 'email':
                this.setState({
                    email: {
                        error: ! email(value),
                        value: value
                    }
                }) 
                break;
            case 'password': 
                this.setState({
                    password: {
                        error: empty(value),
                        value: value
                    }
                })
        }
    }
    emailInvalid() {
        return ! this.state.email.value.trim() || this.state.email.error;
    }          
    passwordInvalid() {
        return empty(this.state.password.value) || this.state.password.error;
    }          
    submit() {
        if(this.emailInvalid() || this.passwordInvalid()) {
            return false;
        }
        
        this.props.loginRequest(true);
        this.verifyLogin();
    }
    verifyLogin() {
        let url = 'https://uae.storat.com/api/v2/login';
        // let url = 'http://uae.storat.com/api/v2/login';
        let formBody = encodePostData({
            email: this.state.email.value,
            password: this.state.password.value
        });

        this.verifyCredentials(url, formBody);
    }
    loginSuccess() {
        let self = this;

        self.props.loginSuccess()
        
        setTimeout(function() {
            self.loginRedirect();
        }, 1000);
    }
    loginRedirect() {
        this.props.loginRedirect();

        setTimeout(() => {
            Actions.store();
        }, 1000)
    }
    verifyCredentials(url, formBody) {
        post(url, formBody)
            .then(response => response.json())
            .then(response => {
                console.log(response);
                if(response.meta.code === 200) {

                    store.update('storat.data', {
                        apiToken: response.data.api_token,
                        info: response.data.info,
                        locations: response.data.locations,
                        categories: response.data.categories,
                        adCache: {
                            country: {id: null, label: null},
                            state: {id: null, label: null},
                            city: {id: null, label: null},
                            region: {id: null, label: null},
                            categories: null
                        }
                    });
                  
                    this.loginSuccess();
                } else {
                    store.update('storat.data', {
                        apiToken: null
                    });
                    this.props.loginFailed();
                }
            })
            .catch(err => {
                console.log(err)
                this.props.unexpectedError();
            });
    }
    hideForgotPasswordModal() {
        this.setState({showForgotPasswordModal: false});
    }
    render() {
        return (
            <View style={styles.container}>

                {/* Toast Danger */}
                <ToastTemplate 
                    type={this.props.toastType}
                    show={this.props.showToast} 
                    message={this.props.toastMessage} 
                />
                                                
                {/* Input Text: Email */}
                <View style={[styles.inputContainer]}>
                    <TextInput
                        style={styles.input}
                        autocapitalize="none"
                        autoCorrect={false}
                        keyboardType="email-address"
                        returnKeyType="next"
                        placeholder="your-email@example.com"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"
                        onChangeText={ text => this.validate('email', text) }
                    />
                    <ErrorLabel 
                        showError={this.state.email.error} 
                        message={'Email is invalid'} 
                    />
                </View>

                {/* Input Text: Passsword */}                
                <View style={[styles.inputContainer]}>
                    <TextInput 
                        style={styles.input}
                        autocapitalize="none"
                        returnKeyType="go"
                        placeholder="Enter password here"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"                    
                        secureTextEntry   
                        onChangeText={ text => this.validate('password', text) }
                                         
                    />
                    <ErrorLabel 
                        showError={this.state.password.error} 
                        message={'Password is invalid'} 
                    />
                </View>

                {/* Forgot password */}
                {/* <TouchableOpacity style={[styles.forgotPasswordContainer]} onPress={() => {this.setState({showForgotPasswordModal: true})}}>
                    <Text style={[styles.link]}>
                        Forgot your password..?
                    </Text>
                </TouchableOpacity> */}

                {/* Submit Button*/}      
                <TouchableOpacity onPress={this.submit} style={styles.buttonContainer}>
                    {this.props.showSpinner ?
                        <ActivityIndicator 
                            animating={true}
                            color={'#fff'}
                        />
                    :
                        <Text style={styles.buttonText}>
                            Login    
                        </Text>
                    }
                </TouchableOpacity>   

                {/* Forgot password */}
                <ForgotPassword 
                    visible={this.state.showForgotPasswordModal}
                    hideModal={this.hideForgotPasswordModal}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create(LoginFormStyles);
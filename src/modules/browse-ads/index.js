import FavoritesContainer from './containers/favorites';
import CategoriesContainer from './containers/categories';

export default class BrowseAdsModule {
    static favorites() {
        return FavoritesContainer;
    }
    static categories() {
        return CategoriesContainer;
    }
}
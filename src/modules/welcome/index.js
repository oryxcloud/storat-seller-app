import WelcomeContainer from './containers/welcome';

export default class WelcomeModule {
    static options() {
        return WelcomeContainer;
    }
}
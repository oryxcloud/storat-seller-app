import { connect } from 'react-redux';
import WelcomeComponent from '@modules/welcome/components/welcome';
import * as WelcomeActions from '@modules/welcome/actions/welcome';

const mapStateToProps = state => ({
    
});    

const mapDispatchToProps = dispatch => ({
    
});

export default connect(mapStateToProps, WelcomeActions)(WelcomeComponent);

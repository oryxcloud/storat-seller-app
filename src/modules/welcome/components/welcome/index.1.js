import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Container, Content, List, ListItem, Header, Thumbnail, Left, Body, Right, Button, Icon, Title, Segment } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import backgroundImage from '@images/splash-header.png';
import footerImage from '@images/splash-footer.png';
import FooterTabComponent from '@modules/common/components/footer';

export default class CategoriesComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            active: false,
            countryLabel: ''
        };

        store.get('storat.cache')
                            .then(data => {
                                countryId = data.country || 1;
                                this.setState({countryLabel: data.locations.countries[countryId-1].nameEn})
                            });

        this.fetchCategories();
    }
    static navigationOptions = {
        headerLeft:null
    }
    getCountryLabel() {
        storat.get('storat.cache')
            .then(data => {
                if(data.country && data.locations) {
                    console.log('COUNTIRES', data);
                    // data.locations.countries.forEach(e => {
                    //     if()
                    // })
                    // this.setState({countryLabel: data.locations.countries});
                }
            });
    }
    fetchCategories() {
        let url = 'https://uae.storat.com/api/v2/categories';

        fetch(url)
            .then(res => res.json())
            .then(res => {
                store.update('storat.cache', {
                    'categories': res
                });
            })
            .then(res => {
                fetch('https://uae.storat.com/api/v2/locations')
                    .then(res => res.json())
                    .then(res => {
                        store.update('storat.cache', {
                            'locations': res,
                            'country': 1 // assume UAE
                        });
                    });
            })
            .then(() => {
                fetch('https://uae.storat.com/api/v2/ads/attributes')
                    .then(res => res.json())
                    .then(res => {
                        store.update('storat.cache', {
                            'attributes': res
                        });
                    });
            });
    }
    render() {
        return (
            <Container style={[styles.container]}>
                {/* Statusbar */}
                <StatusBarTemplate />

                <View style={{}}>
                    {/* <Image source={backgroundImage} style={{height: 70, width: null}}/> */}
                </View>

                {/* Action Items */}
                <Content>
                    <List>
                        
                        {/* <ListItem icon onPress={() => Actions.selectLanguage()}>
                            <Left>
                                <Icon name="people" />
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Set Language (English)</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem> */}

                        <ListItem icon onPress={() => Actions.selectCountry()}>
                            <Left>
                                <Icon name="flag" />
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Set Country ({this.state.countryLabel})</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                        <ListItem icon onPress={() => Actions.browseAdsCategories()}>
                            <Left>
                                <Icon name="grid" />
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Browse Categories & Products</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                        <ListItem icon onPress={() => Actions.store()}>
                            <Left>
                                <Icon name="pin" />
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Post An Ad</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                        <ListItem icon onPress={() => Actions.login()}>
                            <Left>
                                <Icon name="lock" />
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Login</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                        <ListItem icon onPress={() => Actions.register()}>
                            <Left>
                                <Icon name="unlock" />
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Register</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                    </List>
                </Content>

                {/* <View style={[styles.footer]}>
                    <Image 
                        style={[styles.footerImage]}
                        source={footerImage}
                    />
                </View> */}

                {/* FooterTabs */}
                <FooterTabComponent />
                
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },
    footerImage: {
        width: null,
        height: 160
    },
});
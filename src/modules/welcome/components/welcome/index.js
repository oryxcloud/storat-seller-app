import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Linking } from 'react-native';
import { Container, Content, List, ListItem, Header, Thumbnail, Left, Body, Right, Button, Icon, Title, Segment } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import backgroundImage from '@images/splash-header.png';
import footerImage from '@images/splash-footer.png';
import FooterTabComponent from '@modules/common/components/footer';

export default class CategoriesComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            
        };

    }
   
    openAppLink() {
        Linking.openURL('https://storat.com/login');
    }

    render() {
        return (
            <Container style={[styles.container]}>
                {/* Statusbar */}
                <StatusBarTemplate />

                <View style={{justifyContent: 'center', padding: 15, flexDirection: 'column', flex: 1}}>
                    <Icon name={'exit'} style={{textAlign: 'center', fontSize: 40, color: 'green'}} />
                    <Text style={{fontSize: 20, fontWeight: '100', textAlign: 'center'}}>
                        To Register, publish, and configure a Store on Storat, Please go to storat.com website
                    </Text>
                    <Button full large danger style={{borderRadius: 3, marginTop: 30}} onPress={() => {this.openAppLink()}}>
                        <Text style={{fontSize: 18, color: '#fff'}}>Go to storat.com</Text>
                    </Button>
                </View>

                {/* FooterTabs */}
                <FooterTabComponent />
                
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },
    footerImage: {
        width: null,
        height: 160
    },
});
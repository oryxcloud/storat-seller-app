import store from 'react-native-simple-store';

const initialState = {
    info: {},
    categories: {},
    locations: {
        countries: [], states: [], cities: [], regions: []
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_STORE': 
            return {
                ...state,
                info: action.info
            }
        case 'UPDATE_STORE_CATEGORIES':
            return {
                ...state,
                categories: action.categories
            }
        case 'UPDATE_STORE_LOCATIONS':
            return {
                ...state,
                locations: action.locations
            }
        default:
            return state;
    }
};

export default reducer;
import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import ImageCoverStyles from '@modules/store/styles/welcome/ImageCoverStyles';
import store from 'react-native-simple-store';
// import coverImage from '@images/store-bg.jpeg';

export default class ImageCover extends Component {
    constructor(props) {
        super(props);

        this.state = {coverImage: ''};
    }
    componentWillMount() {
        store.get('storat.data')
            .then(res => {
                console.log(res.info.cover)
                this.setState({coverImage: res.info.cover});
            });
    }
    render() {
        return (
            <View style={[styles.storatCoverContainer]}>
                {this.state.coverImage ?
                <Image 
                    style={styles.storatCover}
                    source={{uri: this.state.coverImage}}
                />
                : null}
            </View>
        );
    }
}

const styles = StyleSheet.create(ImageCoverStyles);
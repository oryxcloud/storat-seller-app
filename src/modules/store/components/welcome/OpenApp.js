import React, { Component } from 'react';
import { Text, View, TextInput, StatusBar, StyleSheet, TouchableOpacity, Platform, Linking } from 'react-native';
import { Container, Content, Card, CardItem, Body, Button } from 'native-base';
import { Actions } from "react-native-router-flux";
import PostButtonStyles from '@modules/store/styles/welcome/PostButtonStyles';

export default class PostButton extends Component {
    openAppLink() {
        if (Platform.OS === 'ios') {
            Linking.openURL('https://uae.storat.com/dashboard');
            //  Linking.openURL('https://itunes.apple.com/us/app/storat/id1281947375?ls=1&mt=8');
        } else {
            Linking.openURL('https://uae.storat.com/dashboard');
            //  Linking.openURL('https://play.google.com/store/apps/details?id=com.storatwebview');
        }
    }
    render() {
        return (
            <View style={[styles.container]}>
                <Text>
                    <Text style={[styles.text]}>
                        To configure your store detail,  
                    </Text>
                    <Text> </Text>
                    <Text style={[styles.text, styles.link]} onPress={this.openAppLink}>
                        Click Here
                    </Text>
                </Text>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        marginTop: -100,
        alignItems: 'center',
        padding: 15,
    },
    text: {
        fontSize: 12,
    },
    link: {
        color: '#1470b9',
        fontWeight: 'bold',
        textDecorationLine: 'underline',
        textDecorationStyle: "solid",
        textDecorationColor: "#09f",
        marginLeft: 10,
    }
});
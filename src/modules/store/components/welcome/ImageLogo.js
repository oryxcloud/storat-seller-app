import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import ImageLogoStyles from '@modules/store/styles/welcome/ImageLogoStyles';
import store from 'react-native-simple-store';
// import logoImage from '@images/store-logo.jpeg';


export default class ImageLogo extends Component {
    constructor(props) {
        super(props);

        this.state = {logo: ''};
    }
    componentWillMount() {
        store.get('storat.data')
            .then(res => {
                console.log(res.info.logo)
                this.setState({logo: res.info.logo});
            });
    }
    render() {
        return (
            <View style={styles.logoContainer}>
                {this.state.logo ?
                <Image 
                    style={styles.logoImage}
                    source={{uri: this.state.logo}}
                />
                : null }
            </View>
        );
    }
}

const styles = StyleSheet.create(ImageLogoStyles);
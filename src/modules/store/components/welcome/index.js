import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Container, Footer, FooterTab, Button } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import StoreInfo from './StoreInfo'; 
import ImageLogo from './ImageLogo'; 
import ImageCover from './ImageCover'; 
import PostButton from './PostButton'; 
import OpenApp from './OpenApp'; 
import FooterTabComponent from '@modules/common/components/footer';


export default class Welcome extends Component {
    static navigationOptions = {
        headerLeft:null
    }
    constructor(props) {
        super(props);

        store.get('storat.data')
            .then(res => {
                console.log(res);

                if(! res) {
                    Actions.loginOrRegister();
                    return;
                }

                this.props.updateStore(res.info);
            });
    }
    render() {
        return (
            <Container style={{backgroundColor: '#fff'}}>
                <StatusBarTemplate />
                <ImageCover />
                <StoreInfo info={this.props.info}/>
                <PostButton />
                <OpenApp />

                {/* FooterTabs */}
                <FooterTabComponent />

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    thin: {
        fontSize: 12
    },
    footerTabs: {
        backgroundColor: '#e9e9e9', 
        height: 40
    }
});
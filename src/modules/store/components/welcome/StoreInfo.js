import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import StoreInfoStyles from '@modules/store/styles/welcome/StoreInfoStyles';
import store from 'react-native-simple-store';
// import logoImage from '@images/store-logo.jpeg';
import ImageLogo from './ImageLogo'; 
import ImageLogoStyles from '@modules/store/styles/welcome/ImageLogoStyles';

export default class StoreInfo extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.storeContainer]}>
                    {/* Store Logo */}
                    <ImageLogo />

                    {/* Store Details */}
                    <View style={[styles.storeInfoContainer]}>
                        <Text style={styles.storeHeading}>
                            {this.props.info.title}
                        </Text>
                        <Text style={[styles.storeBlurb]}>
                            {this.props.info.speciality}
                        </Text>
                        <Text style={[styles.lightText]}>
                            {this.props.info.email}
                        </Text>
                        <Text style={[styles.lightText]}>
                            {this.props.info.website}
                        </Text>
                    </View>

                </View>
            </View>
        );
    }
}

const styles = Object.assign(
    StyleSheet.create(ImageLogoStyles),
    StyleSheet.create(StoreInfoStyles)
);
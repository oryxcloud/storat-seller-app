import React, { Component } from 'react';
import { Text, View, TextInput, StatusBar, StyleSheet, TouchableOpacity } from 'react-native';
import { Container, Content, Card, CardItem, Body } from 'native-base';
import { Actions } from "react-native-router-flux";
import PostButtonStyles from '@modules/store/styles/welcome/PostButtonStyles';

export default class PostButton extends Component {
    render() {
        return (
            <Container style={[styles.container]}>
                <Content>
                    <Card>
                        {/*  Card header */}
                        <CardItem header style={[styles.cardHeader]}>
                            <Text>5 Easy Steps To Post An Ad</Text>
                        </CardItem>

                        {/* Card body */}
                        <CardItem>
                            <Body style={[styles.cardBody]}>                
                                {/* Submit Button */}
                                <TouchableOpacity 
                                    onPress={() => Actions.selectAdCategory()} 
                                    style={styles.buttonContainer}
                                >
                                    <Text style={styles.buttonText}>Post New Ad</Text>
                                </TouchableOpacity>                           
                            </Body>
                        </CardItem>
                    </Card>
                </Content>
        </Container>
        );
    }
};

const styles = StyleSheet.create(PostButtonStyles);
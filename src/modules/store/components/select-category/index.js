import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Left, Icon, List, ListItem, Card, CardItem, Body } from 'native-base';
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import { CardTemplate, PickerTemplate } from '@templates/ui';
import { SelectionBoxTemplate, ButtonTemplate } from '@templates/form';
import FooterTabComponent from '@modules/common/components/footer';


export default class SelectCategoryComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPicker: false,
            pickerItems: [],
            categories: null,
            showNextButton: false,
            selectionBoxes: [
                {
                    key: 0,
                    label: 'Select one',
                    parentId: null
                }
            ]
        };

        store.get('storat.data')
            .then(res => { 
                this.state.categories = res.categories;
                
                if(res.adCache.categories) {
                    this.setState({
                        selectionBoxes: JSON.parse(res.adCache.categories),
                        showNextButton: true
                    });
                }
            });
    }
    /**
     * When a user selects a category, we need to dynamically generate a new selection
     * box if there are more sub-categeries, otherwise, show them next button. 
     */
    setCategory(category, selectionBoxId) {
        this.state.selectionBoxes[selectionBoxId].label = category.nameEn;
        this.state.selectionBoxes = this.state.selectionBoxes.slice(0, selectionBoxId + 1);
        
        if(this.hasSubCategories(category.id)) {
            this.state.showNextButton = false;
            this.state.selectionBoxes.push({
                key: this.state.selectionBoxes.length,
                label: 'Select one',
                parentId: category.id    
            });
        } else {
            this.state.showNextButton = true;
        }
        
        this.setState({
            pickerItems: [],
            showPicker: false,
        });

        this.storeCategory(category.id);
    }
    hasSubCategories(categoryId) {
        let result = false;

        this.state.categories.forEach(e => {
            if(e.parentId === categoryId) {
                result = true;
                return;
            }
        });

        return result;
    }
    showPickerItems(parentId, selectionBoxId) {
        let items = [];

        this.state.categories.forEach(e => {
            if(e.parentId === parentId) {
                this.state.pickerItems.push(
                    <ListItem key={e.id} onPress={() => this.setCategory(e, selectionBoxId)}>
                        <Text>{e.nameEn}</Text>
                    </ListItem>
                );
            }
        })

        this.setState({ showPicker: true });
    }
    hidePickerItems() {
        this.setState({
            pickerItems: [],
            showPicker: false
        });
    }
    getSelectionBoxes() {
        let arr = [];
        
        this.state.selectionBoxes.forEach((e, i) => {
            arr.push(
                <SelectionBoxTemplate 
                    key={e.key}
                    text={e.label}
                    onPress={() => { this.showPickerItems(e.parentId, e.key) }}                
                />
            );
        });

        return arr;
    }
    storeCategory(categoryId) {
        store.update('storat.data', {
            postAd: {category: categoryId}
        });
    }
    submitCategory() {
        store.update('storat.data', {
            adCache: {
                categories: JSON.stringify(this.state.selectionBoxes)
            }
        });

        Actions.selectAdLocation();
    }
    render() {
        return (
            <Container>
                <StatusBarTemplate />   
                <Content style={[styles.container]}>
                    <CardTemplate 
                        title='Select Category'
                        body={this.getSelectionBoxes()}
                    />
                    {this.state.showNextButton && ! this.props.hideNextButtonWhenFilter ? 
                        <ButtonTemplate 
                            title='Next' 
                            color='red'
                            onPress={ () => { this.submitCategory() } }    
                        />
                    : null}
                </Content>

                {/* FooterTabs */}
                <FooterTabComponent />

                {/* Picker */}
                <PickerTemplate 
                    show={this.state.showPicker}
                    items={this.state.pickerItems}
                    closePicker={() => { this.hidePickerItems() }} 
                    buttonTitle='Back'
                    buttonSize='small'
                    buttonColor='blue' 
                />
            </Container>
       );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        padding: 15,
    },
});
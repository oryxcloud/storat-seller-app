import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, TouchableHighlight, StyleSheet, Modal, Linking } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, List, ListItem, Card, Icon, CardItem, Body } from 'native-base';
import StatusBarTemplate from '@templates/statusbar';
import store from 'react-native-simple-store';
import FooterTabComponent from '@modules/common/components/footer';

export default class SelectCategoryComponent extends Component {
    constructor(props) {
        super(props);

        // reset ad data
        store.get('storat.data')
            .then(res => {
                store.save('storat.data', Object.assign(res, {
                    postAd: {
                        category: res.postAd.category,
                        country_id: res.postAd.country_id,
                        state_id: res.postAd.state_id,
                        city_id: res.postAd.city_id,
                        region_id: res.postAd.region_id,
                    }
                }));
            });
        //store.update('storat.data', {postAd: null});
    }
    render() {
        return (
        <Container style={{backgroundColor: '#fff'}}>
            <StatusBarTemplate />                       
                <Content style={styles.mainHeader}>
                    <Card>
                        {/*  Card header */}
                        <CardItem header style={[styles.cardHeader]}>
                        <Text style={[styles.cardHeadingText]}> 
                            Great, your ad has been posted for review.
                        </Text>
                        </CardItem>

                        {/* Card body */}
                        <CardItem>
                            <Body style={[styles.cardBody]}>  
                                {/* Submit Button */}
                                <TouchableOpacity 
                                    onPress={() => Actions.selectAdCategory()} 
                                    style={styles.buttonContainer}
                                >
                                    <Text style={styles.buttonText}>Post Another Ad</Text>
                                </TouchableOpacity>                           
                            </Body>
                        </CardItem>
                    </Card>

                    {/* Preview Ad */}
                    <View style={[styles.previewContainer]}>
                        <Text>
                            <Text style={[styles.previewText]}>
                                To preview your ad,  
                            </Text>
                            <Text> </Text>
                            <Text 
                                style={[styles.previewText, styles.previewLink]} 
                                onPress={() => Linking.openURL('https://uae.storat.com/dashboard/ads#store-logo-form')}
                            >
                                Click Here
                            </Text>
                        </Text>
                    </View>

                </Content> 

                {/* FooterTabs */}
                <FooterTabComponent />
                              
        </Container>
       );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    selectCategoryButtonContainer: {
       backgroundColor: '#d9d9d9',
       padding: 10, 
       alignItems: 'center',
       marginTop: 30,
    },
    selectCategoryButtonText: {
        color: 'black',
    },
    simpleText: {
        marginTop: 30
    },
    buttonContainer: {
        backgroundColor: '#e24248',
        paddingVertical: 10,
        width: 150,
        borderRadius: 3
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    cardBody: {
        padding: 5,
        alignItems: 'center',
    },
    cardHeader: {
        backgroundColor: '#f7f7f7'
    },
    cardHeadingText: {
        fontWeight: 'bold',
        fontSize: 13
    },
    selectedCategoriesText: {
        fontSize: 13,
        // fontWeight: 'bold',
        color: '#000',
        marginBottom: 30
    },
    mainHeader: {
        margin: 10,
        marginTop: 30
    },
    iconTextContainer: {
        //flexGrow: 1, 
        // justifyContent:'center',
        //alignItems: 'center'
    },
    icon: {
        color: 'green',
        fontSize: 15,
    },
    previewContainer: {
        alignItems: 'center',
        padding: 15,
    },
    previewText: {
        fontSize: 12,
    },
    previewLink: {
        color: '#1470b9',
        fontWeight: 'bold',
        textDecorationLine: 'underline',
        textDecorationStyle: "solid",
        textDecorationColor: "#09f",
        marginLeft: 10,
    }
})
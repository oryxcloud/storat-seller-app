import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import ErrorLabel from '@templates/error-label';
import { SelectionBoxTemplate, ButtonTemplate, LabelTemplate } from '@templates/form';

export default class DropDownComponent extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            selectItemType: this.props.data.name,
            selectedOptionText: null
        };
    }
    updateText(selectedOptionText) {
        this.setState({selectedOptionText: selectedOptionText});
        this.props.storeSelectedItem(
            this.state.selectItemType, 
            selectedOptionText
        );
    }
    render() {
        return(
            <View style={this.props.styles.field}>
                <View style={[this.props.styles.inputContainer]}>
                    {/* Selection Box */}
                    <LabelTemplate title={this.props.data.name} />     
                    <SelectionBoxTemplate 
                        text={this.state.selectedOptionText || 'Select '  + this.props.data.name}
                        onPress={() => {this.props.populateModalItem(this.props.data.name, selectedOptionText => this.updateText(selectedOptionText))}}              
                    />
                </View>
            </View>    
        );
    }
}
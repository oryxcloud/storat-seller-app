import React, { Component } from 'React';
import { View, ScrollView, TouchableOpacity, StyleSheet, Text, TextInput, Modal, ActivityIndicator } from 'react-native';
import { Container, Header, Content, Left, Icon, List, ListItem, CheckBox, Body, Segment, Button, Form, Item, Label } from 'native-base';
import store from 'react-native-simple-store';
import { Actions } from 'react-native-router-flux';
import ErrorLabel from '@templates/error-label';
import StatusBarTemplate from '@templates/statusbar';
import RangeComponent from './RangeComponent';
import DropDownComponent from './DropDownComponent';
import OptionBoxComponent from './OptionBoxComponent';
import post from '@lib/post';
import encodePostData from '@lib/encodePostData';
import FooterTabComponent from '@modules/common/components/footer';

export default class CustomAttributesComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            attributes: [],
            shouldRender: false,
            isLoading: true,
            modalItems: [],
            attributesData: {},
            hideEmail: false,
            hidePhone: false
        };
        
        store.get('storat.data')
            .then(res => {
                //console.log(res.postAd);
                // http://uae.storat.com/api/v2/attributes/
                fetch('https://uae.storat.com/api/v2/attributes/' + res.postAd.category + '?api_token=' + res.apiToken)
                    .then(res => res.json())
                    .then(res => {
                        this.generateAttributes(res);
                    });
            });

        this.populateModalItem = this.populateModalItem.bind(this);
        this.storeSelectedItem = this.storeSelectedItem.bind(this);
    }
    generateAttributes(data) {
        data.category_attributes.forEach(e => {
            this.mapAttributeToComponent(e.attribute);
        });

        this.setState({shouldRender: true, isLoading: false, attributesData: data});
    }
    mapAttributeToComponent(attributes) {
        switch(attributes.type) {
            case 'Range':
                this.pushRangeComponent(attributes);
                return;
            case 'Drop Down':
                this.pushDropDownComponent(attributes);
                return;
            case 'Option Box':
                this.pushOptionBoxComponent(attributes);
                return;
        }
    }
    pushRangeComponent(attributes) {
        const data = attributes.current_translation[0];

        this.state.attributes.push(
            <RangeComponent 
                styles={styles}
                data={data}
                key={data.id}
            />
        );
    }
    pushDropDownComponent(attributes) {
        const data = attributes.current_translation[0];
     
        this.state.attributes.push(
            <DropDownComponent 
                styles={styles}
                data={data}
                key={data.id}
                populateModalItem={this.populateModalItem}
                storeSelectedItem={this.storeSelectedItem}
            />
        );
    }
    pushOptionBoxComponent(attributes) {
        const data = attributes.current_translation[0];

        this.state.attributes.push(
            <OptionBoxComponent 
                data={data}
                styles={styles}
                key={data.id}
                storeSelectedItem={this.storeSelectedItem}
            />
        ); 
    }
    showContent() {
        return (
            <Content style={[styles.content]}>
                {/* Custom attributes */}
                {this.state.attributes}                
                
                {/* Preferences */}
                {! this.props.hidePreferencesWhenFilter ?
                // <Content style={styles.field}>
                //     <View style={[styles.inputContainer]}>
                //         {/* Label */}
                //         <View style={styles.labelHeading}>
                //             <Text style={styles.heading}>
                //                 Preferences  
                //             </Text>                        
                //         </View>
                //         {/* Checkbox */}
                //         <Content>
                //             <ListItem>
                //                 <CheckBox checked={this.state.hideEmail} onPress={() => this.setState({hideEmail: !this.state.hideEmail})} color='#1470b9' />
                //                     <Body>
                //                         <Text> Show email on your ad</Text>
                //                     </Body>
                //             </ListItem>
                //             <ListItem>
                //                 <CheckBox checked={this.state.hidePhone} onPress={() => this.setState({hidePhone: !this.state.hidePhone})} color='#1470b9' />
                //                     <Body>
                //                         <Text> Show phone on your ad</Text>
                //                     </Body>
                //             </ListItem>
                //         </Content>
                        
                //     </View>
                // </Content>
                null
                : null}
                
                {/* Submit */}
                <View style={styles.field}>
                    <View style={[styles.inputContainer, styles.submitButtonContainer]}>
                        {/* Submit Button */}
                        <TouchableOpacity 
                            onPress={() => this.postAd()} 
                            style={styles.buttonContainer}
                        >
                            <Text style={styles.buttonText}>
                                {this.props.labelWhenFilter || 'Next'}
                            </Text>
                        </TouchableOpacity>     
                    </View>
                </View>

                {/* Spacer */}
                <View style={styles.spacer}></View>
            </Content>
        );
    }
    showIndicator() {
        return (
            <View>
                {/* Spacer */}
                <View style={styles.spacer}></View>

                {/* Indicator */}
                <ActivityIndicator 
                    animating={true}
                    color={'#646464'}
                />
            </View>
        );
    }
    showModal() {
        if(this.state.modalItems.length === 0) {
            return null;
        }

        let items = [];

        this.state.modalItems.forEach(e => {
            items.push(
                <ListItem key={e} onPress={() => this.setSelectedOption(e)}>
                    <Text>{e}</Text>
                </ListItem>
            )
        });

        return this.renderModal(items);
    }
    renderModal(items) {
        return (
            <Modal
                animationType={"slide"}
                transparent={false}
                visible={true}   
            >
                <Container>
                    <Header>
                        <Left>
                            <TouchableOpacity 
                                style={[styles.buttonContainer, styles.buttonSmall, styles.buttonBlue]}
                                onPress={() => this.setState({modalItems: []})}
                            >
                                <Text style={[styles.buttonText]}>
                                    Back
                                </Text>
                            </TouchableOpacity>
                        </Left>    
                    </Header>
                    <Content>
                        <List>
                            {items}
                        </List>                           
                    </Content>
                </Container>
            </Modal>
        );
    }
    populateModalItem(itemName, callback) {
        let options = [];
       
        this.state.attributesData.category_attributes.forEach(e => {
            if(e.attribute.current_translation[0].name === itemName) {
                options = e.attribute.current_translation[0].options;
            }
        });

        this.setState({modalItems: options, callback: callback});
    }
    setSelectedOption(option) {
        this.setState({selectedOption: option, modalItems: []});
        this.state.callback(option);
    }
    storeSelectedItem(itemName, itemValue) {
        this.state.attributesData.category_attributes.forEach(e => {
            if(e.attribute.name === itemName) {
                if(e.attribute.type === 'Drop Down') {
                    store.update('storat.data', {
                        postAd: {['custom-' + e.attribute_id]: 'option-' + e.attribute.options.indexOf(itemValue)}
                    });
                }
                if(e.attribute.type === 'Option Box') {
                    store.update('storat.data', {
                        postAd: {['custom-' + e.attribute_id]: e.attribute.options.indexOf(itemValue)}
                    });
                }
            }
        });
    }
    postAd() {
        this.setState({isLoading: true}); 

        store.get('storat.data')
            .then((data) => {

                if(this.state.hideEmail) data.postAd['hide-email'] = 'on';
                if(this.state.hidePhone) data.postAd['hide-phone'] = 'on';

                var url = 'http://uae.storat.com/api/v2/ad?api_token=' + data.apiToken;
                var formBody = encodePostData({...data.postAd, api_token: data.apiToken});
                
                post(url, formBody)
                    .then(response => {
                        //console.log(response.json())
                        //store.update('storat.data', {postAd: {}})
                        Actions.adPlans();
                    })
                    .catch(err => {
                        //console.log(err)
                    });
            }); 
    }
    render() {        
        return (
            <Container style={[styles.container]}>
                {/* Statusbar */}
                <StatusBarTemplate />

                {/* Content */}
                {this.state.isLoading ? this.showIndicator() : this.showContent()}
            
                {/* FooterTabs */}
                <FooterTabComponent />

                {/* Modal */}
                {this.showModal()}

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    content: {
        padding: 10,
    },
    contentHeading: {
        backgroundColor: '#F8F8F8',
        padding: 15,
    },
    labelHeading: {
        marginBottom: 5,
    },
    heading: {
        fontSize: 13,
        fontWeight: '700',
        color: '#000'
    },
    blurb: {
        color: '#a2a2a2',
        fontSize: 16,
        marginBottom: 15
    },
    inputText: {
        borderBottomColor: '#646464',
        borderBottomWidth: 1
    },
    input: {
        // height: 60,
        // borderBottomWidth: 1,
        // borderBottomColor: '#d3d3d3',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#d3d3d3',
        marginBottom: 5,
        fontSize: 13,
        color: '#000',
        padding: 10,
        paddingLeft: 10
    },
    selectionInput: {
        color: '#94a6b1',
    },
    textArea: {
        height: 120,
        paddingTop: 10,
    },
    inputContainer: {
        marginTop: 30,      
        // padding: 15  
    },
    submitButtonContainer: {
        alignItems: 'center',
    },
    label: {
        color: '#000',
        fontSize: 18
    },
    buttonContainer: {
        backgroundColor: '#e24248',
        paddingVertical: 10,
        width: 120,
        borderRadius: 3
    },
    buttonSmall: {
        paddingVertical: 3,
        width: 60
    },
    buttonBlue: {
        backgroundColor: '#1470b9'
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    spacer: {
        height: 80
    },
    cancelIcon: {
        fontSize: 16,
    }
});
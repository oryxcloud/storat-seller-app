import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body, Segment, Button, Form, Item, Label } from 'native-base';
import ErrorLabel from '@templates/error-label';

export default class OptionBoxComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            checkedItem: null
        }
    }
    listElements(data) {
        let items = [];

        data.options.forEach((e) => {
            items.push(
                <ListItem key={e}>
                    <CheckBox 
                        checked={this.state.checkedItem === e} 
                        onPress={() => {
                            this.props.storeSelectedItem(this.props.data.name, e);
                            this.setState({checkedItem: e})
                        }}
                        color='#1470b9' 
                    />
                    <Body>
                        <Text> {e}</Text>
                    </Body>
                </ListItem>
            );
        });

        return items;
    }
    render() {
        return (
            <Content style={this.props.styles.field}>
                <View style={[this.props.styles.inputContainer]}>
                    {/* Label */}
                    <View style={this.props.styles.labelHeading}>
                        <Text style={this.props.styles.heading}>
                            {this.props.data.name}  
                        </Text>                        
                    </View>
                    {/* Checkbox */}
                    <Content>
                        {this.listElements(this.props.data)}
                    </Content>
                </View>
            </Content>
        );
    }
}
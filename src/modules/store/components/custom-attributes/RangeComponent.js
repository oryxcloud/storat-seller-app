import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import ErrorLabel from '@templates/error-label';

export default class RangeComponent extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            itemName: this.props.data.name,
            itemValue: null
        };
    }
    render() {
        return (
            <View style={this.props.styles.field}>
                <View style={[this.props.styles.inputContainer]}>
                    {/* Label */}
                    <View style={this.props.styles.labelHeading}>
                        <Text style={this.props.styles.heading}>
                            {this.props.data.name}    
                        </Text>                        
                    </View>
                    {/* Text field */}
                    <TextInput
                        style={this.props.styles.input}
                        autocapitalize="none"
                        autoCorrect={false}
                        keyboardType="email-address"
                        returnKeyType="next"
                        placeholder={'Enter ' + this.props.data.name}
                        placeholderTextColor="#94a6b1"
                        underlineColorAndroid="transparent"
                        onChangeText={ text => this.state.itemValue = text }
                    />
                    {/* Error label */}
                    <ErrorLabel 
                        showError={false} 
                        message={'This value should be between 40 to 80 characters'} 
                    />
                </View>
            </View>
        );
    }
}
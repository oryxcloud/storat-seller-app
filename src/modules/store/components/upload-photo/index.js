import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, Platform, Dimensions, ActivityIndicator } from 'react-native';
import { Actions } from "react-native-router-flux";
import ImagePicker from 'react-native-image-picker'
import { Container, Header, Content, List, ListItem, Card, CardItem, Icon, Body } from 'native-base';
import StatusBarTemplate from '@templates/statusbar';
import cameraImage from '@images/camera.jpg';
import encodePostData from '@lib/encodePostData';
import post from '@lib/post';
import store from 'react-native-simple-store';
import FooterTabComponent from '@modules/common/components/footer';

export default class PhotoComponent extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            imagesCount: 0,
            imageRefs: [],
            uploadedImages: {},
            primaryImage: null,
        };
        this.pickPhoto = this.pickPhoto.bind(this);
        this.storeUploadedImages = this.storeUploadedImages.bind(this);

        store.get('storat.data')
            .then(res => console.log(res))

    }
    pickPhoto() {
        let self = this;
        let opts = {
            title: 'Select an image',
            storageOptions: { skipBackup: true },
            allowsEditing: true,
            quality: 0,
            maxWidth: 100,
        };

        ImagePicker.showImagePicker(opts, (response) => {
            if(!response.didCancel && !response.error && !response.customButton) {
                let source = {
                    filename: `image_${(new Date()).getTime()}`,
                    filepath: response.uri.replace('file://', ''),
                    isStatic: true,
                    show: true,
                };

                if(Platform.OS === 'android') {
                    source.filepath = response.uri
                }

                self.pickedImage(source);
            }
        });
    }
    pickedImage(image) {
        this.state.imageRefs.push(image);
        this.setState({imagesCount: this.state.imageRefs.length});
        
        store.get('storat.data')
            .then(data => {
                let url = 'http://uae.storat.com/api/images/listing?api_token=' + data.apiToken;
                let body = new FormData();
                
                body.append('file', {uri: image.filepath, name: image.filename});
                
                fetch(url, { 
                    method: 'POST',
                    headers:{ 'Content-Type': 'multipart/form-data', 'X-Requested-With': 'XMLHttpRequest'}, 
                    body: body
                })
                .then((res) => res.text())
                .then((res) => {
                    this.setState({
                        uploadedImages: {
                            ...this.state.uploadedImages,
                            [image.filename]: res
                        }
                    })
                    console.log(this.state);
                })
                .catch(e => {console.log(e)})

            });
    }
    storeUploadedImages() {
        store.update('storat.data', {
            postAd: {
                ad_uploader: Object.values(this.state.uploadedImages),
                primaryImage: this.state.primaryImage
            }
        })
        .then(data => {
            Actions.adDetails();
        });
    }
    deleteImage(filename) {
        this.state.imageRefs.forEach((elem, index) => {
            if(elem.filename === filename) {
                this.state.imageRefs.splice(index, 1);
                return;
            }
        });

        this.setState({imagesCount: this.state.length});
    }
    setPrimaryImage(filename) {
        this.setState({primaryImage: filename});
    }
    renderImages() {
        let images = [];
        let buttonColor = {};

        this.state.imageRefs.forEach(e => {
            if(this.state.primaryImage === e.filename) { buttonColor = styles.buttonGreen }     
            else { buttonColor = styles.buttonBlue }               
            
            images.push(
                <View key={e.filename} style={[styles.uploadedImageContainer]}>  
                    {/* Delete Icon */}
                    <Icon 
                        name='close-circle' 
                        onPress={() => this.deleteImage(e.filename)}
                        style={[styles.iconDelete]} 
                    />
                 
                    {/* Image */}
                    <Image 
                        style={styles.selectedImage}
                        source={{uri: e.filepath}}
                    />
                    
                    {/* Set Primary Button */}
                    <View style={[styles.primaryButtonContainer]}>
                    {e.filename in this.state.uploadedImages ?
                        <TouchableOpacity   
                            onPress={() => this.setPrimaryImage(e.filename)}                      
                            style={[styles.buttonContainer, styles.buttonSmall, buttonColor]}
                        >
                            <Text style={[styles.buttonText, styles.smallButtonText]}>
                                Set Primary
                            </Text>
                        </TouchableOpacity>  
                    : 
                        <ActivityIndicator 
                            animating={true}
                            color={'#646464'}
                        />
                    }
                   </View>
                </View>  
            );
        });

        return images;
    }
    render() {
        return (
            <Container style={{backgroundColor: '#fff'}}>
                <StatusBarTemplate /> 
                <Content style={[styles.uploadPhotoContainer]}>
                    {/* Upload photos card */}
                    <Card>
                        {/* Card body */}
                        <CardItem>
                            <Body style={[styles.cardBody]}>  
                                {/* Camera image */}
                                <Image 
                                    style={styles.cameraImage}
                                    source={cameraImage}
                                />

                                {/* Pick Photo Button */}
                                <TouchableOpacity 
                                    onPress={this.pickPhoto} 
                                    style={styles.buttonContainer}
                                >
                                    <Text style={styles.buttonText}>
                                        Upload Photos
                                    </Text>
                                </TouchableOpacity>        
                            </Body>
                        </CardItem>
                    </Card>

                    {/* Skip Button */}
                    {this.state.imageRefs.length === 0 ?
                    <TouchableOpacity 
                            onPress={() => Actions.adDetails()} 
                            style={[styles.buttonContainer, styles.buttonLink, styles.buttonFullWidth]}
                        >
                            <Text style={[styles.smallButtonText, styles.buttonLinkText]}>
                                Skip this step
                            </Text>
                    </TouchableOpacity>     
                    : null}                   

                    {/* Uploaded photos card */}
                    {this.state.imageRefs.length !== 0 ?
                    <Card>
                        {/*  Card header */}
                        <CardItem header style={[styles.cardHeader]}>
                            <Text style={styles.uploadPhotosHeader}>
                                Selected images
                            </Text>
                        </CardItem>

                        {/* Card body */}
                        <CardItem>
                            <Body style={[styles.cardBody]}>
                                <Text style={[styles.imageContainer]}>                                                                      
                                    {this.renderImages()}                                   
                                </Text>       
                            </Body>
 
                        </CardItem>
                    </Card>
                    : null }

                    {/* Submit button */}
                    {this.state.imageRefs.length !== 0 ?
                    <TouchableOpacity 
                        onPress={() => this.storeUploadedImages()} 
                        style={[styles.buttonContainer, styles.buttonFullWidth]}
                    >
                        <Text style={styles.buttonText}>
                            Next
                        </Text>
                    
                    </TouchableOpacity> 
                    : null}
                </Content>

                {/* FooterTabs */}
                <FooterTabComponent />
                
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    heading: {
        fontSize: 25,
    },
    buttonContainer: {
        backgroundColor: '#e24248',
        paddingVertical: 10,
        width: 120,
        borderRadius: 3
    },
    buttonFullWidth: {
        width: undefined,
        borderRadius: 0,
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    buttonSmall: {
        paddingVertical: 3,
        width: 100,
    },
    buttonBlue: {
        backgroundColor: '#1470b9'
    },
    buttonGreen: {
        backgroundColor: 'green'
    },
    smallButtonText: {
        fontSize: 13,
        // fontWeight: '100',
    },
    buttonLink: {
        backgroundColor: '#f9f9f9',
        marginTop: 15,
    },
    buttonLinkText: {
        textAlign: 'center',
    },
    cardBody: {
        padding: 5,
        alignItems: 'center',
    },
    cardHeader: {
        backgroundColor: '#f7f7f7'
    },
    uploadPhotoContainer: {
        margin: 10,
        marginTop: 30
    },
    uploadPhotosHeader: {
        fontWeight: 'bold',
        fontSize: 13,
    },
    cameraImage: {
        height: 30,
        width: 30,
        marginBottom: 10
    },
    selectedImage: {
        maxWidth: 150,
        height: 130,
        marginBottom: 10,
        borderRadius: 2,
    },
    uploadedImageContainer: {
        padding: 10,
        height: 230,
        width: 150, 
    },
    iconDelete: {
        textAlign: 'center', 
        color: '#cd5c5c'
    },
    primaryButtonContainer: {
        justifyContent: 'center', 
        flexWrap: 'wrap', 
        flexDirection:'row'
    },
    spacer: {
        height: 80
    },
});
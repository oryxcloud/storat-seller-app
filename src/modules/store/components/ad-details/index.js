import React, { Component } from 'React';
import { View, ScrollView, TouchableOpacity, StyleSheet, Text, TextInput } from 'react-native';
import { Container, Content, Segment, Button, Form, Item, Label, List, ListItem, Card, CheckBox, Body, Icon } from 'native-base';
import store from 'react-native-simple-store';
import { Actions } from 'react-native-router-flux';
import ErrorLabel from '@templates/error-label';
import StatusBarTemplate from '@templates/statusbar';
import {empty, min, max} from '@lib/validation';
import Calendar from 'react-native-calendar-datepicker';
import Moment from 'moment';
import FooterTabComponent from '@modules/common/components/footer';

export default class DetailComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            details: {
                'ad-title': '',
                'ad-desc': '',
                'ad-price': '',
                preference: 'seller', 
                discountedPrice: '',
                discountPercentage: 0,
                discountIsInvalidMessage: '',
                isTimedPromotion: true,
                timedPromotionDate: Moment().add(14, 'days').startOf('day').format('DD-MM-YYYY'),
                selectedDate: Moment().add(14, 'days').startOf('day'),         
            },
            errors: {
                title: false,
                description: false,
            }
        };
    }
    setTitle(text) {
        this.state.details['ad-title'] = text;
        this.titleInvalid();
    }
    setDescription(text) {
        this.state.details['ad-desc'] = text;
        this.descriptionInvalid();
    }
    formInvalid() {
        if(this.titleInvalid() || this.descriptionInvalid()) {
            return true;
        }

        return false;
    }
    titleInvalid() {
        let text = this.state.details['ad-title'];
        let error = empty(text) || !min(text, 40); 

        if(error) {
            this.setState({errors: {
                ...this.state.errors,
                title: true
            }});
        } else {
            this.setState({errors: {
                ...this.state.errors,
                title: false
            }});            
        }

        return error;
    }
    descriptionInvalid() {
        let text = this.state.details['ad-desc'];
        let error = empty(text);

        if(error) {
            this.setState({errors: {
                ...this.state.errors,
                description: true
            }});
        } else {
            this.setState({errors: {
                ...this.state.errors,
                description: false
            }});            
        } 

        return error;
    }
    storeDetails() {
        if(this.formInvalid()) {
            return false;
        }

        store.update('storat.data', {postAd: this.state.details})
            .then((res) => {
                Actions.customAttributes()
            });
    }
    setPreference(value) {
        this.setState({
            details: {
                ...this.state.details,
                preference: value,
                'ad-price': ''
            }
        });
    }
    setPrice(price) {
        this.setState({
            details: {
                ...this.state.details,
                'ad-price': price,
                'preference': price ? '' : 'seller',
                discountedPrice: '',
                discountPercentage: 0,
                discountIsInvalidMessage: ''
            }
        });
    }
    setDiscountedPrice(price) {
        let originalPrice = parseInt(this.state.details['ad-price']) || 0;
        let discountedPrice = parseInt(price) || 0;
        let percentage = 0;
        let discountIsInvalid = false;
        
        if(originalPrice > 0) {
            percentage = parseInt(100 - (( discountedPrice / originalPrice ) * 100));
        }

        if(discountedPrice > originalPrice) {
            discountIsInvalid = true;
        }

        if(discountedPrice === 0) {
            discountIsInvalid = false;
            percentage = 0;
        }

        if(discountedPrice === originalPrice) {
            percentage = 100;
        }

        this.setState({
            details: {
                ...this.state.details,
                discountedPrice: price,
                discountPercentage: percentage,
                discountIsInvalidMessage: discountIsInvalid ? 'Invalid discount' : ''
            }
        });
        
    }
    setTimedPromotion() {
        let isTimedPromotion = ! this.state.details.isTimedPromotion;

        this.setState({
            details: {
                ...this.state.details,
                isTimedPromotion: isTimedPromotion
            }
        });
    }
    setTimedPromotionDate(moment) {
        this.setState({
            details: {
                ...this.state.details,
                timedPromotionDate: moment.date.format('DD-MM-YYYY'),
                selectedDate: moment.date
            }
        });
    }
    render() {
        return (
            <Container>
                {/* Statusbar */}
                <StatusBarTemplate />
                <Content style={[styles.container]}>
                {/* Title */}
                <View style={styles.field}>
                    <View style={[styles.inputContainer]}>
                        {/* Label */}
                        <View style={styles.labelHeading}>
                            <Text style={styles.heading}>Title</Text>                        
                        </View>
                        {/* Text field */}
                        <TextInput
                            style={styles.input}
                            maxLength={80}
                            autocapitalize="none"
                            autoCorrect={false}
                            returnKeyType="next"
                            placeholder="Enter an attractive title for your ad"
                            placeholderTextColor="#94a6b1"
                            underlineColorAndroid="transparent"
                            onChangeText={ text => this.setTitle(text) }
                        />
                        {/* Error label */}
                        <ErrorLabel 
                            showError={this.state.errors.title} 
                            message={'This value should be between 40 to 80 characters'} 
                        />
                    </View>
                </View>

                {/* Description */}
                <View style={styles.field}>
                    <View style={[styles.inputContainer]}>
                        {/* Label */}
                        <View style={styles.labelHeading}>
                            <Text style={styles.heading}>Description</Text>                        
                        </View>
                        {/* Text field */}
                        <TextInput
                            style={[styles.input, styles.textArea]}
                            multiline={true}
                            autocapitalize="none"
                            autoCorrect={false}
                            keyboardType="default"
                            returnKeyType="next"
                            placeholder="Enter a description for your ad"
                            placeholderTextColor="#94a6b1"
                            underlineColorAndroid="transparent"
                            onChangeText={ text => this.setDescription(text) }                            
                        />
                        {/* Error label */}
                        <ErrorLabel 
                            showError={this.state.errors.description} 
                            message={'This value is required'} 
                        />
                    </View>
                </View>

                {/* Price */}
                <View style={styles.field}>
                    {/* Original Price */}
                    <View style={[styles.inputContainer]}>
                        {/* Label */}
                        <View style={styles.labelHeading}>
                            <Text>
                                <Text style={styles.heading}>Original Price</Text>  
                                {/* Price button */}
                                <TouchableOpacity style={[styles.priceButton]}>
                                    <Text style={[styles.priceButtonText]}>AED</Text>
                                </TouchableOpacity>    
                            </Text>                  
                        </View>
                        {/* Text field */}
                        <TextInput
                            style={styles.input}
                            autocapitalize="none"
                            autoCorrect={false}
                            keyboardType="numeric"
                            returnKeyType="next"
                            placeholder="How much do you want for your item?"
                            placeholderTextColor="#94a6b1"
                            underlineColorAndroid="transparent"
                            value={this.state.details['ad-price']}
                            onChangeText={ text => { this.setPrice(text) } }                                                        
                        />
                    </View>

                    {/* Discounted Price */}
                    <View style={[styles.inputContainer]}>
                        {/* Label */}
                        <View style={styles.labelHeading}>
                            <Text>
                                <Text style={styles.heading}>Discounted Price</Text>  
                                {/* Price button */}
                                <TouchableOpacity style={[styles.priceButton]}>
                                    <Text style={[styles.priceButtonText]}>AED</Text>
                                </TouchableOpacity>    
                                
                                {this.state.details.discountIsInvalidMessage ?
                                    <Text style={[styles.red]}>     ({this.state.details.discountIsInvalidMessage})</Text>                                    
                                : 
                                    <Text style={[]}>     ({this.state.details.discountPercentage}%)</Text>
                                }
                            </Text>                  
                        </View>
                        {/* Text field */}
                        <TextInput
                            style={styles.input}
                            autocapitalize="none"
                            autoCorrect={false}
                            keyboardType="numeric"
                            returnKeyType="next"
                            placeholder="Add your discounted price here"
                            placeholderTextColor="#94a6b1"
                            underlineColorAndroid="transparent"
                            value={this.state.details.discountedPrice}
                            onChangeText={ text => { this.setDiscountedPrice(text) } }                                                        
                        />
                    </View>

                    {/* Preferences */}
                    {this.state.details.discountIsInvalidMessage === '' && this.state.details.discountedPrice > 0?
                    <View style={[styles.inputContainer]}>
                        <ListItem style={{width: 300, borderBottomWidth: 0, marginTop: -15, marginLeft: 0}}>
                            <CheckBox 
                                onPress={() => this.setTimedPromotion()}
                                checked={this.state.details.isTimedPromotion === true} 
                                color='#1470b9'  
                            />
                            <Body>
                                <Text style={[styles.checkText]}> Timed Promotion {this.state.details.isTimedPromotion ? '(' + this.state.details.timedPromotionDate + ')' : ''}</Text>
                            </Body>
                        </ListItem>

                        {this.state.details.isTimedPromotion ? 
                        <Calendar
                            onChange={(date) => this.setTimedPromotionDate({date})}
                            selected={this.state.details.selectedDate}
                            minDate={Moment().startOf('day')}
                            maxDate={Moment().add(10, 'years').startOf('day')}
                            style={{borderWidth: 1, borderColor: '#ccc', borderRadius: 5}}
                            barView={{backgroundColor: '#e9e9e9'}}
                        />
                        : null}
                        {/* <ListItem style={{width: 150, borderBottomWidth: 0}}>
                            <CheckBox 
                                onPress={() => this.setPreference('seller')}
                                checked={this.state.details.preference === 'seller'} 
                                color='#1470b9' 
                            />
                            <Body>
                                <Text style={[styles.checkText]}> Check with seller</Text>
                            </Body>
                        </ListItem> */}
                    </View>
                    : null}
                </View>

                {/* Youtube */}
                <View style={styles.field}>
                    <View style={[styles.inputContainer]}>
                        {/* Label */}
                        <View style={styles.labelHeading}>
                            <Text style={styles.heading}>Youtube Video Link</Text>                        
                        </View>
                        {/* Text field */}
                        <TextInput
                            style={styles.input}
                            autocapitalize="none"
                            autoCorrect={false}
                            keyboardType="email-address"
                            returnKeyType="next"
                            placeholder="https://www.youtube.com/watch?v=tdter"
                            placeholderTextColor="#94a6b1"
                            underlineColorAndroid="transparent"
                            onChangeText={ text => this.state.details['ad-youtube'] = text }
                        />
                        {/* Error label */}
                        <ErrorLabel 
                            showError={false} 
                            message={'This value is required'} 
                        />
                    </View>
                </View>

                {/* Submit */}
                <View style={styles.field}>
                    <View style={[styles.inputContainer, styles.submitButtonContainer]}>
                        {/* Submit Button */}
                        <TouchableOpacity 
                            onPress={() => this.storeDetails()} 
                            style={styles.buttonContainer}
                        >
                            <Text style={styles.buttonText}>Next</Text>
                        </TouchableOpacity>     
                    </View>
                </View>

                {/* Spacer */}
                <View style={[styles.spacer]}></View>

                </Content>

                {/* FooterTabs */}
                <FooterTabComponent />

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#fff',
    },
    content: {
        marginBottom: 10,
        // padding: 15,
        paddingBottom: 15, 
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ccc',
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 1.5,
        elevation: 3
    },
    field: {

    },
    contentHeading: {
        backgroundColor: '#F8F8F8',
        padding: 15,
    },
    labelHeading: {
        marginBottom: 5,
    },
    heading: {
        fontSize: 13,
        fontWeight: '700',
        color: '#000'
    },
    blurb: {
        color: '#a2a2a2',
        fontSize: 16,
        marginBottom: 15
    },
    inputText: {
        borderBottomColor: '#646464',
        borderBottomWidth: 1
    },
    input: {
        // height: 60,
        // borderBottomWidth: 1,
        // borderBottomColor: '#d3d3d3',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#d3d3d3',
        marginBottom: 5,
        fontSize: 13,
        color: '#000',
        padding: 10,
        paddingLeft: 10
    },
    textArea: {
        height: 120,
        paddingTop: 10,
    },
    inputContainer: {
        marginTop: 30,      
        // padding: 15  
    },
    submitButtonContainer: {
        alignItems: 'center',
    },
    label: {
        color: '#000',
        fontSize: 18
    },
    buttonContainer: {
        backgroundColor: '#e24248',
        paddingVertical: 10,
        width: 120,
        borderRadius: 3
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    priceButton: {
        marginTop: 3,
        marginLeft: 10,
        height: 15,
        width: 50,
        backgroundColor: '#1470b9',
    },
    priceButtonText: {
        textAlign: 'center',
        fontSize: 12,
        color: '#fff',
    },
    spacer: {
        height: 80
    },
    checkText: {
        fontSize: 13,
        fontWeight: 'bold'
    },
    red: {
        color: 'red'
    }
});
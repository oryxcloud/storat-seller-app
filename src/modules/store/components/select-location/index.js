import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Left, Icon, List, ListItem, Card, CardItem, Body } from 'native-base';
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import { CardTemplate, PickerTemplate, SpacerTemplate } from '@templates/ui';
import { SelectionBoxTemplate, ButtonTemplate, LabelTemplate } from '@templates/form';
import FooterTabComponent from '@modules/common/components/footer';

const map = {
    countries: 'country',
    states: 'state',
    cities: 'city',
    regions: 'region'
};

/**
 * NOTE:
 * 
 * We have assumed that for each country, there are states & cities. But a city may not have
 * any region. In that case, show next button and hide selection box for region.
 * 
 * Populating countries, states, cities, and regions are taken care of in their respective methods.
 */

export default class SelectLocationComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPicker: false,
            showNextButton: false,
            showRegionSelectionBox: true,
            locations: null,
            pickerItems: [],
            selectedLocation: {
                id: {
                    country: null,
                    state: null,
                    city: null,
                    region: null
                },
                label: {
                    country: null,
                    state: null,
                    city: null,
                    region: null
                }
            }
        };

        store.get('storat.data')
            .then(res => {
                console.log(res)
                this.state.locations = res.locations;

                this.setState({
                    selectedLocation: {
                        id: {
                            country: res.adCache.country.id || null,
                            state: res.adCache.state.id || null,
                            city: res.adCache.city.id || null,
                            region: res.adCache.region.id || null,
                        },
                        label: {
                            country: res.adCache.country.label || null,
                            state: res.adCache.state.label || null,
                            city: res.adCache.city.label || null,
                            region: res.adCache.region.label || null,
                        }
                    },
                    showNextButton: res.adCache.country.id ? 'true' : false,
                    showRegionSelectionBox: res.adCache.region.id ? 'true': false
                });
            }); 
    }
    showPickerItems(locationType) {
        switch(locationType) {
            case 'countries':
                this.populateCountries();
                break;
            case 'states':
                this.populateStates();
                break;
            case 'cities':
                this.populateCities();
                break;
            case 'regions':
                this.populateRegions();
        }
    }
    populateCountries() {
        this.state.locations.countries.slice(0, 1).forEach(e => {
            this.state.pickerItems.push(
                <ListItem key={e.id} onPress={ () => { this.setLocation(e.id, e.nameEn, 'countries') } }>
                    <Text>{e.nameEn}</Text>
                </ListItem>
            );
        });

        this.setState({ showPicker: true });        
    }
    populateStates() {
        let items = [];

        this.state.locations.states.forEach(e => {
            if(e.countryId === this.state.selectedLocation.id['country']) {
                items.push(
                    <ListItem key={e.id} onPress={ () => { this.setLocation(e.id, e.nameEn, 'states') } }>
                        <Text>{e.nameEn}</Text>
                    </ListItem>
                );
            }
        });

        if(items.length > 0) {
            this.state.pickerItems = items;
            this.setState({ showPicker: true });        
        } else {
            this.setState({ showPicker: false });                    
        }
    }
    populateCities() {
        let items = [];

        this.state.locations.cities.forEach(e => {
            if(e.stateId === this.state.selectedLocation.id['state']) {
                items.push(
                    <ListItem key={e.id} onPress={ () => { this.setLocation(e.id, e.nameEn, 'cities') } }>
                        <Text>{e.nameEn}</Text>
                    </ListItem>
                );
            }
        });

        if(items.length > 0) {
            this.state.pickerItems = items;
            this.setState({ showPicker: true });        
        } else {
            this.setState({ showPicker: false });                    
        }
    }
    populateRegions() {
        let items = [];

        this.state.locations.regions.forEach(e => {
            if(e.cityId === this.state.selectedLocation.id['city']) {
                items.push(
                    <ListItem key={e.id} onPress={ () => { this.setLocation(e.id, e.nameEn, 'regions') } }>
                        <Text>{e.nameEn}</Text>
                    </ListItem>
                );
            }
        });

        if(items.length > 0) {
            this.state.pickerItems = items;
            this.setState({ showPicker: true });        
        } else {
            this.setState({ showPicker: false });                    
        }
    }
    resetStateLabel() {
        this.state.selectedLocation.label.state = null;    
    }
    resetCityLabel() {
        this.state.selectedLocation.label.city = null;    
    }
    resetRegionLabel() {
        this.state.selectedLocation.label.region = null;    
    }
    hidePickerItems() {
        this.setState({
            pickerItems: [],
            showPicker: false
        });
    }
    getLocationSelectionBoxes() {
        return (
            <View style={{alignSelf: 'stretch'}}>
                
                {/* Country */}
                <LabelTemplate title='Country' />
                <SelectionBoxTemplate 
                    text={this.state.selectedLocation.label.country || 'Select one'}
                    onPress={() => { this.showPickerItems('countries') }}                
                />
                <SpacerTemplate height={20} />

                {/* State */}
                <LabelTemplate title='State' />                
                <SelectionBoxTemplate 
                    text={this.state.selectedLocation.label.state || 'Select one'}
                    onPress={() => { this.showPickerItems('states') }}                
                />
                <SpacerTemplate height={20} />

                {/* City */}
                <LabelTemplate title='City' />                
                <SelectionBoxTemplate 
                    text={this.state.selectedLocation.label.city || 'Select one'}
                    onPress={() => { this.showPickerItems('cities') }}                
                />

                {/* Region */}
                {this.state.showRegionSelectionBox ? 
                <View>
                    <SpacerTemplate height={20} />
                    <LabelTemplate title='Region' />                
                    <SelectionBoxTemplate 
                        text={this.state.selectedLocation.label.region || 'Select one'}
                        onPress={() => { this.showPickerItems('regions') }}                
                    />
                </View>
                : null}

            </View>
        );
    }
    storeLocation(locationType, locationId, locationText) {
        console.log([locationType, locationId, locationText])
        store.update('storat.data', {
            postAd: {[locationType]: locationId},
            adCache: {
                [locationType.split('_')[0]]: {id: locationId, label: locationText}
            }
        });
    }
    setLocation(locationId, locationText, locationType) {
        this.state.selectedLocation.id[map[locationType]] = locationId;
        this.state.selectedLocation.label[map[locationType]] = locationText;

        switch(locationType) {
            case 'countries':
                this.resetStateLabel();
                this.resetCityLabel();
                this.resetRegionLabel();
                this.state.showNextButton = false;
                this.storeLocation('country_id', locationId, locationText);
                this.resetCache('country');

                break;
            case 'states':
                this.resetCityLabel();
                this.resetRegionLabel();
                this.state.showNextButton = false;      
                this.storeLocation('state_id', locationId, locationText);    
                this.resetCache('state');                         
                
                break;  
            case 'cities':
                this.resetRegionLabel(); 
                this.storeLocation('city_id', locationId, locationText)   
                this.resetCache('city');                             
                
                if(! this.cityHasRegions()) {
                    this.state.showNextButton = true;
                    this.state.showRegionSelectionBox = false;
                } else {
                    this.state.showNextButton = false;         
                    this.state.showRegionSelectionBox = true;                    
                }

                break;
            case 'regions':
                this.state.showNextButton = true;
                this.storeLocation('region_id', locationId, locationText)             
                
        }

        this.hidePickerItems();
    }
    cityHasRegions() {
        let result = false;

        this.state.locations.regions.forEach(e => {
            if(e.cityId === this.state.selectedLocation.id.city) {
                result = true;
                return;
            }
        });

        return result;
    }
    resetCache(key) {
        store.get('storat.data')
            .then(res => {
                switch(key) {
                    case 'country':
                        store.update('storat.data', {
                            adCache: {
                                state: { id: null, label: null },
                                city: { id: null, label: null },
                                region: { id: null, label: null }
                            }
                        });

                        break;
                    case 'state':
                        store.update('storat.data', {
                            adCache: {
                                city: { id: null, label: null },
                                region: { id: null, label: null }
                            }
                        })

                        break;
                    case 'city':
                        store.update('storat.data', {
                            adCache: {
                                region: { id: null, label: null }
                            }
                        });

                        break;
                }
            });
    }
    render() {
        return (
            <Container>
                <StatusBarTemplate />   
                <Content style={[styles.container]}>
                    <CardTemplate 
                        title='Select Location'
                        body={this.getLocationSelectionBoxes()}
                    />
                    {this.state.showNextButton  && ! this.props.hideNextButtonWhenFilter ? 
                        <ButtonTemplate 
                            title='Next' 
                            color='red'
                            onPress={ () => { Actions.uploadPhoto() } }    
                        />
                    : null}
                </Content>

                {/* FooterTabs */}
                <FooterTabComponent />

                {/* Picker */}
                <PickerTemplate 
                    show={this.state.showPicker}
                    items={this.state.pickerItems}
                    closePicker={() => { this.hidePickerItems() }} 
                    buttonTitle='Back'
                    buttonSize='small'
                    buttonColor='blue' 
                />
            </Container>
       );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        padding: 15,
    },
});
import { connect } from 'react-redux';
import AdDetailsComponent from '@modules/store/components/ad-details';
//import * as AdDetailsActions from '@modules/store/actions/ad-details';

const mapStateToProps = state => ({
   categories: state.store.categories
});    

const mapDispatchToProps = dispatch => ({
 
});

export default connect(mapStateToProps, mapDispatchToProps)(AdDetailsComponent);

import { connect } from 'react-redux';
import SelectCategoryComponent from '@modules/store/components/select-category';
import * as SelectCategoryActions from '@modules/store/actions/select-category';

const mapStateToProps = state => ({
   categories: state.store.categories
});    

const mapDispatchToProps = dispatch => ({
 
});

export default connect(mapStateToProps, SelectCategoryActions)(SelectCategoryComponent);

import { connect } from 'react-redux';
import WelcomeComponent from '@modules/store/components/welcome';
import * as WelcomeActions from '@modules/store/actions/welcome';

const mapStateToProps = state => ({
   info: state.store.info
});    

const mapDispatchToProps = dispatch => ({
 
});

export default connect(mapStateToProps, WelcomeActions)(WelcomeComponent);

import { connect } from 'react-redux';
import UploadPhotoComponent from '@modules/store/components/upload-photo';
import * as UploadPhotoActions from '@modules/store/actions/upload-photo';

const mapStateToProps = state => ({
   categories: state.store.categories
});    

const mapDispatchToProps = dispatch => ({
 
});

export default connect(mapStateToProps, UploadPhotoActions)(UploadPhotoComponent);

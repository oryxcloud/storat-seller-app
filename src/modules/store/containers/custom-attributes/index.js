import { connect } from 'react-redux';
import CustomAttributesComponent from '@modules/store/components/custom-attributes';
//import * as CustomAttributesActions from '@modules/store/actions/ad-details';

const mapStateToProps = state => ({
   categories: state.store.categories
});    

const mapDispatchToProps = dispatch => ({
 
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomAttributesComponent);

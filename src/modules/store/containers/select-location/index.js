import { connect } from 'react-redux';
import SelectLocationComponent from '@modules/store/components/select-location';
import * as SelectLocationActions from '@modules/store/actions/select-location';

const mapStateToProps = state => ({
   locations: state.store.locations
});    

const mapDispatchToProps = dispatch => ({
 
});

export default connect(mapStateToProps, SelectLocationActions)(SelectLocationComponent);

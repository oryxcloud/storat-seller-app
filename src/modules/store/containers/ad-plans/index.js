import { connect } from 'react-redux';
import AdPlansComponent from '@modules/store/components/ad-plans';
//import * as AdPlansActions from '@modules/store/actions/ad-plans';

const mapStateToProps = state => ({
   categories: state.store.categories
});    

const mapDispatchToProps = dispatch => ({
 
});

export default connect(mapStateToProps, mapDispatchToProps)(AdPlansComponent);

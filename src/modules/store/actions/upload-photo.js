export const updateStoreCategories = (res) => {
    return {
        type: 'UPDATE_STORE_CATEGORIES',
        categories: res
    }
}
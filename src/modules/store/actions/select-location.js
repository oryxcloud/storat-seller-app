export const updateStoreLocations = (res) => {
    return {
        type: 'UPDATE_STORE_LOCATIONS',
        locations: res
    }
}
import WelcomeContainer from './containers/welcome';
import SelectCategoryContainer from './containers/select-category';
import SelectLocationContainer from './containers/select-location';
import UploadPhotoContainer from './containers/upload-photo';
import AdDetailsContainer from './containers/ad-details';
import CustomAttributesContainer from './containers/custom-attributes';
import AdPlansContainer from './containers/ad-plans';

export default class WelcomeModule {
    static welcome() {
        return WelcomeContainer;
    }
    static selectCategory() {
        return SelectCategoryContainer;
    }
    static selectLocation() {
        return SelectLocationContainer;
    }
    static uploadPhoto() {
        return UploadPhotoContainer;
    }
    static adDetails() {
        return AdDetailsContainer;
    }
    static customAttributes() {
        return CustomAttributesContainer;
    }
    static adPlans() {
        return AdPlansContainer;
    }
}
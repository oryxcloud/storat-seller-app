export default {
    container: {
        padding: 5,
        marginTop: 30,
    },
    storeContainer: {
        flexDirection: 'row', 
        flexWrap: 'nowrap',
    },
    storeInfoContainer: {
        paddingLeft: 10
    },
    storeHeading: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#0659a0',
        marginBottom: 5,
    },
    storeBlurb: {
        marginBottom: 5
    },
    lightText: {
        fontSize: 12,
        color: '#787d89',
    }
};
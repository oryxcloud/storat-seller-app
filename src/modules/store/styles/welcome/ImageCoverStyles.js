export default {
    storatCoverContainer: {
        width: null,
        height: 130,
        backgroundColor: '#f9f9f9'
    },
    storatCover: {
        resizeMode: 'cover',
        width: null,
        height: 130,
        marginBottom: 5,
    }
};
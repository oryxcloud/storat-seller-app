export default {
    container: {
        flex: 1,
        margin: 10,
        marginTop: 30,
    },
    cardBody: {
        // padding: 20,
        // paddingVertical: 20,
        alignItems: 'center',
    },
    cardHeader: {
        backgroundColor: '#f7f7f7'
    },
    buttonContainer: {
        backgroundColor: '#e24248',
        paddingVertical: 10,
        width: 120,
        borderRadius: 3
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }
};
import React, { Component } from 'react';
import { Container, Content, List, ListItem, Text } from 'native-base';
import StatusBarTemplate from '@templates/statusbar';
import { Actions } from "react-native-router-flux";


export default class CategoryComponent extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            categories: [
                // 'Properties',
                // 'Motors',
                // 'Classifieds',
                // 'Services',
                // 'Jobs',
                // 'Community',
                // 'Financials',
                // 'Restaurants',
                // 'Education',
                // 'Health & Wellness'
            ],
            hasErrored: false,
            isLoading: false
        }
    }
    componentDidMount() {
        //this.fetchData('http://facebook.github.io/react-native/movies.json');
        //this.fetchData('https://uae.storat.com/api/v2/categories/?api_token=vl0yii4epQ7TBO077LNaomxo');
        this.fetchData('https://uae.storat.com/api/v2/categories');
        //this.fetchData('http://api.hamooz.com/v1/category?username=hamooz&password=hamooz@123');
    }
    fetchData(url) {
        this.setState({isLoading: true});

        return fetch(url)
                    .then((response) => response.json())
                    .then((responseJson) => {
                        console.log(responseJson);
                        let categories = [];
                        responseJson.forEach(function(category) {
                            if(!category.parentId) {
                                categories.push(category.nameEn);
                            }
                        });
                        
                        this.setState({categories: categories})    
                    })
                    .catch((error) => {
                        console.log(error);
                        this.setState({hasErrored: true});
                    });
    }
    render() {
        if(this.state.categories.length) {
            return (
                <Container style={{backgroundColor: '#fff'}}>
                    <StatusBarTemplate />
                    <Content>
                        <List 
                            dataArray={this.state.categories}
                            renderRow={(category) => 
                                <ListItem onPress={() => Actions.country()}>
                                    <Text>{category}</Text>
                                </ListItem>
                            }
                        >
                        </List>
                    </Content> 
                </Container>
            );
        }

        return (
            <Text>Please wait..</Text>
        );
    }
}
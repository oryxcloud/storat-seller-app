import LoginContainer from './containers/login';
import RegisterContainer from './containers/register';
import LoginRegisterContainer from './containers/login-register';
import VerifyMobileContainer from './containers/verify-mobile';

export default class AuthModule {
    static login() {
        return LoginContainer;
    }
    static register() {
        return RegisterContainer;
    }
    static loginOrRegister() {
        return LoginRegisterContainer;
    }
    static verifyMobile() {
        return VerifyMobileContainer;
    }
}
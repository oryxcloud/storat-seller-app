import React, { Component } from 'react';
// import { Text, View } from 'react-native';
import {
    View,
    Modal,
    Linking,
    TextInput,
    StatusBar,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Image
} from 'react-native';
import { Text, Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right, Item, Input } from 'native-base';
import LoginFormStyles from '@modules/auth/styles/login/LoginFormStyles';
import StatusBarTemplate from '@templates/statusbar';
import ToastTemplate from '@templates/toast';
import { Actions } from 'react-native-router-flux';
import flagArab from '@images/flags/arab.png';


export default class VerifyMobileComponent extends Component {
    static navigationOptions = {
        headerLeft:null
    }
    constructor(props) {
        super(props);

        this.state = {
            number: null,
            verified: false,
            showToast: false
        };

        // this.verifyMobile = this.verifyMobile.bind(this);
    }
    storeNumber(number) {
        this.setState({number: number});
    }
    verifyMobile() {
        if(this.state.number === '55555') {
            this.setState({verified: true, showToast: true});

            setTimeout(() => {
                Actions.browseAdsCategories({type: 'replace'});
            }, 1000)
        } else {
            this.setState({verified: false, showToast: true});
        }
    }
    validate() {

    }
    render() {
        return (
            <Container style={{backgroundColor: '#fff'}}>
                {/* Status bar */}
                <StatusBarTemplate />

                <Content style={{padding: 10}}>

                {/* Toast Template */}
                <View style={{marginTop: 10, marginBottom: 10}}>
                    <ToastTemplate 
                        type={this.state.verified ? 'success' : 'danger'}
                        show={this.state.showToast} 
                        message={this.state.verified ? 'Success! Your mobile number has been verified.' : 'Failure! Could not verify your number.'} 
                    />
                </View>

                {/* Input Text: Mobile */}
                <View style={[styles.inputContainer, {marginBottom: 25}]}>
                    
                    {/* Mobile Verification Heading */}
                    <Text style={{fontWeight: 'bold', fontSize: 25, marginBottom: 30, alignSelf: 'center'}}>
                        Mobile Verification
                    </Text>
                    
                    {/* Mobile Verification Sub-Heading */}
                    <Text style={{marginBottom: 3, alignSelf: 'center', fontWeight: 'bold', marginBottom: 30}}>
                        Please make sure you have entered a correct mobile number to recieve the SMS
                    </Text>
                    
                    {/* Mobile Number Input Box */}
                    <View style={{flexDirection: 'row'}}>

                        {/* Countries Flag */}
                        <TouchableOpacity onPress={() => this.showCountriesPicker()}>
                            <Image source={flagArab} style={{height: 36, width: 70, marginRight: 8, marginTop: 2}} />
                        </TouchableOpacity>

                        {/* Mobile Number Input */}
                        <View style={{flex: 1}}>
                            <TextInput
                                style={[styles.input]}
                                autocapitalize="none"
                                autoCorrect={false}
                                keyboardType="phone-pad"
                                returnKeyType="next"
                                placeholder="3312 3456"
                                placeholderTextColor="#8095a2"
                                underlineColorAndroid="transparent"
                                value={'33123456'}
                                onChangeText={ text => this.validate('mobile', text) }
                            />
                        </View>
                    </View>
                    {/* <ErrorLabel 
                        showError={this.state.mobile.error} 
                        message={'Mobile number is required'} 
                    /> */}
                </View>

                <Card style={{backgroundColor: '#1471b9'}}>
                    <CardItem header style={{backgroundColor: '#1471b9'}}>
                        <Text style={{fontWeight: 'bold', color: '#fff'}}>
                            Enter verification code sent to your mobile by SMS
                        </Text>
                    </CardItem>
                    
                    <CardItem>
                        <Body>
                            <Item rounded success>
                                <Input 
                                    style={{fontSize: 20, paddingLeft: 20}}
                                    value={this.state.number}
                                    keyboardType='numeric'
                                    placeholder='Enter Code Here'
                                    onChangeText={(text) => this.storeNumber(text)}/>
                            </Item>
                        </Body>
                    </CardItem>
                    
                    <CardItem footer style={{justifyContent: 'center'}}>
                        <Button danger small style={{marginTop: 10, marginRight: 10}} onPress={() => this.verifyMobile()}>
                            <Text style={{color: '#fff', fontWeight: 'bold'}}>Verify</Text>
                        </Button>

                        <Button light small style={{marginTop: 10}} onPress={() => Actions.store()}>
                            <Text style={{color: '#000', fontWeight: 'bold'}}>Skip</Text>
                        </Button>
                    </CardItem>
                </Card>
                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create(LoginFormStyles);

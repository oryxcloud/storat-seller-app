import React, { Component } from 'react';
// import { Text, View } from 'react-native';
import {
    View,
    Modal,
    Linking,
    TextInput,
    StatusBar,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Image
} from 'react-native';
import { Text, Container, Header, Content, Card, CardItem, Icon, Right, List, ListItem, Left, Body } from 'native-base';
import LoginFormStyles from '@modules/auth/styles/login/LoginFormStyles';
import StatusBarTemplate from '@templates/statusbar';
import ToastTemplate from '@templates/toast';
import { Actions } from 'react-native-router-flux';
import flagArab from '@images/flags/arab.png';
import FooterTabComponent from '@modules/common/components/footer';


export default class VerifyMobileComponent extends Component {
    static navigationOptions = {
        headerLeft:null
    }
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Container style={{backgroundColor: '#fff'}}>
                {/* Status bar */}
                <StatusBarTemplate />

                <Content style={{padding: 10}}>
                <Card>
                    <CardItem header style={{backgroundColor: '#f7f7f7'}}>
                        <Text>To post an ad, please Login/Register</Text>
                    </CardItem>

                    <CardItem button onPress={() => Actions.login()}>
                        <Icon active name="lock" style={{color: '#1470b9'}} />
                        <Text style={{color: '#1470b9', fontSize: 14}}>Login</Text>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </CardItem>
                    <CardItem button onPress={() => Actions.register()}>
                        <Icon active name="unlock" style={{color: '#1470b9'}} />
                        <Text style={{color: '#1470b9', fontSize: 14}}>Register</Text>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </CardItem>

                </Card>
                </Content>

                {/* FooterTabs */}
                <FooterTabComponent />
            </Container>
        );
    }
}

const styles = StyleSheet.create(LoginFormStyles);

import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'native-base';

export default class SocialLogin extends Component {
    render() {
        return (
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 30,}}>
                <View style={[styles.line]}></View>
                <View style={[styles.horizontalSpacer]}></View>
                <Text style={[styles.greyText]}>OR</Text>
                <View style={[styles.horizontalSpacer]}></View>                
                <View style={[styles.line]}></View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    line: {
        height: 1,
        width: 90,
        backgroundColor: '#d3d3d3',
    },
    horizontalSpacer: {
        width: 10,
    },
    greyText: {
        color: '#ccc',
        fontSize: 14,
        fontWeight: 'bold',
    }
});
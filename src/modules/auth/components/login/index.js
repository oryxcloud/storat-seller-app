import React, { Component } from 'react';
import { Text } from 'react-native';
import { Root, Container } from 'native-base';
import StatusBarTemplate from '@templates/statusbar';
import ImageLogo from './ImageLogo'; 
import ImageCover from './ImageCover'; 
import SocialLogin from './SocialLogin'; 
import Separator from './Separator'; 
import LoginForm from './LoginForm'; 

export default class LoginComponent extends Component {
    static navigationOptions = {
        // headerLeft:null
    }
    render() {
        return (
            <Container style={{backgroundColor: '#fff'}}>
                {/* Status bar */}
                <StatusBarTemplate />
                {/* Social login */}
                <SocialLogin 
                    loginFailed={this.props.loginFailed} 
                    loginSuccess={this.props.loginSuccess}
                    loginRedirect={this.props.loginRedirect}
                />
                {/* Separator */}
                <Separator />
                {/* Login */}
                <LoginForm 
                    showSpinner={this.props.showSpinner}
                    toastType={this.props.toastType}
                    showToast={this.props.showToast}
                    toastMessage={this.props.toastMessage}
                    loginFailed={this.props.loginFailed} 
                    loginSuccess={this.props.loginSuccess}
                    loginRequest={this.props.loginRequest}
                    loginRedirect={this.props.loginRedirect}
                    loginButtonText={this.props.loginButtonText}
                    unexpectedError={this.props.unexpectedError}
                />
            </Container>
        );
    }
}
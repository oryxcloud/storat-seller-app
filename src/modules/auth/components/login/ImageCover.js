import React, { Component } from 'react';
import {
    Image,
    StyleSheet
} from 'react-native';
import ImageCoverStyles from '@modules/auth/styles/login/ImageCoverStyles';
import coverImage from '@images/bg.jpg';

export default class ImageCover extends Component {
    render() {
        return (
            <Image 
                style={styles.storatCover}
                source={coverImage}
            />
        );
    }
}

const styles = StyleSheet.create(ImageCoverStyles);
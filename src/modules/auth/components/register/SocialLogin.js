import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Platform, Linking } from 'react-native';
import { Actions } from 'react-native-router-flux';
import store from 'react-native-simple-store';
import SafariView from 'react-native-safari-view';
import { Icon } from 'native-base';
import encodePostData from '@lib/encodePostData';
import post from '@lib/post';
import ToastTemplate from '@templates/toast';


export default class SocialLogin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            token: null,
            toast: {
                type: '',
                visible: false,
                message: ''
            }
        };
    }
    componentDidMount() {
        Linking.addEventListener('url', (e) => this.handleURL(e));
    }
    componentWillUnmount() {
        Linking.removeEventListener('url', (e) => this.handleURL(e));        
    }
    handleURL(event) {
        console.log(event.url)
        
        if(event.url.search('/google') !== -1) {
            this.verifyGoogleLogin(event.url);
        } else {
            this.verifyFacebookLogin(event.url);
        }

        if(Platform.OS === 'ios') {
            SafariView.dismiss();
        }
    }
    handleFacebookLogin() {
        let url = '';
        
        url += 'https://graph.facebook.com/oauth/authorize';
        url += '?response_type=token';
        url += '&client_id='+'770624093121108';
        url += '&redirect_uri=fb770624093121108://authorize';
        url += '&scope=email';

        this.openURL(url);
    }
    handleGooglePlusLogin() {
        let url = '';

        url += 'https://accounts.google.com/o/oauth2/v2/auth';
        url += '?scope=email';
        url += '&response_type=code';
        url += '&state=security_token%3D138r5719ru3e1%26url%3Dhttps://oauth2.example.com/token';        
        url += '&redirect_uri=com.googleusercontent.apps.113961123563-79i5g02knp2mbr2gdh4bdrc2242ku4uk:/google';
        url += '&client_id=113961123563-79i5g02knp2mbr2gdh4bdrc2242ku4uk.apps.googleusercontent.com';
       
        this.openURL(url);
    }
    verifyGoogleLogin(url) {
        let code = '';
        let formBody = null;
        
        code += url.split('&').reverse()[0].split('=')[1];
        code += code.slice(0, code.length - 1);

        formBody = encodePostData({
            code: code,
            client_id: '113961123563-79i5g02knp2mbr2gdh4bdrc2242ku4uk.apps.googleusercontent.com',
            redirect_uri: 'com.googleusercontent.apps.113961123563-79i5g02knp2mbr2gdh4bdrc2242ku4uk:/google',
            grant_type: 'authorization_code'
        });

        post('https://accounts.google.com/o/oauth2/token', formBody)
            .then(response => response.json())
            .then(response => {
                fetch('https://www.googleapis.com/plus/v1/people/me?access_token=' + response.access_token)
                    .then(res => res.json()) 
                    .then(res => {
                        console.log(res); 
                        this.verifyCredentials(res.emails[0].value, res.name.givenName, res.name.familyName)
                    });
            });
    }
    verifyFacebookLogin(url) {
        const facebookToken = url.split('=')[1].split('&')[0];
        
        this.setState({token: facebookToken}, () => {
            post('https://graph.facebook.com/v2.8/me?fields=id,name,email&access_token=' + facebookToken)
                .then(res => res.json())
                .then(res => {
                    console.log(res); 
                    this.verifyCredentials(res.email, res.name.split(' ')[0], res.name.split(' ')[1] || '')
                })
        });
    }
    verifyCredentials(email, firstName, lastName) {
        let url = 'https://uae.storat.com/api/v2/social/register';
        let formBody = encodePostData({
            firstName: firstName,
            lastName: lastName,
            email: email
        });

        post(url, formBody)
            .then(response => response.json())
            .then(response => {
                console.log(response);

                if(response.meta.code === 400) {
                    // The email has already been taken.
                    this.setState({toast: {
                        type: 'danger',
                        visible: true,
                        message: 'The email has already been taken.'
                    }});

                    return;
                }

                if(response.meta.code === 200) {

                    store.update('storat.data', {
                        apiToken: response.data.user.api_token,
                        userID: response.data.user.id,
                        isLoggedIn: true,
                        firstName: response.data.user.first_name,
                        lastName: response.data.user.last_name,
                        ext: null,
                        mobile: null, 
                        adCache: {
                            country: {id: null, label: null},
                            state: {id: null, label: null},
                            city: {id: null, label: null},
                            region: {id: null, label: null},
                            categories: null
                        }
                    });
                  
                    // success
                    this.setState({toast: {
                        type: 'success',
                        visible: true,
                        message: 'Success!! Your account has been registered.'
                    }});

                    // take to mobile verification
                    setInterval(() => {
                        Actions.verifyMobile();
                    }, 2000);

                } else {
                    store.update('storat.data', {
                        apiToken: null
                    });
                    
                    // failure
                    this.setState({toast: {
                        type: 'danger',
                        visible: true,
                        message: 'Sorry!! We couldn\'t create you account. Please try again.'
                    }});
                }
            })
            .catch(err => {
                console.log(err)
                
                // something went wrong
                this.setState({toast: {
                    type: 'danger',
                    visible: true,
                    message: 'Whoops!! We are facing some technical issues. Please try again.'
                }});
            });
    }
    openURL(url) {
        if(Platform.OS === 'ios') {
            SafariView.show({url: url});
        } else {
            Linking.openURL(url);
        }
    }
    loginSuccess() {
        let self = this;

        self.props.loginSuccess()
        
        setTimeout(function() {
            self.loginRedirect();
        }, 1000);
    }
    loginRedirect() {
        this.props.loginRedirect();

        setTimeout(() => {
            Actions.store();
        }, 1000)
    }
    render() {
        return (

            <View>
                {/* Toast Danger */}
                <View style={{flex: 1}}>
                    <ToastTemplate 
                        type={this.state.toast.type}
                        show={this.state.toast.visible} 
                        message={this.state.toast.message} 
                    />
                </View>

                {/* Social Buttons */}
                <View style={[styles.container]}>

                    {/* Facebook button */}
                    <TouchableOpacity style={[styles.facebook, styles.socialButton]} onPress={() => this.handleFacebookLogin()}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon name='logo-facebook' style={[styles.icon]} />
                            <Text style={[styles.socialText]}>
                                LOGIN WITH FACEBOOK
                            </Text>
                        </View>
                    </TouchableOpacity>

                    {/* Spacer */}
                    <View style={[styles.horizontalSpacer]}></View>

                    {/* Google+ button */}
                    <TouchableOpacity style={[styles.google, styles.socialButton]} onPress={() => this.handleGooglePlusLogin()}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon name='logo-googleplus' style={[styles.icon]} />
                            <Text style={[styles.socialText]}>
                                LOGIN WITH GOOGLE
                            </Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row', 
        justifyContent: 'center', 
        marginTop: 30
    },
    facebook: {
        backgroundColor: '#588cc8',
    },
    google: {
        backgroundColor: '#f94e51',
    },
    socialButton: {
        width: 150,        
        paddingVertical: 5,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 30
    },
    socialText: {
        color: '#fff',
        fontSize: 9,
    },
    icon: {
        color: '#fff',
        fontSize: 16,
        marginRight: 5
    },
    horizontalSpacer: {
        width: 10,
    }
});
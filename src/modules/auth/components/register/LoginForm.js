import React, { Component } from 'react';
import {
    Text,
    View,
    Modal,
    Linking,
    TextInput,
    StatusBar,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Button,
    Image
} from 'react-native';
import { Container, Header, Content, ListItem, CheckBox, Body } from 'native-base';
import { Actions } from 'react-native-router-flux';
import store from 'react-native-simple-store';
import { Spinner, Toast } from 'native-base';
import LoginFormStyles from '@modules/auth/styles/login/LoginFormStyles';
import ErrorLabel from './ErrorLabel';
import ForgotPassword from './ForgotPassword'; 
import ToastTemplate from '@templates/toast';
import post from '@lib/post';
import {email, empty} from '@lib/validation';
import encodePostData from '@lib/encodePostData';
import { PickerTemplate } from '@templates/ui';
import CountryPickerComponent from '@modules/common/components/countries';
import flagQatar from '@images/flags/qatar.png';
import flagBahrain from '@images/flags/bahrain.png';
import flagKuwait from '@images/flags/kuwait.png';
import flagSaudi from '@images/flags/saudi.png';
import flagArab from '@images/flags/arab.png';

export default class LoginForm extends Component {
    constructor(props) {
        super(props);
       
        this.state = {
            firstName: {
                error: false,
                value: ''
            },
            lastName: {
                error: false,
                value: ''
            },
            email: {
                error: false,
                value: ''
            },
            password: {
                error: false,
                value: ''
            },
            mobile: {
                error: false,
                value: ''
            },
            agreeToTerms: false,
            accountType: 'individual',
            showForgotPasswordModal: false,
            showPicker: false,
            countriesPickerItems: [],
            flags: {
                arab: flagArab,
                qatar: flagQatar,
                saudi: flagSaudi,
                kuwait: flagKuwait,
                bahrain: flagBahrain
            },
            exts: {
                arab: ['United Arab Emirates', '971'],
                qatar: ['Qatar', '974'],
                saudi: ['Saudi Arabia', '966'],
                kuwait: ['Kuwait', '965'],
                bahrain: ['Bahrain', '973']
            },
            activeFlag: 'arab',
            activeMobileCodeHint: '050 123 4567',
            toast: {
                type: '',
                visible: false,
                message: ''
            }
        };

        this.submit = this.submit.bind(this);
        this.setAccountType.bind(this);
        this.hideForgotPasswordModal = this.hideForgotPasswordModal.bind(this);
        this.setCountriesPicker();
    }
    hidePickerItems() {
        this.setState({
            showPicker: false
        });
    }
    setMobileFlag(country, code) {
        // alert(country);
        this.setState({mobile: {error: false, value: ''}, activeFlag: country, activeMobileCodeHint: code, showPicker: false});
    }
    setCountriesPicker() {
        this.state.countriesPickerItems.push(
            <CountryPickerComponent closePicker={(country, code) => { this.setMobileFlag(country, code) }}/>
        );
    }
    showCountriesPicker() {
        this.setState({showPicker: true});
    }
    validate(field, value) {
        switch(field) {
            case 'firstName':
                this.setState({
                    firstName: {
                        error: empty(value),
                        value: value
                    }
                }) 
                break;
            case 'lastName':
                this.setState({
                    lastName: {
                        error: empty(value),
                        value: value
                    }
                }) 
                break;
            case 'email':
                this.setState({
                    email: {
                        error: ! email(value),
                        value: value
                    }
                }) 
                break;
            case 'password':
                this.setState({
                    password: {
                        error: empty(value),
                        value: value
                    }
                }) 
                break;
            case 'mobile': 
                this.setState({
                    mobile: {
                        error: empty(value) || (value.length !== this.state.activeMobileCodeHint.split(' ').join('').length),
                        value: value
                    }
                })
        }
    }
    firstNameInvalid() {
        this.validate('firstName', this.state.firstName.value);
        return empty(this.state.firstName.value) || this.state.firstName.error;
    }          
    lastNameInvalid() {
        this.validate('lastName', this.state.lastName.value);
        return empty(this.state.lastName.value) || this.state.lastName.error;
    }  
    emailInvalid() {
        this.validate('email', this.state.email.value);        
        return ! this.state.email.value.trim() || this.state.email.error;
    }   
    passwordInvalid() {
        this.validate('password', this.state.password.value);
        return empty(this.state.password.value) || this.state.password.error;
    }          
    mobileInvalid() {
        this.validate('mobile', this.state.mobile.value);        
        return 
            empty(this.state.mobile.value) || 
            this.state.mobile.error || 
            (this.state.mobile.value.length == this.state.activeMobileCodeHint.split(' ').join('').length)
        ;
    }          
    submit() {
        if(! this.state.agreeToTerms) {
            alert('Please agree to Storat\'s Terms & Conditions');
            return false;
        }

        if(this.firstNameInvalid() || this.lastNameInvalid() || this.emailInvalid() || this.passwordInvalid() || this.mobileInvalid()) {
            return false;
        }
        
        this.verifyRegister();
    }
    verifyRegister() {
        let url = 'https://uae.storat.com/api/v2/register';
        let formBody = encodePostData({
            firstName: this.state.firstName.value,
            lastName: this.state.lastName.value,
            email: this.state.email.value,
            password: this.state.password.value,
            ext: this.state.exts[this.state.activeFlag][1],
            mobile: this.state.mobile.value,
            terms: 1,
            // account_type: this.state.accountType
        });

        this.registerNewUser(url, formBody);
    }
    registerSuccess() {
        let self = this;

        self.props.registerSuccess()
        
        setTimeout(function() {
            self.registerRedirect();
        }, 1000);
    }
    registerRedirect() {
        this.props.registerRedirect();

        setTimeout(() => {
            Actions.store();
        }, 1000)
    }
    registerNewUser(url, formBody) {
        post(url, formBody)
            .then(response => response.json())
            .then(response => {
                console.log(response);

                if(response.meta.code === 400) {
                    // The email has already been taken.
                    this.setState({toast: {
                        type: 'danger',
                        visible: true,
                        message: 'The email has already been taken.'
                    }});

                    return;
                }

                if(response.meta.code === 200) {

                    store.update('storat.data', {
                        apiToken: response.data.user.api_token,
                        userID: response.data.user.id,
                        isLoggedIn: true,
                        firstName: response.data.user.first_name,
                        lastName: response.data.user.last_name,
                        ext: this.state.exts[this.state.activeFlag][1],
                        mobile: this.state.mobile.value, 
                        adCache: {
                            country: {id: null, label: null},
                            state: {id: null, label: null},
                            city: {id: null, label: null},
                            region: {id: null, label: null},
                            categories: null
                        }
                    });
                  
                    // success
                    this.setState({toast: {
                        type: 'success',
                        visible: true,
                        message: 'Success!! Your account has been registered.'
                    }});

                    // take to mobile verification
                    setInterval(() => {
                        Actions.verifyMobile();
                    }, 2000);

                } else {
                    store.update('storat.data', {
                        apiToken: null
                    });
                    
                    // failure
                    this.setState({toast: {
                        type: 'danger',
                        visible: true,
                        message: 'Sorry!! We couldn\'t create you account. Please try again.'
                    }});
                }
            })
            .catch(err => {
                console.log(err)
                
                // something went wrong
                this.setState({toast: {
                    type: 'danger',
                    visible: true,
                    message: 'Whoops!! We are facing some technical issues. Please try again.'
                }});
            });
    }
    hideForgotPasswordModal() {
        this.setState({showForgotPasswordModal: false});
    }
    setAccountType(type) {
        this.setState({accountType: type});
    }
    setAgreement() {
        this.setState({agreeToTerms: ! this.state.agreeToTerms});
    }
    render() {
        return (
            <View style={styles.container}>

                {/* Toast Danger */}
                <ToastTemplate 
                    type={this.state.toast.type}
                    show={this.state.toast.visible} 
                    message={this.state.toast.message} 
                />

                {/* Input Text: First Name */}
                <View style={[styles.inputContainer]}>
                    <TextInput
                        style={styles.input}
                        autocapitalize="none"
                        autoCorrect={false}
                        keyboardType="default"
                        returnKeyType="next"
                        placeholder="First name"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"
                        onChangeText={ text => this.validate('firstName', text) }
                    />
                    <ErrorLabel 
                        showError={this.state.firstName.error} 
                        message={'First name is required'} 
                    />
                </View>

                {/* Input Text: Last Name */}
                <View style={[styles.inputContainer]}>
                    <TextInput
                        style={styles.input}
                        autocapitalize="none"
                        autoCorrect={false}
                        keyboardType="default"
                        returnKeyType="next"
                        placeholder="Last name"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"
                        onChangeText={ text => this.validate('lastName', text) }
                    />
                    <ErrorLabel 
                        showError={this.state.lastName.error} 
                        message={'Last name is required'} 
                    />
                </View>

                {/* Input Text: Email */}
                <View style={[styles.inputContainer]}>
                    <TextInput
                        style={styles.input}
                        autocapitalize="none"
                        autoCorrect={false}
                        keyboardType="email-address"
                        returnKeyType="next"
                        placeholder="your-email@example.com"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"
                        onChangeText={ text => this.validate('email', text) }
                    />
                    <ErrorLabel 
                        showError={this.state.email.error} 
                        message={'Email is invalid'} 
                    />
                </View>

                {/* Input Text: Passsword */}                
                <View style={[styles.inputContainer]}>
                    <TextInput 
                        style={styles.input}
                        autocapitalize="none"
                        returnKeyType="go"
                        placeholder="Enter password here"
                        placeholderTextColor="#8095a2"
                        underlineColorAndroid="transparent"                    
                        secureTextEntry   
                        onChangeText={ text => this.validate('password', text) }
                                         
                    />
                    <ErrorLabel 
                        showError={this.state.password.error} 
                        message={'Password is required'} 
                    />
                </View>


                {/* Input Text: Mobile */}
                <View style={[styles.inputContainer]}>
                    <Text style={{marginBottom: 3}}>Select Country Extension</Text>
                    <TouchableOpacity style={{flexDirection: 'row', borderWidth: 1, borderColor: '#e9e9e9', padding: 10}} onPress={() => this.showCountriesPicker()}>               
                        <Image source={this.state.flags[this.state.activeFlag]} style={{height: 16, width: 36, marginRight: 8, marginTop: 2}} />
                        <View style={{flex: 1}}>
                            <Text>{this.state.exts[this.state.activeFlag][0]} (+{this.state.exts[this.state.activeFlag][1]})</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                {/* Input Text: Mobile */}
                <View style={[styles.inputContainer]}>
                    <Text style={{marginBottom: 3}}>Mobile Number</Text>
                    <View style={{flex: 1}}>
                        {/* <TouchableOpacity onPress={() => this.showCountriesPicker()}>
                            <Image source={this.state.flags[this.state.activeFlag]} style={{height: 36, width: 70, marginRight: 8, marginTop: 2}} />
                        </TouchableOpacity> */}

                        {/* <View style={{flex: 1}}> */}
                        <TextInput
                            style={[styles.input]}
                            autocapitalize="none"
                            autoCorrect={false}
                            keyboardType="phone-pad"
                            returnKeyType="next"
                            placeholder={this.state.activeMobileCodeHint}
                            value={this.state.mobile.value}
                            placeholderTextColor="#8095a2"
                            underlineColorAndroid="transparent"
                            onChangeText={ text => this.validate('mobile', text) }
                        />
                        {/* </View> */}
                    </View>
                    <ErrorLabel 
                        showError={this.state.mobile.error} 
                        message={'Mobile number is invalid'} 
                    />
                </View>

                {/* Account Type */}
                {/* <View style={[styles.inputContainer, {flexDirection: 'row'}]}>
                    <Text>Account Type</Text>                    
                    <TouchableOpacity style={{flexDirection: 'row'}} onPress={() => this.setAccountType('individual')}>
                        <CheckBox 
                            onPress={() => this.setAccountType('individual')}
                            checked={this.state.accountType === 'individual'} 
                            style={{marginRight: 10}} 
                        />
                        <Text> Individual</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flexDirection: 'row'}} onPress={() => this.setAccountType('business')}>
                        <CheckBox 
                            onPress={() => this.setAccountType('business')}                            
                            checked={this.state.accountType === 'business'} 
                            style={{marginRight: 10}} 
                        />
                        <Text> Business</Text>
                    </TouchableOpacity>
                </View> */}

                {/* Agreement */}
                <View style={[styles.inputContainer, {marginBottom: -10}]}>              
                    <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'flex-start'}} onPress={() => this.setAgreement()}>
                        <CheckBox 
                            onPress={() => this.setAgreement()}
                            checked={this.state.agreeToTerms === true} 
                            style={{marginRight: 10, marginLeft: -10, height: 17, width: 17, borderRadius: 0}} 
                        />
                        <Text> I agree to Storat's </Text>
                        <Text style={{color: '#1471b9'}} onPress={() => Linking.openURL('https://sell.storat.com/en/terms.html')}> 
                            Terms & Conditions
                        </Text>
                    </TouchableOpacity>
                </View>

                {/* Submit Button*/}      
                <TouchableOpacity onPress={this.submit} style={styles.buttonContainer}>
                    {this.props.showSpinner ?
                        <ActivityIndicator 
                            animating={true}
                            color={'#fff'}
                        />
                    :
                        <Text style={styles.buttonText}>
                            Register    
                        </Text>
                    }
                </TouchableOpacity>   

                {/* Picker */}
                <PickerTemplate 
                    show={this.state.showPicker}
                    items={this.state.countriesPickerItems}
                    closePicker={() => { this.hidePickerItems() }} 
                    buttonTitle='Back'
                    buttonSize='small'
                    buttonColor='blue' 
                />
            </View>
        );
    }
}

const styles = StyleSheet.create(LoginFormStyles);
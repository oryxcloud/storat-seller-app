import { connect } from 'react-redux';
import LoginRegisterComponent from '@modules/auth/components/login-register';
import * as UserActions from '@modules/auth/actions/register';

const mapStateToProps = state => ({
    isLoggedIn: state.auth.isLoggedIn,
    toastType: state.auth.toastType,
    showToast: state.auth.showToast,
    toastMessage: state.auth.toastMessage,
    showSpinner: state.auth.showSpinner,
    loginButtonText: state.auth.loginButtonText,
});    

const mapDispatchToProps = dispatch => ({
    
});

export default connect(mapStateToProps, UserActions)(LoginRegisterComponent);

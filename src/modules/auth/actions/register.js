export const registerFailed = () => {
    return {
        type: 'USER_REGISTER_FAILED'
    }
}

export const registerSuccess = () => {
    return {
        type: 'USER_REGISTER_SUCCESS'
    }
}

export const registerRequest = () => {
    return {
        type: 'USER_REGISTER_REQUEST'
    }
}

export const registerRedirect = () => {
    return {
        type: 'USER_REGISTER_REDIRECT'
    }
}

export const unexpectedError = () => {
    return {
        type: 'UNEXPECTED_ERROR'
    }
}
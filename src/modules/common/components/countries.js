import React, { Component } from 'react';
import { View, StyleSheet, WebView, TouchableOpacity, Modal, Image, ActivityIndicator } from 'react-native';
import { Container, Content, List, ListItem, Header, Thumbnail, Left, Body, Right, Button, Icon, Title, Segment, Text } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import footerImage from '@images/dubai.png';
import flagQatar from '@images/flags/qatar.png';
import flagBahrain from '@images/flags/bahrain.png';
import flagKuwait from '@images/flags/kuwait.png';
import flagSaudi from '@images/flags/saudi.png';
import flagArab from '@images/flags/arab.png';


export default class CountryPickerComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            
        };
    }
    setCountry(country, code) {
        this.props.closePicker(country, code);
    }
    getCountriesList() {
        return (
            <List>
                {/* UAE flag */}
                <ListItem key={'arab'} icon onPress={() => this.setCountry('arab', '050 123 4567')}>
                    <Left>
                        <Image source={flagArab} style={[styles.flag]}/>
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>United Arab Emirates (+971)</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                {/* Bahrain flag */}
                <ListItem key={'bahrain'} icon onPress={() => this.setCountry('bahrain', '3600 1234')}>
                    <Left>
                        <Image source={flagBahrain} style={[styles.flag]}/>
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Bahrain (+973)</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                {/* Kuwait flag */}
                <ListItem key={'kuwait'} icon onPress={() => this.setCountry('kuwait', '500 12345')}>
                    <Left>
                        <Image source={flagKuwait} style={[styles.flag]}/>
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Kuwait (+965)</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                {/* Saudi flag */}
                <ListItem key={'saudi'} icon onPress={() => this.setCountry('saudi', '051 234 5678')}>
                    <Left>
                        <Image source={flagSaudi} style={[styles.flag]}/>
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Saudi Arabia (+966)</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>

                {/* Qatar flag */}
                <ListItem key={'qatar'} icon onPress={() => this.setCountry('qatar', '3312 3456')}>
                    <Left>
                        <Image source={flagQatar} style={[styles.flag]}/>
                    </Left>
                    <Body>
                        <Text style={[styles.flagText]}>Qatar (+974)</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>
            </List>
        );
    }
    render() {
        return (
            <View style={[styles.container]}>
                {/* Statusbar */}
                <StatusBarTemplate />

                {/* Language buttons */}
                <Content>
                    {this.getCountriesList()}
                </Content>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 120,
    },
    languageButton: {
        width: 110,
        justifyContent: 'center'
    },
    spacer: {
        width: 50,
        alignItems: 'center',
        paddingTop: 5,
    },
    spacerText: {
        color: '#ccc',
    },
    flag: {
        width: 40,
        height: 20,
    },
    flagText: {
        fontSize: 15,
    },
    footerImage: {
        width: null,
        height: 160
    },
});
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Container, Content, List, ListItem, Header, Thumbnail, Left, Body, Right, Button, Icon, Title, Segment, Footer, FooterTab } from 'native-base';
import { Actions } from "react-native-router-flux";
import IPhoneX from "@theme/iphoneX";

if(IPhoneX()) {
    height = 85;
    paddingBottom = 20;
} else {
    height = 50; // 50
    paddingBottom = 0
}

export default class FooterTabComponent extends Component {

    render() {

        return (
            <Footer style={{backgroundColor: '#1470b9', height: height, paddingBottom: paddingBottom}}>
                <FooterTab>
                    <Button vertical onPress={() => Actions.browseAdsCategories()}>
                        <Icon name="list" style={{color: '#fff'}} />
                        <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 12}}>Categories</Text>
                    </Button>
                    <Button vertical onPress={() => Actions.browseAdsFavorites()}>
                        <Icon name="heart" style={{color: '#fff'}} />
                        <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 12}}>Favorite Ads</Text>
                    </Button>
                    <Button vertical onPress={() => Actions.welcome()}>
                        <Icon name="settings" style={{color: '#fff'}} />
                        <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 12}}>Settings</Text>
                    </Button>
                </FooterTab>
            </Footer>
        );

    }

}
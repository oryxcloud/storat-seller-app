import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Item, Input, Icon, Button, ListItem, Body, Right } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
import { PickerTemplate } from '@templates/ui';


export default class SearchBarComponent extends Component {

    constructor(props) {

        super(props);

        this.state = {
            locations: {},
            countries: [],
            states: [],
            showPicker: false,
            pickerItems: [],
            country: '',
            countryLabel: '',
            state: '',
            stateLabel: '',
            searchText: ''
        };

        store.get('storat.cache')
            .then(res => {
                this.setState({
                    countries: res.locations.countries,
                    states: res.locations.states,
                    country: res.country || res.locations.countries[0].id,
                    countryLabel: res.countryLabel || res.locations.countries[0].nameEn,
                    state: res.state || null,
                    stateLabel: res.stateLabel || 'All States'
                });
                
                return res;
            })
            .then((res) => {
                store.update('storat.cache', {
                    country: res.country || this.state.countries[0].id,
                    countryLabel: res.countryLabel || this.state.countries[0].nameEn,
                    state: res.state || null,
                    stateLabel: res.stateLabel || 'All States'
                })
            })
            .then(() => {
                store.get('storat.cache')
                    .then(res => {
                        console.log(res);
                    });
            });

    }

    populateCountries() {
        this.state.pickerItems = [];

        this.state.countries.forEach(e => {
            this.state.pickerItems.push(
                <ListItem key={'country-' + e.id} icon onPress={() => this.setSelectedCountry(e.id, e.nameEn)}>
                    <Body>
                        <Text style={[styles.flagText]}>{e.nameEn}</Text>
                    </Body>
                    <Right>
                        <Icon name="arrow-forward" />
                    </Right>
                </ListItem>
            );
        })

        this.setState({ showPicker: true });
    }

    populateStates() {
        this.state.pickerItems = [
            <ListItem key={'state-' + '-1'} icon onPress={() => this.setSelectedState(null, 'All States')}>
                <Body>
                    <Text style={[styles.flagText]}>All States</Text>
                </Body>
                <Right>
                    <Icon name="arrow-forward" />
                </Right>
            </ListItem>
        ];

        this.state.states.forEach(e => {
            if(e.countryId === this.state.country) {
                this.state.pickerItems.push(
                    <ListItem key={'state-' + e.id} icon onPress={() => this.setSelectedState(e.id, e.nameEn)}>
                        <Body>
                            <Text style={[styles.flagText]}>{e.nameEn}</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>
                );
            }
        })

        this.setState({ showPicker: true });  
    }

    setSelectedCountry(country, countryLabel) {
        store.update('storat.cache', {
            country: country,
            countryLabel: countryLabel,
            state: null,
            stateLabel: 'All States'
        });

        this.setState({
            country: country,
            countryLabel: countryLabel,
            state: null,
            stateLabel: 'All States'
        });

        this.hidePickerItems();
    }

    setSelectedState(state, stateLabel) {
        store.update('storat.cache', {
            state: state,
            stateLabel: stateLabel
        });

        this.setState({
            state: state,
            stateLabel: stateLabel
        });

        this.hidePickerItems();
    }

    setSearchText(text) {
        this.state.searchText = text.trim();
    }

    searchAds() {
        if(this.state.searchText) {

            let filters = {
                q: this.state.searchText,
                country: this.state.countryLabel,
                state: this.state.stateLabel 
            };

            store.update('storat.cache', {filterOptions: filters})
                .then(() => {
                    Actions.adListings();
                })
        }
    }

    hidePickerItems() {
        this.setState({
            pickerItems: [],
            showPicker: false
        });
    }

    render() {

        return (
            <View style={[styles.container]}>
                {/* Search Box */}
                <View style={[styles.searchContainer]}>
                    <Item regular style={[styles.searchBox]}>
                        <Input 
                            placeholder='I am looking for..' 
                            onChangeText={ text => this.setSearchText(text) }
                        />
                    </Item>
                    <Button dark style={[styles.searchButton]} onPress={ () => {this.searchAds()} }>
                        <Icon name="search" />
                    </Button>
                </View>
                {/* Locations */}
                <View style={[styles.locationContainer]}>
                    <Button iconRight light small style={[styles.locationButton]} onPress={ () => this.populateCountries() }>
                        <Text style={[styles.bold]}>{this.state.countryLabel}</Text>
                        <Icon name='arrow-down' style={[styles.locationIcon]} />
                    </Button>
                
                    <Button iconRight light small style={[styles.locationButton]} onPress={ () => this.populateStates() }>
                        <Text style={[styles.bold]}>{this.state.stateLabel}</Text>
                        <Icon name='arrow-down' style={[styles.locationIcon]} />
                    </Button>
                </View>
                {/* Picker */}
                <PickerTemplate 
                    show={this.state.showPicker}
                    items={this.state.pickerItems}
                    closePicker={() => { this.hidePickerItems() }} 
                    buttonTitle='Back'
                    buttonSize='small'
                    buttonColor='blue' 
                />
            </View>
        );

    }

};

const styles = StyleSheet.create({

    container: {
        padding: 15,
        paddingTop: 0,
        backgroundColor: '#fff'        
    },
    searchContainer: {
        flexDirection: 'row',
        paddingTop: 10
    },
    searchBox: {
        flex: 0.9,
        height: 30,
        borderColor: '#666',
        borderRadius: 5,
    },
    searchButton: {
        flex: 0.1,
        height: 30,
        marginLeft: 5,
        justifyContent: 'center'
    },
    locationContainer: {
        paddingTop: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    locationButton: {
        marginLeft: 10,
    },
    locationIcon: {
        marginLeft: 0,
        fontSize: 13
    },
    bold: {
        fontWeight: '700',
        color: '#103D60'
    }

});
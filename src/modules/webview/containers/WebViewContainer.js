import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import WebViewComponent from '@modules/webview/components/WebViewComponent'

export default class WebViewContainer extends Component {
    render() {
        return (
            <WebViewComponent />
        );
    }
}

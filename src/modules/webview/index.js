import WebViewContainer from './containers/WebViewContainer';

export default class WebViewModule {
    static launchWebView() {
        return WebViewContainer;
    }
}
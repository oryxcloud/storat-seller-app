import React, { Component } from 'react';
import { View, StyleSheet, WebView, TouchableOpacity, Text, Modal, ActivityIndicator } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { Actions } from "react-native-router-flux";
import StatusBarTemplate from '@templates/statusbar';

const WEBVIEW_REF = 'WEBVIEW_REF';
const SITE_URL = 'https://www.storat.com';

// react-native-webbrowser

export default class SplashComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            // canGoBack: true,
            // canGoForward: true, 
            currentUrl: SITE_URL,
            loading: false,
        };
    }
    onNavigationStateChange(navState) {
        // this.state.canGoBack = navState.canGoBack;
        // this.state.canGoForward = navState.canGoForward;
        
        if(navState.url !== this.state.currentUrl) {
            this.setState({
                loading: true,
                currentUrl: navState.url
            });
        } else {
            this.setState({
                loading: false,
            });
        }
    }
    onBack() {
        this.refs[WEBVIEW_REF].goBack();
    }
    onForward() {
        this.refs[WEBVIEW_REF].goForward();
    }
    onReload() {
        this.refs[WEBVIEW_REF].reload();
    }
    // onLoadStart() {
    //     this.setState({ webviewLoaded: false });
    // }
    // onLoadEnd() {
    //     this.setState({ webviewLoaded: true });
    // }
    jsCodeToInject() {
        let js = "";
        
        js += "document.querySelector('.prefooter').style.display='none';";
        js += "document.querySelector('#footer').style.display='none';";
        
        return js;
    }
    render() {
        return (
            <View style={[styles.container]}>
                
                {/* Header */}
                <Header iosBarStyle='light-content' style={[styles.header]} >
                   
                    <Left>
                        <Button transparent onPress={this.onBack.bind(this)}>
                            <Icon name='arrow-round-back' style={[styles.icon, styles.iconBack]} />
                        </Button>
                    </Left>
                    

                    <Body>
                        <Button transparent onPress={this.onReload.bind(this)}>
                            {/* <Icon name='refresh' style={[styles.icon, styles.iconReload]} /> */}
                            <Text style={[styles.whiteText]}>storat.com</Text>
                        </Button>
                    </Body>

                    
                    <Right>
                        {/* <Button transparent onPress={this.onForward.bind(this)}>
                            <Icon name='arrow-round-forward' style={[styles.icon, styles.iconForward]} />
                        </Button> */}
                        {this.state.loading ?
                            <ActivityIndicator color='#fff' />
                        : null}
                    </Right>
                    
                </Header>

                {/* WebView */}
                <WebView
                    ref={WEBVIEW_REF}
                    source={{uri: SITE_URL}}
                    javaScriptEnabled={true}
                    //onLoadEnd={this.onLoadEnd.bind(this)}
                    onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                    injectedJavaScript={this.jsCodeToInject()}
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },
    header: {
        backgroundColor:'#1470b9',
    },
    icon: {
        color: '#fff',
        fontSize: 30
    },
    iconBack: {
     
    },
    iconForward: {

    },
    iconReload: {
        fontSize: 25,
    },
    back: {
        color: '#fff',
    },
    whiteText: {
        color: '#fff'
    }
});
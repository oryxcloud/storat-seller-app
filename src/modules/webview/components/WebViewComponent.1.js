import React, { Component } from 'react';
import { View, StyleSheet, WebView, TouchableOpacity, Text } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { Actions } from "react-native-router-flux";
import StatusBarTemplate from '@templates/statusbar';

const WEBVIEW_REF = 'WEBVIEW_REF';
const SITE_URL = 'https://www.storat.com';

export default class SplashComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            canGoBack: false,
            canGoForward: false, 
            canReload: true
        };
    }
    onNavigationStateChange(navState) {
        this.setState({
            canGoBack: navState.canGoBack,
            canGoForward: navState.canGoForward,
            canReload: navState.canReload,
        });
    }
    onBack() {
        this.refs[WEBVIEW_REF].goBack();
    }
    onForward() {
        this.refs[WEBVIEW_REF].goForward();
    }
    onReload() {
        this.refs[WEBVIEW_REF].reload();
    }
    injectJavaScript() {
        let js = "";
        
        js += "document.querySelector('.prefooter').style.display='none';";
        js += "document.querySelector('#footer').style.display='none';";

        return js;
    }
    render() {
        return (
            <View style={[styles.container]}>
                
                {/* Header */}
                <Header style={[styles.header]} >
                    {this.state.canGoBack ? 
                    <Left>
                        <Button transparent onPress={this.onBack.bind(this)}>
                            <Icon name='arrow-round-back' style={[styles.icon]} />
                            <Text style={[styles.back]}>  Back</Text>
                        </Button>

                        <Button transparent onPress={this.onReload.bind(this)}>
                            <Icon name='refresh' style={[styles.icon]} />
                            <Text style={[styles.back]}>Refresh  </Text>
                        </Button>

                        <Button transparent onPress={this.onForward.bind(this)}>
                            <Text style={[styles.back]}>Forward  </Text>
                            <Icon name='arrow-round-forward' style={[styles.icon]} />
                        </Button>
                    </Left>
                    : null}

                    <Body>
                        <Button transparent onPress={this.onReload.bind(this)}>
                            <Icon name='refresh' style={[styles.icon]} />
                            <Text style={[styles.back]}>Refresh  </Text>
                        </Button>
                    </Body>

                    {this.state.canGoForward ? 
                    <Right>
                        <Button transparent onPress={this.onForward.bind(this)}>
                            <Text style={[styles.back]}>Forward  </Text>
                            <Icon name='arrow-round-forward' style={[styles.icon]} />
                        </Button>
                    </Right>
                    : null} 
                </Header>

                {/* WebView */}
                <WebView
                    ref={WEBVIEW_REF}
                    source={{uri: SITE_URL}}
                    javaScriptEnabled={true}
                    onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },
    header: {
        backgroundColor:'#1470b9',
    },
    icon: {
        color: '#fff',
    },
    back: {
        color: '#fff',
    }
});
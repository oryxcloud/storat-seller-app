import React, { Component } from 'react';
import { View, StyleSheet, WebView, TouchableOpacity, Text, Modal } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { Actions } from "react-native-router-flux";
import StatusBarTemplate from '@templates/statusbar';
import Webbrowser from 'react-native-webbrowser';

const WEBVIEW_REF = 'WEBVIEW_REF';
const SITE_URL = 'https://www.storat.com';

export default class SampleApp extends Component {
    render() {
        return (
            <View style={{paddingTop:20, flex:1}}>
            
                <Webbrowser
                    url={SITE_URL}
                    hideHomeButton={false}
                    hideToolbar={false}
                    hideAddressBar={false}
                    hideStatusBar={false}
                    foregroundColor={'#efefef'}
                    backgroundColor={'#333'}
                />
                
            </View>
        );
    }
}
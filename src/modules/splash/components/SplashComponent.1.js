import React, { Component } from 'react';
import { View, Text, Image, StatusBar, StyleSheet, ActivityIndicator } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
// import SlideShow from './SlideShow';
import ToastTemplate from '@templates/toast';
import StatusBarTemplate from '@templates/statusbar';
import headerImage from '@images/splash-header.png';
import footerImage from '@images/splash-footer.png';
import * as Animatable from 'react-native-animatable';

export default class SplashComponent extends Component {

    constructor(props) {

        super(props);

        this.state = {
            errorMessage: 'Whoops! We are facing some technical issues. May be you are disconnected. Please try later after some time.',
            showError: false
        };

        this.fetchDataAndSendToHomePage();
    }

    fetchDataAndSendToHomePage() {
        let url = 'https://uae.storat.com/api/v2/categories';

        fetch(url)
            .then(res => res.json())
            .then(res => {
                store.update('storat.cache', {
                    'categories': res
                });
            })
            .then(res => {
                fetch('https://uae.storat.com/api/v2/locations')
                    .then(res => res.json())
                    .then(res => {
                        store.update('storat.cache', {
                            'locations': res,
                            'country': 1 // assume UAE
                        });
                    })
                    .then(() => {
                        fetch('https://uae.storat.com/api/v2/ads/attributes')
                            .then(res => res.json())
                            .then(res => {
                                store.update('storat.cache', {
                                    'attributes': res
                                });
                            })
                            .then(() => {
                                Actions.browseAdsCategories();
                            })
                    })
            })
            .catch(() => {
                this.setState({showError: true});
            });
            
    }

    skipIntro() {
    
    }
    render() {
        return (
            <View style={[styles.container]}>
                
                {/* StatusBar */}
                <StatusBarTemplate hidden={true} />

                {/* Top */}
                <View style={[styles.header]}>
                    <Image 
                        style={[styles.headerImage]}
                        source={headerImage}
                    />
                </View>

                {/* Mid */}
                <View style={[styles.mid]}>
                    <Text style={[styles.brand]}>
                        STORAT
                    </Text>
                    <Text style={[styles.teaser]}>
                        Social, Easy, Trusted Marketplace
                    </Text>
                    {/* <Text style={[styles.website]}>
                        www.storat.com
                    </Text> */}
                </View>

                {/* Indicator */}
                {! this.state.showError ? 
                <View>
                    <ActivityIndicator />
                </View>
                :
                    <ToastTemplate type="danger" show={true} message={this.state.errorMessage} />
                }
                
                {/* Marketing Label */}
                <View style={[styles.slideShow]}>
                    <Text style={[styles.slideHeader]}>Find trusted local sellers and services providers</Text>
                </View>

                {/* SlideShow */}
                {/* <SlideShow skip={this.skipIntro} /> */}

                
                {/* Bottom */}
                <View style={[styles.footer]}>
                    <Image 
                        style={[styles.footerImage]}
                        source={footerImage}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },
    mid: {
        alignItems: 'center'
    },
    headerImage: {
        alignSelf: 'flex-end',
        width: 370,
        height: 75,
        position: 'absolute',
        top: -20
    },
    footerImage: {
        width: null,
        height: 100
    },
    brand: {
        fontWeight: 'bold',
        fontSize: 27,
        color: '#1470b9',
        letterSpacing: 1.5
    },
    teaser: {
        fontWeight: 'bold',
        fontSize: 11,
        letterSpacing: 2,
        marginBottom: 10,
    },
    website: {
        fontSize: 10,
        color: '#646464',
        fontStyle: 'italic',
        marginBottom: 25
    },
    skipIntro: {
        alignSelf: 'center',
        color: '#1470b9',
        fontWeight: 'bold',
        fontSize: 13,
        textDecorationLine: 'underline',
        textDecorationStyle: "solid",
        textDecorationColor: "#09f"
    },
    slideShow: {
        margin: 15,
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#d3d3d3',
        alignItems: 'center',
        backgroundColor: '#f9f9f9',
    },
    slideHeader: {
        backgroundColor: '#f9f9f9',
        alignSelf: 'stretch',
        textAlign: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 12,
        fontWeight: 'bold',
    },
});
import React, { Component } from 'react';
import { View, Text, Image, StatusBar, StyleSheet, ActivityIndicator } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
import SlideShow from './SlideShow';
import StatusBarTemplate from '@templates/statusbar';
import headerImage from '@images/splash-header.png';
import footerImage from '@images/splash-footer.png';
import * as Animatable from 'react-native-animatable';

export default class SlideShowComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            slides: {
                0: 'on',
                1: 'off',
                2: 'off',
                3: 'off',
                4: 'off',
                5: 'off',
                6: 'off',
            }
        };
    }
    componentDidMount() {
        let counter = 1;

        let interval = setInterval(() => {
            if(counter > 6) {
                counter = 0;
                clearInterval(interval);
                this.props.skip();
            }

            this.changeSlide({[counter++]: 'on'});

        }, 900)    
    }
    componentWillUnmount() {
        this.state = null;
    }
    changeSlide(slideData) {
        this.setState({
            slides: Object.assign({
                0: 'off',
                1: 'off',
                2: 'off',
                3: 'off',
                4: 'off',
                5: 'off',
                6: 'off',
            }, slideData)
        })
    }
    render() {
        return (
            <View style={[styles.slideShow]}>
                <Text style={[styles.slideHeader]}>Post Your Ad In 5 Simple Steps</Text>
                
                {this.state.slides[0] === 'on' ?
                <Animatable.View animation="bounceIn">
                    <View style={[styles.slideBodyContainer]}>
                        <Icon name='clipboard' style={[styles.slideBodyIcon]} />
                        <Text style={[styles.slideBodyText]}>In 5 easy steps..</Text>
                    </View>
                </Animatable.View>
                : null }

                {this.state.slides[1] === 'on' ?
                <Animatable.View animation="bounceInRight">
                    <View style={[styles.slideBodyContainer]}>
                        <Icon name='navigate' style={[styles.slideBodyIcon]} />
                        <Text style={[styles.slideBodyText]}>Step 1: Select Your Location</Text>
                    </View>
                </Animatable.View>
                : null }
                
                {this.state.slides[2] === 'on' ?
                <Animatable.View animation="bounceInRight">
                    <View style={[styles.slideBodyContainer]}>
                        <Icon name='list-box' style={[styles.slideBodyIcon]} />
                        <Text style={[styles.slideBodyText]}>Step 2: Select Ad Category</Text>
                    </View>
                </Animatable.View>
                : null }

                {this.state.slides[3] === 'on' ?
                <Animatable.View animation="bounceInRight">
                    <View style={[styles.slideBodyContainer]}>
                        <Icon name='images' style={[styles.slideBodyIcon]} />
                        <Text style={[styles.slideBodyText]}>Step 3: Upload Ad Photos</Text>
                    </View>
                </Animatable.View>
                : null }

                {this.state.slides[4] === 'on' ?
                <Animatable.View animation="bounceInRight">
                    <View style={[styles.slideBodyContainer]}>
                        <Icon name='paper' style={[styles.slideBodyIcon]} />
                        <Text style={[styles.slideBodyText]}>Step 4: Fill Up Ad Details</Text>
                    </View>
                </Animatable.View>
                : null }

                {this.state.slides[5] === 'on' ?
                <Animatable.View animation="bounceInRight">
                    <View style={[styles.slideBodyContainer]}>
                        <Icon name='copy' style={[styles.slideBodyIcon]} />
                        <Text style={[styles.slideBodyText]}>Step 5: Add Extra Information</Text>
                    </View>
                </Animatable.View>
                : null }

                {this.state.slides[6] === 'on' ?
                <Animatable.View animation="bounceIn">
                    <View style={[styles.slideBodyContainer]}>
                        <Icon name='checkmark-circle' style={[styles.slideBodyIcon, styles.iconSuccess]} />
                        <Text style={[styles.slideBodyText]}>Cool! Your Ad Is Now Posted.</Text>
                    </View>
                </Animatable.View>
                : null }

                <Text style={[styles.slideNavigationContainer]}>
                    <Icon 
                        name={'radio-button-' + (this.state.slides[1] === 'on' ? 'on' : 'off')} 
                        style={[styles.slideNavigationIcon]} 
                    />
                    <Icon 
                        name={'radio-button-' + (this.state.slides[2] === 'on' ? 'on' : 'off')} 
                        style={[styles.slideNavigationIcon]} 
                    />
                    <Icon 
                        name={'radio-button-' + (this.state.slides[3] === 'on' ? 'on' : 'off')} 
                        style={[styles.slideNavigationIcon]} 
                    />
                    <Icon 
                        name={'radio-button-' + (this.state.slides[4] === 'on' ? 'on' : 'off')} 
                        style={[styles.slideNavigationIcon]} 
                    />
                    <Icon 
                        name={'radio-button-' + (this.state.slides[5] === 'on' ? 'on' : 'off')} 
                        style={[styles.slideNavigationIcon]} 
                    />
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    slideShow: {
        margin: 15,
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#d3d3d3',
        alignItems: 'center',
        backgroundColor: '#f9f9f9',
    },
    slideHeader: {
        backgroundColor: '#f9f9f9',
        alignSelf: 'stretch',
        textAlign: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 12,
        fontWeight: 'bold',
    },
    slideBodyContainer: {
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 30,
    },
    slideBodyIcon: {
        color: '#000',
    },
    iconSuccess: {
        color: 'green'
    },
    slideBodyText: {
        fontSize: 12,
    },
    slideNavigationContainer: {
        marginBottom: 10,
    },
    slideNavigationIcon: {
        fontSize: 12,
        color: '#646464',
    }
});
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import SplashComponent from '@modules/splash/components/SplashComponent'
import store from 'react-native-simple-store';

export default class SplashContainer extends Component {
    render() {
        return (
            <SplashComponent />
        );
    }
}

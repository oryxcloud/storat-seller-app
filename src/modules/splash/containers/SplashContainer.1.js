import React, { Component } from 'react';
import { Text } from 'react-native';
import { Container, Content, List, ListItem } from 'native-base';
import { Actions } from "react-native-router-flux";
import StatusBarTemplate from '@templates/statusbar';

export default class SplashContainer extends Component {
    render() {
        return (
            <Container style={{backgroundColor: '#fff'}}>
                <StatusBarTemplate />
                <Content>
                    <List>
                        <ListItem onPress={() => Actions.login()}>
                            <Text>Login</Text>
                        </ListItem>
                        <ListItem onPress={() => Actions.store()}>
                            <Text>Welcome</Text>
                        </ListItem>
                        <ListItem onPress={() => Actions.selectAdCategory()}>
                            <Text>Category</Text>
                        </ListItem>
                        <ListItem onPress={() => Actions.selectAdLocation()}>
                            <Text>Location</Text>
                        </ListItem>
                        <ListItem onPress={() => Actions.uploadPhoto()}>
                            <Text>Photo</Text>
                        </ListItem>
                        <ListItem onPress={() => Actions.adDetails()}>
                            <Text>Post</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}

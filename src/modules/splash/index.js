import SplashContainer from './containers/SplashContainer';

export default class SplashModule {
    static launch() {
        return SplashContainer;
    }
}
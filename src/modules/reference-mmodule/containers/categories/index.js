import { connect } from 'react-redux';
import CategoriesComponent from '@modules/browse-ads/components/categories';
import * as CategoriesActions from '@modules/browse-ads/actions/categories';

const mapStateToProps = state => ({
    
});    

const mapDispatchToProps = dispatch => ({
    
});

export default connect(mapStateToProps, CategoriesActions)(CategoriesComponent);

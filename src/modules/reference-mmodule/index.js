import CategoriesContainer from './containers/categories';

export default class BrowseAdsModule {
    static categories() {
        return CategoriesContainer;
    }
}
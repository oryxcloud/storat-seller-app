import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, StyleSheet } from 'react-native';
import { ModalTemplate } from '@templates/ui';
import { ButtonTemplate, InputTemplate } from '@templates/form';

export default class SocialLogin extends Component {
    constructor(props) {
        super(props);
    }
    getBody() {
        return (
            <View style={[styles.container]}>

                {/* Header */}
                <Text style={[styles.header]}>
                    Reset your password
                </Text>

                {/* Message */}
                <Text style={[styles.message]}>
                    Please enter the email that you use to access storat.com below.
                    If we find it in our database, we will send you a link to reset 
                    your password.
                </Text>

                {/* Email Input */}
                <InputTemplate
                    placeholder='Enter your email'
                    onChangeText={text => console.log(text)}
                />

                {/* Buttons */}
                <View style={[styles.buttonsContainer]}>
                    {/* Cancel button */}
                    <ButtonTemplate 
                        title='Cancel'
                        color='blue'
                        size='medium'
                        onPress={this.props.hideModal}
                    />
                    {/* Spacer */}
                    <View style={[styles.horizontalSpacer]}></View>
                    {/* Send button */}
                    <ButtonTemplate 
                        title='Send Reset Link' 
                        color='red'
                        size='medium'
                        onPress={this.props.hideModal}
                    />
                </View>
            </View>
        );
    }
    render() {
        return (
            <ModalTemplate 
                visible={this.props.visible}
                body={this.getBody()}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        
    },
    header: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#646464',
        marginBottom: 30,
    },
    message: {
        fontSize: 12,
        textAlign: 'justify',
        marginBottom: 30
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
    },
    horizontalSpacer: {
        width: 15
    },
});
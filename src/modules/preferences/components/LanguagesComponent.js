import React, { Component } from 'react';
import { View, Text, StyleSheet, WebView, TouchableOpacity, Modal, Image, ActivityIndicator } from 'react-native';
import { Container, Content, List, ListItem, Header, Left, Body, Right, Button, Icon, Title, Segment } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import footerImage from '@images/dubai2.png';


export default class SplashComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            
        };
    }
    static navigationOptions = {
        headerLeft:null
    }
    setPreference(language) {
        store.update('storat.cache', {language: language})
            .then(()=> Actions.welcome());
    }
    render() {
        return (
            <View style={[styles.container]}>
                {/* Statusbar */}
                <StatusBarTemplate />

                <List>

                    {/* English */}
                    <ListItem icon onPress={() => this.setPreference('')}>
                        <Body>
                            <Text style={[styles.flagText]}>English</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>

                    {/* Arabic */}
                    <ListItem icon onPress={() => this.setPreference('ar')}>  
                        <Body>
                            <Text style={[styles.flagText]}>العربية</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>

                </List>
                
                {/* Footer */}
                <View style={[styles.footer]}>
                    <Image 
                        style={[styles.footerImage]}
                        source={footerImage}
                    />
                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 30,
    },
    languageButton: {
        width: 110,
        justifyContent: 'center'
    },
    spacer: {
        width: 50,
        alignItems: 'center',
        paddingTop: 5,
    },
    spacerText: {
        color: '#646464',
    },
    footerImage: {
        width: null,
        height: 160
    },
});
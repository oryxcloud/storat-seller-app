import React, { Component } from 'react';
import { View, StyleSheet, WebView, TouchableOpacity, Modal, Image, ActivityIndicator } from 'react-native';
import { Container, Content, List, ListItem, Header, Thumbnail, Left, Body, Right, Button, Icon, Title, Segment, Text } from 'native-base';
import { Actions } from "react-native-router-flux";
import store from 'react-native-simple-store';
import StatusBarTemplate from '@templates/statusbar';
import footerImage from '@images/dubai.png';
import flagQatar from '@images/flags/qatar.png';
import flagBahrain from '@images/flags/bahrain.png';
import flagKuwait from '@images/flags/kuwait.png';
import flagSaudi from '@images/flags/saudi.png';
import flagArab from '@images/flags/arab.png';


export default class SplashComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            
        };
    }
    setPreference(countryId) {
        store.update('storat.cache', {country: countryId})
            .then(()=> Actions.welcome());
    }
    render() {
        return (
            <View style={[styles.container]}>
                {/* Statusbar */}
                <StatusBarTemplate />

                {/* Language buttons */}
                <Content>
                    <List>

                        {/* UAE flag */}
                        <ListItem icon onPress={() => this.setPreference(1)}>
                            <Left>
                                <Image source={flagArab} style={[styles.flag]}/>
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>United Arab Emirates</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                        {/* Bahrain flag */}
                        <ListItem icon onPress={() => this.setPreference(3)}>
                            <Left>
                                <Image source={flagBahrain} style={[styles.flag]}/>
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Bahrain</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                        {/* Kuwait flag */}
                        <ListItem icon onPress={() => this.setPreference(4)}>
                            <Left>
                                <Image source={flagKuwait} style={[styles.flag]}/>
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Kuwait</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                        {/* Saudi flag */}
                        <ListItem icon onPress={() => this.setPreference(5)}>
                            <Left>
                                <Image source={flagSaudi} style={[styles.flag]}/>
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Saudi Arabia</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                        {/* Qatar flag */}
                        <ListItem icon onPress={() => this.setPreference(2)}>
                            <Left>
                                <Image source={flagQatar} style={[styles.flag]}/>
                            </Left>
                            <Body>
                                <Text style={[styles.flagText]}>Qatar</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>

                    </List>
                </Content>
                
                {/* Footer */}
                <View style={[styles.footer]}>
                    <Image 
                        style={[styles.footerImage]}
                        source={footerImage}
                    />
                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 120,
    },
    languageButton: {
        width: 110,
        justifyContent: 'center'
    },
    spacer: {
        width: 50,
        alignItems: 'center',
        paddingTop: 5,
    },
    spacerText: {
        color: '#ccc',
    },
    flag: {
        width: 40,
        height: 20,
    },
    flagText: {
        fontSize: 15,
    },
    footerImage: {
        width: null,
        height: 160
    },
});
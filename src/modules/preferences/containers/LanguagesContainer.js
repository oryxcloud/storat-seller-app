import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import LanguagesComponent from '@modules/preferences/components/LanguagesComponent'

export default class LangaugesContainer extends Component {
    render() {
        return (
            <LanguagesComponent />
        );
    }
}

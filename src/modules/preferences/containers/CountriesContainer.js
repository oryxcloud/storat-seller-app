import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import CountriesComponent from '@modules/preferences/components/CountriesComponent'

export default class CountriesContainer extends Component {
    render() {
        return (
            <CountriesComponent />
        );
    }
}

import LanguagesContainer from './containers/LanguagesContainer';
import CountriesContainer from './containers/CountriesContainer';

export default class PreferencesModule {
    static selectLanguage() {
        return LanguagesContainer;
    }

    static selectCountry() {
        return CountriesContainer;
    }
}
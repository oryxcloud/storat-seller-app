import React from 'react';
import { connect, Provider } from 'react-redux';
import { Router } from 'react-native-router-flux';
import Routes from '@routes/';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '@redux';

/**
 * Set application mode to development
 */
global.___DEV___ = true;

/**
 * Hide yellow warning messages
 */
console.disableYellowBox = true;

/**
 * Tie the router with redux.
 */
const ReduxRouter = connect()(Router);

/**
 * Set up the store.
 */
const store = compose(
    applyMiddleware(thunk)
)(createStore)(rootReducer);

/**
 * Make redux available to all components using redux proivider.
 */
const AppRoot = () => (
    <Provider store={store}>
        <ReduxRouter scenes={Routes} />
    </Provider>
);

export default AppRoot;
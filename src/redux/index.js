import { combineReducers } from 'redux';
import auth from '@modules/auth/reducers';
import store from '@modules/store/reducers';

const rootReducer = (state, action) => (
    combineReducers({
        auth,
        store,
    })(state, action)
);

export default rootReducer;

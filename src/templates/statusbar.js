import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { AppStyles } from '@theme';

export default class StatusBarTemplate extends Component {
    render() {
        return (
            <StatusBar
                hidden={this.props.hidden || false} 
                backgroundColor={AppStyles.navbar.backgroundColor} 
                barStyle="light-content" 
            />
        );
    }
};

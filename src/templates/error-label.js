import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

export default class ErrorLabel extends Component {
    render() {
        if(this.props.showError) {
            return (
                <Text style={styles.errorLabel}>
                    {this.props.message}
                </Text>
            );
        }  

        return null;
    }
}

const styles = StyleSheet.create({
    errorLabel: {
        color: '#cd5d5c',
        fontWeight: 'bold',
        fontSize: 12
    }
})
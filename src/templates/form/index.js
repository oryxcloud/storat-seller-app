import LabelTemplate from './label';
import InputTemplate from './input';
import ButtonTemplate from './button';
import SelectionBoxTemplate from './selection';

export { 
    LabelTemplate,
    InputTemplate,
    ButtonTemplate,
    SelectionBoxTemplate,
};
import React, { Component } from 'react';
import { TextInput, StyleSheet } from 'react-native';
import { Icon } from 'native-base';

export default class InputTemplate extends Component {
    render() {
        return (
            <TextInput
                style={[styles.input]}
                maxLength={this.props.maxLength}
                autocapitalize={this.props.autocapitalize || 'none'}
                autoCorrect={this.props.autoCorrect || false}
                returnKeyType={this.props.returnKeyType || 'next'}
                placeholder={this.props.placeholder || ''}
                placeholderTextColor="#94a6b1"
                underlineColorAndroid="transparent"
                onChangeText={ text => this.props.onChangeText(text) }
            />
        );
    }
}

const styles = StyleSheet.create({
    input: {
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#d3d3d3',
        marginBottom: 5,
        fontSize: 13,
        color: '#000',
        padding: 10,
        paddingLeft: 10
    },
});
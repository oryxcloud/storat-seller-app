import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class ButtonTemplate extends Component {
    getStyles() {
        let arr = [styles.buttonContainer];

        if(this.props.size) {
            arr.push(styles[this.props.size]);
        }

        if(this.props.color) {
            arr.push(styles[this.props.color]);            
        }

        return arr;
    }
    render() {
        return (
            <TouchableOpacity 
                style={this.getStyles()}
                onPress={ () => { this.props.onPress() }} 
            >
                <Text style={[styles.buttonText]}>
                    {this.props.title}
                </Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        backgroundColor: '#ccc',
        paddingVertical: 10,
        borderRadius: 3
    },
    small: {
        padding: 5,
        paddingVertical: 3,
    },
    medium: {
        padding: 10,
        paddingVertical: 6,
    },
    large: {
        padding: 15,
        paddingVertical: 10,
    },
    blue: {
        backgroundColor: '#1470b9'
    },
    red: {
        backgroundColor: '#e24248'
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
});
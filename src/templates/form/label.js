import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

export default class LabelTemplate extends Component {
    render() {
        return (
            <Text style={[styles.label]}>
                {this.props.title}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    label: {
        fontSize: 13,
        fontWeight: 'bold',
        marginBottom: 5,
    }
});
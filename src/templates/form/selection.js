import React, { Component } from 'react';
import { Icon } from 'native-base';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';

export default class SelectionBoxTemplate extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={[styles.input]}>
                <View style={[styles.selectionTextBox]}>
                    <Text>{this.props.text || 'Select one'}</Text>
                    <Icon name='arrow-dropdown-circle' style={[styles.icon]}/>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#d3d3d3',
        marginBottom: 5,
        padding: 10,
        paddingLeft: 10,
        alignSelf: 'stretch',
    },
    selectionTextBox: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    icon: {
        fontSize: 18,
        color: '#ccc',
    }
});
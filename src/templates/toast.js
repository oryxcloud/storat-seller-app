import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class ToastDangerTemplate extends Component {
    render() {
        let containerStyle = styles[this.props.type + 'Container'];
        let containerText = styles[this.props.type + 'Text'];

        if(this.props.show) {
            return (            
                <View style={[styles.container, containerStyle]}>
                    <Text style={[containerText]}>{this.props.message}</Text>
                </View>
            );
        }

        return null;
    }
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 10,
        paddingLeft: 10,
        borderWidth: 1,
    },
    dangerContainer: {
        backgroundColor: '#f2dede',
        borderColor: '#ebccd1',
    },
    successContainer: {
        backgroundColor: '#dff0d8',
        borderColor: '#d6e9c6',
    },
    dangerText: {
        color: '#a94442'
    },
    successText: {
        color: '#3c763d'
    }
});
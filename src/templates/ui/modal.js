import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, StyleSheet } from 'react-native';

export default class ModalTemplate extends Component {
    render() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.visible}
            >
                <View style={[styles.container]}>
                    <View style={[styles.content]}>
                        {this.props.body}
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: 'rgba(0,0,0,.6)', 
        justifyContent: 'center', 
        alignItems: 'center',
        padding: 20,
    },
    content: {
        flexDirection: 'row',
        alignSelf: 'stretch',
        backgroundColor: '#fff',      
        padding: 20,
    }
});
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Card, CardItem, Body, Content, Button } from 'native-base';

export default class CardTemplate extends Component {
    render() {
        return (
            <Content style={styles.card}>
                <View style={styles.cardHeader}>
                    <Text style={styles.selectedCategoriesHeader}>
                            {this.props.title}
                    </Text>
                </View>
                <View style={styles.cardBody}>
                    {this.props.body || null}
                </View>
            </Content>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#ccc',
        marginBottom: 10
    },
    cardHeader: {
        padding: 15,
        backgroundColor: '#f7f7f7',
    },
    selectedCategoriesHeader: {
        fontWeight: 'bold',
        fontSize: 13
    },
    cardBody: {
        padding: 15,
        alignItems: 'center',
    },
});
import React, { Component } from 'react';
import { Text, Modal, StyleSheet } from 'react-native';
import { Container, Content, Header, Left, List } from 'native-base';
import { ButtonTemplate } from '@templates/form';
import iphoneX from '@theme/iphoneX';

let paddingValue = iphoneX() ? 40 : 10;

export default class PickerTemplate extends Component {
    render() {
        return (
            <Modal
                animationType={"slide"}
                transparent={false}
                visible={this.props.show}   
            >
                <Container style={{paddingTop: paddingValue}}>
                    <Header>
                        <Left>
                            <ButtonTemplate 
                                size={this.props.buttonSize}
                                color={this.props.buttonColor}
                                title={this.props.buttonTitle}
                                onPress={this.props.closePicker}
                            />
                        </Left>    
                    </Header>
                    <Content>
                        <List>
                            {this.props.items}
                        </List>                           
                    </Content>
                </Container>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({

});
import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

export default class SpacerTemplate extends Component {
    render() {
        return (
            <View style={{height: this.props.height}}></View>
        );
    }
}
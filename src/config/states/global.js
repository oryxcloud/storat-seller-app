const {Record} = require('immutable');

const data = Record({
    currentUser: null
});

var InitialState = Record({
    data: new data()
});

export default InitialState;
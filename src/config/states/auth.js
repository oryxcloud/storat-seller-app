const {Record} = require('immutable');

const data = Record({
    emailHasError: true,
    username: 'Pradeep',
    disabled: false,
    error: null,
    isValid: false,
    isFetching: false,
});

var InitialState = Record({
    data: new data()
});

export default InitialState;


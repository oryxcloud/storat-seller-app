export default {
    development: {
        base: 'https://uae.storat.com/api/v2/'
    },
    staging: {
        base: 'http://uae.hamooz.com/api/v2'
    },
    production: {
        base: 'http://storat.com/api/v2/'
    }
}
import { AppColors, AppStyles, AppSizes } from '@theme';

export default {
    hideNavBar: false,
    titleStyle: AppStyles.navbarTitle,
    headerTintColor: AppStyles.navbarButton.tintColor,
    navigationBarStyle: AppStyles.navbar,
    leftButtonIconStyle: AppStyles.navbarButton,
    rightButtonIconStyle: AppStyles.navbarButton,
    backButtonTextStyle: {fontSize: 12}, // hack to hide back button text 
    sceneStyle: {
        backgroundColor: AppColors.background,
        paddingTop: AppSizes.navbarHeight,
    },
}
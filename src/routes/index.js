import React from 'react';
import { Actions, Scene } from 'react-native-router-flux';

/**
 * Import props configurations for nav bar.
 */
import navbarProps from '@config/props/navbar';

/**
 * Top level containers.
 */
import PreferencesModule from '@modules/preferences';
import BrowseAdsModule from '@modules/browse-ads';
import WelcomeModule from '@modules/welcome';
import SplashModule from '@modules/splash';
import StoreModule from '@modules/store';
import AuthModule from '@modules/auth';
import AdModule from '@modules/ad';

/**
 * Google Analytics
 */
import {
    GoogleAnalyticsTracker,
    GoogleTagManager,
    GoogleAnalyticsSettings
} from "react-native-google-analytics-bridge";

let tracker = new GoogleAnalyticsTracker("UA-70252435-5");

/**
 * Screens
 */
const Routes = Actions.create(
    <Scene key={'root'} {...navbarProps}>
        <Scene key={'launch'} component={SplashModule.launch()} hideNavBar={true} initial />
        <Scene key={'welcome'} component={WelcomeModule.options()} title={'Settings'} />
        <Scene key={'browseAdsCategories'} component={BrowseAdsModule.categories()} title={'storat.com'} type='replace' hideNavBar={true} />
        <Scene key={'browseAdsFavorites'} component={BrowseAdsModule.favorites()} title={'storat.com'} />
        <Scene key={'selectLanguage'} component={PreferencesModule.selectLanguage()} title={'Select Language'} />
        <Scene key={'selectCountry'} component={PreferencesModule.selectCountry()} title={'Select Country'} />
        <Scene key={'login'} component={AuthModule.login()} title={'Login'} />
        <Scene key={'register'} component={AuthModule.register()} title={'Register'} />
        <Scene key={'loginOrRegister'} component={AuthModule.loginOrRegister()} title={'Login/Register'} type='replace' />
        <Scene key={'verifyMobile'} component={AuthModule.verifyMobile()} title={'Mobile verification'} type='replace' />
        <Scene key={'store'} component={StoreModule.welcome()} title={'Welcome'} type='replace' />
        <Scene key={'selectAdCategory'} component={StoreModule.selectCategory()} title={'Step 1'} />
        <Scene key={'selectAdLocation'} component={StoreModule.selectLocation()} title={'Step 2'} />
        <Scene key={'uploadPhoto'} component={StoreModule.uploadPhoto()} title={'Step 3: Upload Photos'} />
        <Scene key={'adDetails'} component={StoreModule.adDetails()} title={'Step 4: Ad Details'} />
        <Scene key={'customAttributes'} component={StoreModule.customAttributes()} title={'Step 5: Additional Information'} />
        <Scene key={'adPlans'} component={StoreModule.adPlans()} title={'Congrats'} />
        <Scene key={'adListings'} component={AdModule.adListings()} title={'storat.com'} />
        <Scene key={'adListing'} component={AdModule.adListing()} title={''} />
        <Scene key={'adFilter'} component={AdModule.adFilter()} title={'Refine Search'} />
    </Scene>
);

/**
 * Tracking Screens
 */
tracker.trackScreenView("launch");
tracker.trackScreenView("welcome");
tracker.trackScreenView("browseAdsCategories");
tracker.trackScreenView("browseAdsFavorites");
tracker.trackScreenView("selectLanguage");
tracker.trackScreenView("selectCountry");
tracker.trackScreenView("adListings");
tracker.trackScreenView("adListing");
tracker.trackScreenView("adFilter");

/**
 * Dispatch tracking events every 30 secs
 */
GoogleAnalyticsSettings.setDispatchInterval(30);

export default Routes;
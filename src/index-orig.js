import React from 'react';
import { compose, createStore, applyMiddleware } from 'redux';
import { connect, Provider } from 'react-redux';
import { Router } from 'react-native-router-flux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import Routes from '@routes/';
import rootReducer from '@redux/';

/**
 * Tie all routes with Redux.
 */
const ReduxRouter = connect()(Router);

/**
 * Define an array of middlewares.
 */
let middleware = [thunk];

/**
 * Apply following middlewares only in development.
 */
if(__DEV__) {
    middleware = [
        ...middleware,
        createLogger()
    ];
}

/**
 * Configure the store with root reducer and middlewares.
 */
const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(...middleware)
    )
);

/**
 * Make redux available to all components using redux proivider.
 */
const AppRoot = () => (
    <Provider store={store}>
        <ReduxRouter scenes={Routes} />
    </Provider>
);

export default AppRoot;